#ZoomBA CareerCup Samples
## Q1 
### Problem 
 >If you can hop 1, 2, or 3 steps at a time, calculate the total number of possible combinations for `n` steps. 


This question originally appeared [here](https://careercup.com/question?id=5642960319283200).
Tagged with : Linkedin,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA let's you use sequences 
steps = seq( 1,1,2 ) -> { $.p[-1] + $.p[-2] + $.p[-3] }
n = 5
total = fold ( [3:n+1] ) -> { steps.next }
println( total )
```

## Q2 
### Problem 
 >For a given integer array, find a closet pair of elements in the array with a sum of N. For example, {1, 5, 3, 6, 4, 2} and 8, (5, 3) is the pair, but not (6, 2).Note: the condition is an array of integers, which can be either positive or negative. 


This question originally appeared [here](https://careercup.com/question?id=5640434945622016).
Tagged with : Algorithm 

### Solution 

```scala
/* 
 To solve this :
 1. Create a map with value as key, and index as value.
 2. Iterate the list with testing sum (S) S - item is in the map or not.
    2.1. If it is, 
       2.1.1 and the difference in position is 
       smaller than previous min store as min 
       2.1.2 Ignore 
    2.2. If it is not continue to [2] 
*/

def solution( arr, S ){
  // a map of key ( item ) -> item index 
  map = dict(arr) -> { [ $.o , $.i ] }
  min = [ -1,-1, num('inf') ] // store +infinity as minimum 
  fold ( arr , min ) -> {
    diff = S - $.o 
    if( diff @ map &&  map[diff] != $.i && $.p.2 > #|$.i - map[diff]| ){
      $.p = [ $.i , map[diff] , #|$.i - map[diff]| ]
    }
    $.p // returns 
  } 
}
arr = [1,4,5,3,6,4,2] 
#(i,j) = solution( arr , 8 )
printf( '%d,%d\n', arr[i], arr[j] )
```

## Q3 
### Problem 
 >Given a binary tree, how do you serialize and deserialize. Remember it is not BST it is a general binary tree which can also have duplicate elements. 


This question originally appeared [here](https://careercup.com/question?id=5660482122809344).
Tagged with : Amazon,SDE1 

### Solution 

```scala
def Node{
  def $$(value, l=null, r=null){
    $.value = value
    $.children = [l,r]
  } 
  def s_rep(){ // for string format 
    r_str = $.children[1] == null ? '' : $.children[1].s_rep()
    l_str = $.children[0] == null ? '' : $.children[0].s_rep()
    str( '(%s %s %s)', l_str, $.value, r_str )
  }
}
// call it off 
ll = new ( Node , 10 )
lr = new ( Node , 30 )
l = new ( Node , 100 ,ll, lr )
r = new ( Node , 130 )
root = new ( Node , 42 , l , r ) // root node 
println( root.s_rep() )  ((( 10 ) 100 ( 30 )) 42 ( 130 ))  ( ( left_sub_tree_rep )  my_value ( right_sub_tree_rep ) )
```

## Q4 
### Problem 
 >Given a string s and a dictionary of words dict, determine if s can be segmented into a space-separated sequence of one or more dictionary words.For example, givens = "leetcode",dict = ["leet", "code"].Return true because "leetcode" can be segmented as "leet code". 


This question originally appeared [here](https://careercup.com/question?id=5101591666360320).
Tagged with : Uber,Software Developer,Algorithm 

### Solution 

```scala
//ZoomBA : Imperative Style , almost 
def part_word( word , dictionary ){
  len = #|word| 
  for ( n : [ 0 : 2 ** ( len-1) ] ){
    bm = str(n,2)
    bm = '0' ** ( len -  #|bm| -1 ) + bm 
    println( bm )
    last = len - 1 
    start = 0 
    splits = list()
    for ( i = len-2 ; i >=0 ; i -= 1){
       if ( bm[i] == '1' ){  
          splits += word [ i + 1: last ]
          last = i  
       }
    }
    splits += word[ start : last ]
    println ( splits )
    successful = !exists(splits) :: { ! ( $.o @ dictionary ) }
    if ( successful ) return true  
  }
  return false 
}
```

## Q5 
### Problem 
 >Given the root of a Binary Tree along with two integer values. Assume that both integers are present in the tree.Find the LCA (Least Common Ancestor) of the two nodes with values of the given integers.2 pass solution is easy. You must solve this in a single pass. 


This question originally appeared [here](https://careercup.com/question?id=5736619949686784).
Tagged with : Amazon,SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA uLanguage. Simple.
paths = [ '', '' ]
def find_paths( key1, key2,  node , path  ){
  if ( !empty(path.0) && !empty(path.1) ) return 
  if ( node == null ) {
    if ( path #$ key1 ){ paths.0 = path }
    if ( path #$ key2 ){ paths.1 = path }
    return 
  }
  find_paths ( node.left, path + '/' + node.key )
  find_paths ( node.right, path + '/' + node.key )
}
// find paths 
find_paths( key1, key2, root, '' )
// split and now match 
paths.0 = paths.0.split('/')
paths.1 = paths.1.split('/')
#(s,l) = minmax ( paths ) :: { #|$.left| < #|$.right| }
inx = index ( [ #|s|-1:-1 ] ) :: {  
  paths[0][$.item] != paths[1][$.item]  
}
println( s[0][inx] )
```

## Q6 
### Problem 
 >You are provided with 2D char array. You have to provide the 2D char array as response which contains the multiplication od the input array. For eg: input=> {{a,b},{c,d}}, output => {{a,c},{a,d},{b,c},{b,d}} 


This question originally appeared [here](https://careercup.com/question?id=5645649973346304).
Tagged with : Amazon,SDE-2,Coding 

### Solution 

```scala
/* The recursive formulation is too easy 
 So, let's improve on that.
 Observe the problem can be recursively formulated
 as cardinal product of two lists, left one, is a conjugate
 (items themselves may be list), while right ine is a primitive one.
 Thus, simple for loop suffices, which then yields the left list
 for the next iteration.
 */
def _cross_( conjugate, primitive ){
  result = list()
  for ( l : conjugate ){
    for ( r : primitive ){
        // concatenate these lists
        row = list(l)
        row += r 
        result.add(row)   
    }
  }
  result // return them 
}

def cross( arr_of_arr ){
  if ( size(arr_of_arr ) <= 0 ) return [] // fixed 
  cur_index = 0
  // generate left list, conjugate 
  left_list = list ( arr_of_arr[cur_index] ) as { list( $.o ) }
  cur_index += 1
  while ( cur_index < size(arr_of_arr) ){
    left_list = _cross_(left_list,arr_of_arr[cur_index] )
    cur_index += 1
  }
  left_list // return... and we are good 
}

// sample test case
a = [  [ 'a' , 'b' ] , [ 'c' , 'd' ] ]
r = cross( a )
println(r)
```

## Q7 
### Problem 
 >Given an array of words (i.e. ["ABCW", "BAZ", "FOO", "BAR", "XTFN", "ABCDEF"]), find the max value of length(s) * length(t), where s and t are words from the array. The catch here is that the two words cannot share any characters.Assume that there are many words in the array (N words) and average length of word is M.Answer for the example above is "ABCW" and "XTFN" as the result is 4 * 4 = 12. "ABCW" and "ABCDEF" do not work since they share similar characters. 


This question originally appeared [here](https://careercup.com/question?id=5113734827606016).
Tagged with : Google,Software Engineer 

### Solution 

```scala
// ZoomBA : Not optimal this. But shows the power of expression
words = ["ABCW", "BAZ", "FOO", "BAR", "XTFN", "ABCDEF" ]
max_tuple = [ num("-inf") , null, null ]
r = [ 0 : #|words| ]
join( r , r ) :: {
  continue ( $.o.0 >= $.o.1 )
  left = words[ $.o.0 ] ; right = words[ $.o.1 ]
  continue ( !empty( left.value & right.value ) )
  val = #|left| * #|right|
  continue ( max_tuple.0 >= val )
  max_tuple.0 = val ; max_tuple.1 = left ; max_tuple.2 = right 
  false 
} 
println( max_tuple )
```

## Q8 
### Problem 
 >Program to Print below number sequenceIf(n=3)then1*2*3  7*8*9  4*5*6if n=51*2*3*4*511*12*13*14*1521*22*23*24*2516*17*18*19*206*7*8*9*10Can anyone also tell me what kind of pattern it is?Correct me If something is wrong. 


This question originally appeared [here](https://careercup.com/question?id=6231328140820480).
Tagged with : Algorithm 

### Solution 

```scala
// ZoomBA
def print_pattern( n ){
  if ( n < 2 ) { println ( n ) ; return }
  // create n * n matrix
  m = list( [0:n] ) -> { list() }
  // 0 , n-1, 1, n - 2 ...
  row_indices = lfold ( [ 1:n-1],  list(0,n-1) ) ->{
      $.partial += ( $.partial[-2] + ( (-1) ** $.index) )
  }
  count = 1
  for ( row : row_indices ){
    for ( [0:n ] ){
      m[row] += count
      count +=1
    }
  }
  lfold(m) -> { println ( str( $.item, ' * ' ) ) }
}
// use it
print_pattern ( 3 )
```

## Q9 
### Problem 
 >Given n1, n2 is the number after removing one digit from n1. Example, n1 = 123, then n2 can be 12, 13 or 23.If we know the sum of n1 + n2, and find the possible values of n1 and n2.public List<List<Integer>> getNumber(int sum){} 


This question originally appeared [here](https://careercup.com/question?id=6206693105991680).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/* 
The regex is :
n2 = (\d)?x(\d)?y...(\d)?
under |string_n1| = |string_n2| + 1  
*/
def get_number( sum_value ){
  list ( [ 1: sum_value /2 + 1 ]  ) -> {
      n2 = $.o 
      n1 = sum_value - n2 
      // when the digits are not one up 
      s_n1 = str(n1)
      s_n2 = str(n2)
      continue ( size(s_n2) + 1 != size(s_n1) )
      decorated_n2 = fold( s_n2.value , '(\d)?' ) -> { $.p + $.o + '(\d)?' }
      continue( s_n1 !~ str(decorated_n2) )
      [ n1, n2 ]  
  }
}
ns = get_number ( int(@ARGS[0]) )
println(ns)
```

## Q10 
### Problem 
 >Given an unsorted array of integers, find the length of the longest consecutive elements sequence.For example,Given [100, 4, 200, 1, 3, 2],The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.Your algorithm should run in O(n) complexity. 


This question originally appeared [here](https://careercup.com/question?id=5699583505072128).
Tagged with : Uber,Senior Software Development Engineer,Algorithm 

### Solution 

```scala
def  max_contiguous_sub_seq( my_list ){
  left = dict()
  right = dict()
  for ( x : my_list ){
    prev = x - 1
    next = x + 1
    merged = false 
    if ( prev @ right ){
      merged = true
      val = right[prev]
      val += x 
      right -= prev 
      right[x] = val
    } 
    if ( next @ left ){
      merged = true
      val = left[next]
      left -= next
      val.add(0,x)
      left[x] = val 
    }
    if ( !merged ){
      // here, create one element guy 
      my_list = list( x )
      left[x] =  my_list
      right[x] = my_list
    }
  }
  // find the max length guy from left/right 
  #(min,max) = minmax( left ) where { size($.l.value ) < size($.r.value ) } 
  max.value // here we go 
}
l = [ 1, 11, 102, 12, 32, 13, 80, 10 ]
sol = max_contiguous_sub_seq(l)
println(sol)
```

## Q11 
### Problem 
 >Given a array of integers there is one that is repeated several time. How would you compute the length of the sequence of repeated elements.Assuming the initial array is sorted can you do better than O(n) both for spatial and temporal cost? 


This question originally appeared [here](https://careercup.com/question?id=5163293279780864).
Tagged with : unknown,Software Engineer,Coding 

### Solution 

```scala
def find_max_repeated_elem(a){
    m = mset(a)
    #(min,max) = minmax(m){ $.l.value < $.r.value }
    max.value 
}
def find_max_repeated_elem_sorted(a){
    p = { 'item': a[0] , 'count' : 0, 'max' : 0 }
    p = fold ( a , p ) -> {
        if( $.o == $.p.item ){  
           $.p.count += 1
        } else {
           if ( $.p.count > $.p.max ){
                $.p.max = $.p.count 
           }
           $.p.count = 1 
           $.p.item = $.o 
        }
        $.p // return 
    } 
    p.item  
}
```

## Q12 
### Problem 
 >A new species has been discovered and we are trying to understand their form of communication. So, we start by identifying their alphabet. Tough Mr. Brown managed to steal a vocabulary that has words in it in ascending order and a team already identified the letters. So, we have now ordered words as sequences of numbers, print every letter once in ascending order. [3,2][4,3,2][4,1,1][1, 2][1, 1]alphabet: [3, 4, 2, 1] 


This question originally appeared [here](https://careercup.com/question?id=5731729282170880).
Tagged with : Software Engineer,Data Structures 

### Solution 

```scala
words = [ [3,2] , [4,3,2] , [4,1,1] , [1, 2] , [1, 1] ]
letters = words.flatten()
ms = mset(letters)
l = list( ms ) -> { [$.key,$.value] }
sorta(l) :: { $.left[1] < $.right[1] }
alpha = list ( l ) ->{ $.0 }
println(alpha)
```

## Q13 
### Problem 
 >Array consist of -1 and 1, Find count of all sub-arrays where sum = 0.Ex. [-1,1,-1,1]Ans :  4 [-1,1] [1,-1],[-1,1],[-1,1,-1,1]Ex. [-1,-1,1,1]Ans : 2 [-1,1][-1,-1,1,1] 


This question originally appeared [here](https://careercup.com/question?id=5750115626123264).
Tagged with : Snapdeal,Tech Lead,Algorithm 

### Solution 

```scala
/* shows how declarative paradigm is powerful */
def count_zero_sum_sub_arrays( arr ){
  len = size(arr)
  // generate sum from combinations of [0,1,2,...len-1] taking 2 at a time 
  sum ( comb( [0:len] , 2 ) ) ->{
    // when the sum is 0 add 1, else 0 
    ( 0 == sum( [$.0:$.1+1] ) -> { arr[$.o] } ) ? 1 : 0 
  }
}
arr = [-1,1,-1,1]  
println( count_zero_sum_sub_arrays(arr) )
```

## Q14 
### Problem 
 >A = "ffgggtvshjsdhjfffffffhvjbjcharu"Find the max consecutive repitative chracterOutput : f -> 7 


This question originally appeared [here](https://careercup.com/question?id=5068525614923776).
Tagged with : Amazon,Testing / Quality Assurance,Online Test 

### Solution 

```scala
// ZoomBA 
word = "ffgggtvshjsdhjfffffffhvjbjcharu" 
max = { 'count' : 0, 'letter' : null , 'current' : 0 }
reduce ( word.value ) -> {
  continue ( $.o == $.p ){ max.current += 1 ; $.o }
  if ( max.count < max.current ){
    max.count = max.current
    max.letter = $.p 
    max.current = 1
  }
  $.o 
}
max -= 'current'
println( max )
```

## Q15 
### Problem 
 >It was an over email interview:Write a program that takes as input a sufficiently large text document (several are available online for testing; e.g. via Project Gutenberg), and produces as output (via stdout) an alphabetical listing of each unique word in the document (case insensitive and space separated, though be careful to consider hyphenated words), along with the lines from the input document that the word appears on. Each unique word (and the list of lines that it appears on) should be on a separate line in the output.For example, taking the following text as input:This issome kind OF text itIs an example of textThe following would be the output:an 3example 3is 1 3it 2kind 2of 2 3some 2text 2 3this 1 


This question originally appeared [here](https://careercup.com/question?id=5742635739250688).
Tagged with : Apkudo,SDE1,Sorting 

### Solution 

```scala
d = fold ( file( 'foo.txt') , sdict() ) -> {
  line_no = $.i + 1 
  partial = $.p
  line = $.o 
  tokens ( line , '\S+') -> {  
    word = $.o.toLowerCase 
    if ( word @ partial ){
      partial[word] += line_no
    } else {
      partial[word] = list( line_no )
    }
  }
  $.p // return the partial 
}
// print the stuff 
for ( d ) { printf('%s : %s\n', $.key , str( $.value , ' ' ) ) }
```

## Q16 
### Problem 
 >Given a list L of numbers from 0 to n, and another number k = [0-9], find how many times k appears in L. If the target number in L is more than one digit, treat each digit separately. For example, k=0 appears twice in L = [0,10]. 


This question originally appeared [here](https://careercup.com/question?id=5655981552828416).
Tagged with : Adobe,SDE-3,Algorithm 

### Solution 

```scala
def find_occurences( numbers , k ){
  sk = str(k)
  sum ( numbers ) -> { sk @ str($.o) ? 1 : 0 }
}
println( find_occurences ( [0,10] , 0 ) )
```

## Q17 
### Problem 
 >Given a binary tree, write a recursive method boolean method(int x, int y) which will return true 1. if node y (meaning a node with a value of int y) is a node that is contained in one of the two possible subtrees of x, 2. true if    x==y, and a node with a value x==y exists,    otherwise  3. return false,          basicallly whether   x  contains       y? I had  a question like this on my exam, I solved this problem using   four arguments in my     function. I wonder whether it is even possible to solve it with only 2 int arguments and   recursively. 


This question originally appeared [here](https://careercup.com/question?id=5685873063493632).
Tagged with : Trees and Graphs 

### Solution 

```scala
def find_node_with_value(root,node_value){
  stack = list()
  stack.push(root)
  while ( !empty(stack) ){
    node = stack.pop()
    if ( node_value == node.value ){
      return node 
    }
    if ( node.left != null ){
      stack.push(node.left)
    }  
    if ( node.right != null ){
      stack.push(node.right)
    }
  }
  return null
}
// i have an implicit root 
def find(x,y){
  x_node = find_node_with_value(root ,x)
  if ( x_node == null ) return false 
  y_node = find_node_with_value(x_node ,y)
  y_node != null // return this 
}
```

## Q18 
### Problem 
 >2.1 career discussion2.2 divide two numbers with no / or % 


This question originally appeared [here](https://careercup.com/question?id=5707164625666048).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
def div_wo(a,b){
  panic( b == 0 , 'Terrible parameter!')
  printf('%d/%d%n', a,b)
  sign_change = false  
  if ( a < 0  ){
    sign_change = true   
    a = -a 
  } 
  if ( b < 0 ){
    sign_change ^= true
    b = -b
  } 
  res = 0 
  while ( a >= b ){
     res += 1
     a -= b 
  } 
  sign_change ? -res : res 
}
println( div_wo(0,1) )
println( div_wo(1,1) )
println( div_wo(1,2) )
println( div_wo(2,1) )
println( div_wo(2,-1) )
println( div_wo(-2,-1) )
println( div_wo(-2,1) )
println( div_wo(3,2) )
```

## Q19 
### Problem 
 >Find the first and last occurrence of a number in a sorted array of integersFor Example: int[] a = {1,2,3,4,5,5,7,8} 


This question originally appeared [here](https://careercup.com/question?id=5120301346062336).
Tagged with : Amazon,Quality Assurance Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
a = [ 1,2,3,4,4,4,5,5,7,8 ]
def bin_search( a, n, i, j ){
   while ( i <= j ){
      mid = ( i + j )/2
      if( a[mid] < n ){
         i = mid + 1
      }else if( a[mid] > n ){
         j = mid - 1
      }else{
         return mid
      }
   }
   return -1
}
def find_l_r( a, n ){
   inx = bin_search( a , n , 0 , #|a| - 1 )
   if ( inx < 0 ) return [ -1, -1 ]
   l = inx ; r = inx
   while ( inx >= 0 ){
     l = inx
     inx = bin_search ( a , n, 0, inx -1 )
   }
   inx = r
   while ( inx >= 0 ){
     r = inx
     inx = bin_search ( a, n , inx + 1 , #|a| - 1 )
   }
   [ l, r ]
}
println( find_l_r ( a, 5 ) )
```

## Q20 
### Problem 
 >Given a group of movies and their start time, assuming that are 1 hour long,Returns a movie schedule (no time conflict).enter:Movie ("Shining", [14, 15, 16])Movie ("kill bill", [14, 15])Movie ("Pulp fiction", [14, 15])One possible result is shining 16, kill bill 15, pulp fiction 14public void schedule (HashMap <String, List <Integer >> map) { } 


This question originally appeared [here](https://careercup.com/question?id=5682055978418176).
Tagged with : Facebook,SDE1 

### Solution 

```scala
movies = { "Shining" : [14, 15, 16], 
"kill bill" : [14, 15], 
"Pulp fiction": [14, 15] }
args = list ( movies.entries ) as { name = $.key ; list( $.value ) as { [ name, $.o ] } }
movie_size = size(movies)
all_sched = join( @ARGS = args ) where {
  items = list ( $.o )
  sorta ( items ) where { $.left[1] < $.right[1] }
  !exists ( [1:movie_size] ) where { items[$.o][1] - items[$.o-1][1] < 1 }
}
println(str(all_sched,'\n'))  ➜  wiki git:(master) ✗ zmb tmp.zm
@[ @[ Shining,16 ],@[ Pulp fiction,14 ],@[ kill bill,15 ] ]
@[ @[ Shining,16 ],@[ Pulp fiction,15 ],@[ kill bill,14 ] ]
➜  wiki git:(master) ✗
```

## Q21 
### Problem 
 >Given employee information in an organisation in the formal - emp_id,firstname,lastname,reports_to in the following  way{    string[] Values = new[] { "Mc   Grill,Mc,Grill,Karmon","Karmon,Zech,Karmon,Joe","Mithun,Try,Mithun,Joe","Joe,Top,Joe,","Zara,Aman,Zara,Mc Grill","Fizzy,Dude,Fizzy,Mc Grill"}; } Print the information from the top to bottom level in the way         1) Top Joe            2) Try Mithun            2) Zech Karmon               3) Mc Grill                   4)Aman Zara                   4) Dude Fizzy 


This question originally appeared [here](https://careercup.com/question?id=5761473844346880).
Tagged with : Amazon 

### Solution 

```scala
// ZoomBA
emp_list = [ "Mc Grill,Mc,Grill,Karmon",
   "Karmon,Zech,Karmon,Joe","Mithun,Try,Mithun,Joe",
   "Joe,Top,Joe,","Zara,Aman,Zara,Mc Grill",
   "Fizzy,Dude,Fizzy,Mc Grill" ]

def create_node( string ){
  fields = string.split( ',' ,-1)
  { 'id' : fields[0] , 'fname' : fields[1] ,
    'lname' : fields[2] , 'mgr' : fields[3] , 'children' : set() } 
}   

def print_node( node_id ){
   node = nodes[node_id]
   printf( '%s %s\n', node.fname, node.lname )
   for ( node.children ){
      print_node ($)
   }
}

nodes = dict ( emp_list ) -> { 
  e = create_node( $.o )
  [ e.id , e ]
}

root = lfold(nodes, '' ) ->{
  node = $.o.value 
  continue ( empty(node.mgr) ){ $.p = node.id }
  parent = nodes[node.mgr]
  parent.children += node.id 
  $.p
}

print_node( root )
```

## Q22 
### Problem 
 >Write a program to check if the given string is repeated substring or not.ex 1: abcabcabc - yes - abc is repeated.2: abcdabababababab - no. 


This question originally appeared [here](https://careercup.com/question?id=5157662506352640).
Tagged with : Ivycomptech,Algorithm 

### Solution 

```scala
// ZoomBA
exists ( [1: #|s|/2 ] ) :: {
   pat = '(' + s[0:$.o] +')+' // create pattern 
   s =~ pat // s matches pattern?
}
```

## Q23 
### Problem 
 >Given an array of numbers find the duplicates 


This question originally appeared [here](https://careercup.com/question?id=5727733213560832).
Tagged with : Linkedin,Software Developer,Algorithm 

### Solution 

```scala
// From ZoomBA , with Love 
def not_sorted( a ){
   select ( mset( a ) ) :: { $.o.value > 1 } -> { $.o.key }
}
```

## Q24 
### Problem 
 >Given a nxn matrix, with partially filled cells of numbers from 1..n and the rest with 0's. Fill the cells such each row and column has numbers 1 to n without any repetition.Eg:[	[1, 2, 0],	[0, 1, 0],	[0, 0, 1]][	[1, 2, 3],	[3, 1, 2],	[2, 3, 1]] 


This question originally appeared [here](https://careercup.com/question?id=5695933925818368).
Tagged with : Yahoo,SDE1,Algorithm 

### Solution 

```scala
// These sort of problems are kids play in ZoomBA
// We first create a regular expression match from the templates given 
// Here, we replace 0 -> .? to ensure only one char matches for 0
def matching_rows( template_row , all_permutations ){
  template = str(template_row,'').replace('0','.?')
  select( all_permutations ) :: { str($.item,'') =~ template }
}
// A join result sudoku is valid if and only if  
// the column splitting are all unique
def valid_sudoku( m , n ){
  !exists ( [0:n] ) :: {
    s = set ( [0:n] ) -> { m[$.item][$.$.item] }
    #|s| != n  
  }
}
// get the template
my_template = [ [1, 2, 0], [0, 1, 0], [0, 0, 1] ]

// generate the permutations 
perms = join( @ARGS = list([1:4]) -> { [1:4].list } ) :: {
   #|set($.item)| == #|$.item|
}
// from the templates, get the permutations which can be used for 
// generating the input argument for the join 
args = list ( my_template ) -> { 
  matching_rows( $.item , perms )
}
// finally join the options to solve the sudoku as a join problem 
results = join ( @ARGS = args ) :: {
   valid_sudoku ( $.item , 3 )
}
println( results )
// and it is still less than 42 lines of code.
```

## Q25 
### Problem 
 >a = 0, b=0, c=1 are the 1st three terms. All other terms in the Lucas sequence are generated by the sum of their 3 most recent predecessors.Write a program to generate the first n terms of a Lucas Sequence. 


This question originally appeared [here](https://careercup.com/question?id=6267265864433664).
Tagged with : xyz 

### Solution 

```scala
// ZoomBA
numbers = lfold( [2:n+1] , list(0,0,1) ) ->{
   current_item = $.p[-1] + $.p[-2] + $.p[-3] 
   $.p += current_item 
}
```

## Q26 
### Problem 
 >Each test file starts with an integer ‘t’ - the number of testcases.In each of the next ‘t’ lines, you are given a string of ‘n’ characters [ either ‘(‘ or ’)’ or ‘*’ ]. Your task is to find the number of distinct balanced parentheses expressions you can make by replacing the ‘*’ with either ‘(‘ or ‘)’ or removing the ‘*’Note : You have to replace each ‘*’ with one of  ‘(‘ or ‘)’ or remove it. If removed, assume the string has reduced by 1 character.Duplicate strings are not allowed. The final expressions to be counted have to be distinctAs the answer may be large, please output it modulo 1000000007 (10^9+7)Output one integer per line corresponding to each testcase.Constraints :1 <= t <= 201 <= n <= 1000 <= Number of ‘*’ in the input string <= min(n,10)Sample Input:2(*(*)*)*(*(**)*Sample Output59ExplanationThe five possible valid solutions are for the first input are :((()))()(())()()()(())()(())The nine possible valid solutions are for the second input are :(((())))(()(()))(()()())(()())((()))()(())()()()()()(()) 


This question originally appeared [here](https://careercup.com/question?id=5734337711439872).
Tagged with : Uber,Software Developer,Algorithm 

### Solution 

```scala
// ZoomBA 
/* 
A replacement of '*' -> '(' implies +1 
A replacement of '*' -> ')' implies -1 
At any point, if the count is < 0, drop the path.
Finally, if the count is == 0, accept the path.
As we have three options, the choices can be made in ternary,
Thus, given k choice locations, there are 3 ** k options
Hence, we can reduce the recursion to iteration.  
Using a mixture of imp/dec because of readability  
*/

def count_options( s ){
  num_stars = sum( s.value ) -> { $.item == _'*' ? 1 : 0  }
  options = set() ; len = #|s|
  map = { _'0' : '',  _'1' : '(' , _'2' : ')' , _'(' : 1, _')' : -1 }
  for ( select_pattern : [0: 3 ** num_stars ] ){
    star_count = -1 
    ter_pattern = str( select_pattern , 3 ) 
    ter_pattern = '0' ** ( #|ter_pattern | - num_stars ) + ter_pattern 
    transformed = fold( s.value ,'' ) -> { 
      $.prev + ( $.item == _'*' ? map[ter_pattern[ star_count += 1 ]] :  $.item )
    }
    continue( transformed @ options ) // avoid unnecessary stuff
    res = sum ( transformed.value ) -> { 
      break( $.prev < 0 ){ -len } ; map[$.item] 
    }
    if ( res == 0 ){ options += transformed }
  }
  println( str( options,'\n' ) )
  size( options ) // return size of options 
}
println( count_options( '(*(*)*)' ) )
```

## Q27 
### Problem 
 >Given a string find biggest palindrome substring. For example for given string "AABCDCBA" output should be "ABCDCBA" and for given string "DEFABCBAYT" output should be "ABCBA". 


This question originally appeared [here](https://careercup.com/question?id=5635968435486720).
Tagged with : Bloomberg LP,Financial Software Developer,Algorithm 

### Solution 

```scala
// ZoomBA : Better optimised code 
def is_palindrome( s, i, j ){
  !exists ( [i : (j - i)/2 + i + 1 ] ) :: { 
    s[ $.item ] != s[ i + j - $.item ] 
  } 
}
def find_max_palindrome( string ){
   max_palindrome = [0,0] ; r = [0:#|string|]
   join ( r, r ) :: {
     i = $.item.0 ; j = $.item.1
     continue( max_palindrome.1 - max_palindrome.0 > j - i ||
      !is_palindrome( string , i, j ) )
     max_palindrome.0 = i ; max_palindrome.1 = j 
     false 
   }
   string[ max_palindrome.0 : max_palindrome.1 ]
}

s =  "DEFABCBAYT" 
r = find_max_palindrome( s )
println(r)
```

## Q28 
### Problem 
 >override "+" operator for class Point to achieve the following:   Point a(1,2), b(3,4);  Point c = a+b; // c => (4,6) 


This question originally appeared [here](https://careercup.com/question?id=5737277222289408).
Tagged with : Developer Advocate 

### Solution 

```scala
// ZoomBA
// define Point 
def Point : {
   $$ : def(x=0,y=0){  $.x = x ; $.y = y },
   $add : def (o) { new ( Point , $.x + o.x , $.y + o.y ) },
   $str : def(){ str('(%s,%s)', $.x, $.y)}
}

a = new ( Point, 1,2)
b = new ( Point, 3,4)
c = a + b 
println(c)
```

## Q29 
### Problem 
 >Give you an unsorted integer iterator and a percentage that is expressed in double (for example, 0.4 for 40%), and find the number of the sorted array at the percentage position. Example: Enter [1 3 2 5 4 6 7 9 8 10], and 0.6, you will return 6public int findNumber(Iterator<Integer> nums, double percent){} 


This question originally appeared [here](https://careercup.com/question?id=5761201817518080).
Tagged with : Facebook,Software Engineer 

### Solution 

```scala
primes = seq ( 2 ) ->{
   prime_cache = $.prev 
   last = prime_cache[-1] + 1 
   //  en.wikipedia.org/wiki/Bertrand%27s_postulate 
   end = last * 2 
   last + index( [ last : end ] ) :: {  !exists ( prime_cache ) :: {  $.item /? $.$.item  }  }
}  god_knows = seq( random(100) ) -> { random(100 ) }
```

## Q30 
### Problem 
 >Given a set of numbers like <10, 36, 54,89,12> we want to find sum of weights based on the following conditions    1. 5 if a perfect square    2. 4 if multiple of 4 and divisible by 6    3. 3 if even numberAnd sort the numbers based on the weight and print it as follows<10,its_weight>,<36,its weight><89,its weight>Should display the numbers based on increasing order. 


This question originally appeared [here](https://careercup.com/question?id=5751300286316544).
Tagged with : Zoho,SDE1,General Questions and Comments 

### Solution 

```scala
// ZoomBA
wl = list( numbers ) ->{
   n = $.o 
   w = 0 // a /? b => a divides b ? boolean 
   w += (((sr = n ** 0.5) == int(sr) ) ? 5 : 0 )
   w += ((( 4 /? n ) &&  ( 6 /? n ) ) ? 4 : 0 ) 
   w += ((2 /? n) ? 3 : 0 )
   [n,w]  
}
sorta( wl ) :: { $.o.0.1 < $.o.1.1 }
for ( wl ){  printf ( '<%d,%d>' , $.0, $.1 ) }
println()
```

## Q31 
### Problem 
 >prime factors. given a number  return the prime factor multiplication.  eg. 90 = 2 * 3 * 3 * 5. 


This question originally appeared [here](https://careercup.com/question?id=5695742657167360).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/* 
Prime factors
Automatic finding of primes and then factors.
So, here, we go.
*/
def prime_factors_str( n ){
  // basic stuff 
  upto = ceil( n ** 0.5 )
  // should be pretty easily understandable 
  #(primes, factors) = fold( [2:upto],  [ list(), list() ] ) ->  {
    // is my current no prime ?
    pp = $.o // possible prime
    #(primes,factors) = $.p 
    continue ( exists( primes ) :: { $.o /? pp  } )
    // now it is prime 
    primes += pp 
    // now, is it a factor ?
    while ( pp /? n  ){ n /= pp ; factors += pp  } 
    $.p // return  
  }
  // print factors 
  str ( factors , '*' ) 
}
println( prime_factors_str(90) )
```

## Q32 
### Problem 
 >Phone Interview, New Grad - Software DeveloperImagine you are given 10,000 files each containing 1 Million integers. I would you sum all of them and give the final result?---> Interviewer wanted to test scalability, distributed concepts.He has written the basic code and wanted to improve upon that.Here's the basic code.public getSum(String[] file_names) {
	int sum = 0;
	for(String f: file_names) {
		sum = sum + sumOfFile(f);
	}
	return sum;
}Questions:What's wrong with above code? Ans: Integer overflowHow would you implement sumOfFile?What if 'sumOfFile' takes lot of time to finish computing?How do you fasten the program?Overall scalability etc 


This question originally appeared [here](https://careercup.com/question?id=5167397506908160).
Tagged with : Google,Software Developer,Distributed Computing 

### Solution 

```scala
/* 
1. bucketize on thread pool.
2. Shoot.
*/

// per thread num of file 
bucket_size = 100 
// assume list of files are here 
files // yep, files as in like list of strings 
num_files = size(files)
def process_files ( start_inx ){
  end_inx = start_inx + bucket_size 
  end_inx = end_inx > num_files ? num_files : end_inx
  sum ( [start_inx : end_inx ] ) -> { sumOfFile( files[$.o] ) }
} 
num_buckets = ceil ( num_files / bucket_size )
start_inx = 0 
threads = list ( [0:num_buckets] ) -> {
  start_inx += bucket_size
  thread( ) -> { process_files( start_inx ) }
}
// do not get into a loop, ever 
succeeded = poll( 42, 300 ) :: { !exists( threads ) :: {  $.o.alive } }
// zoomba threads return value, proper they are option monad 
assert ( succeeded , "Why you fail in timeout???" )
total_sum = sum ( threads ) -> { $.o.value }
println( total_sum ) // should be cool
```

## Q33 
### Problem 
 >input ={"p","q","r"},{"p","s","w"} return intersection value {"p"}output:{"p"} 


This question originally appeared [here](https://careercup.com/question?id=5671982566735872).
Tagged with : Persistent Systems,Java Developer 

### Solution 

```scala
// ZoomBA
input1 = [ "p","q","r"]  
input2 = [ "p","s","w"] 
intersect = input1 & input2
```

## Q34 
### Problem 
 >Given the root of a binary tree containing integers, print the columns of the tree in order with the nodes in each column printed top-to-bottom.Input:
      6
     / \
    3   4
   / \   \
  5   1   0
 / \     /
9   2   8
     \
      7

Output:
9 5 3 2 6 1 7 4 8 0

Input:
       1
     /   \
    2     3
   / \   / \
  4   5 6   7

When two nodes share the same position (e.g. 5 and 6), they may be printed in either order:

Output:
4 2 1 5 6 3 7
or:
4 2 1 6 5 3 7 


This question originally appeared [here](https://careercup.com/question?id=5749533368647680).
Tagged with : Facebook,Software Engineer Intern,Trees and Graphs 

### Solution 

```scala
// ZoomBA
mapping = dict()
// the basic mapping is created using this 
def traverse( node , cur_x, cur_y ){
  if ( !(cur_x @ mapping) ){  mapping[cur_x] = list() }
  // append :store both y co ordinate as well as value
  mapping[cur_x].add ( [ cur_y , node.value ])  
  // change the x to left, so one minus, and level one down 
  if ( !empty( node.left) ) { traverse( node.left, cur_x - 1, cur_y + 1 ) }
  // change the x to right, so one plus, and level one down
  if ( !empty( node.right) ) { traverse( node.right, cur_x + 1, cur_y + 1 ) }
}
traverse( root, 0, 0 ) // basis 
// sort the keys based on x coordinate 
keys = sorta ( list ( mapping.keySet ) )
fold ( keys ) ->{
   values = mapping[$.item] // get the pair values in that key 
   // sort the values based on the pairs left item, e.g. y coordinate
   sorta( values ) :: { $.item[0][0] < $.item[1][0]  }
   // now, simply print all the values : the right value 
   printf ( '%s', str( values , ' ') ->{  $.item[1] } )
}
// end with a newline 
println()
```

## Q35 
### Problem 
 >Given a sorted array, find all the numbers that occur more than n/4 times. 


This question originally appeared [here](https://careercup.com/question?id=5669183904808960).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
println( select ( mset( array ) ) ) :: { $.value > size(array)/4 } )
```

## Q36 
### Problem 
 >Given a string, print out all of the unique characters and the number of times it appeared in the string 


This question originally appeared [here](https://careercup.com/question?id=5721038127104000).
Tagged with : Microsoft,Intern,Java,String Manipulation 

### Solution 

```scala
// ZoomBA
s = "I am awesomely in Love With ZoomBA!" 
ms = mset(  s.value )
// produces 
/* 
{ =6, a=2, A=1, !=1, B=1, e=3, h=1, I=1, i=2, l=1, L=1, m=3, n=1, o=4, s=1, t=1, v=1, w=1, W=1, y=1, Z=1} // HashMap
*/
```

## Q37 
### Problem 
 >Write a program to generate the anagrams of a word 


This question originally appeared [here](https://careercup.com/question?id=5723965491249152).
Tagged with : JP Morgan,Java Developer,Java 

### Solution 

```scala
def anagrams( word ){
  indices = [0:size(word)].asList()
  // permutate the indices 
  // use the index to map to a word ( a tuple is index tuple )
  // collect into a set 
  from ( perm( indices ) , set() ) as { str($.o,'') as { word[$.o] } }
}
println( anagrams('test') )
```

## Q38 
### Problem 
 >Lazy Bartender :

There are N number of possible drinks.(n1,n2..)
Has C number of fixed customers.
Every customer has fixed favourite set of drinks.
Bartender has to create least possible number of drinks
to suffice need of all the customers
Example:
Cust1: n3,n7,n5,n2,n9
Cust2: n5
Cust3: n2,n3
Cust4: n4
Cust5: n3,n4,n3,n5,n7,n4

Output: 3(n3,n4,n5) 


This question originally appeared [here](https://careercup.com/question?id=5672092990177280).
Tagged with : Amazon,SDE-2 

### Solution 

```scala
mix = { 'Cust1' : [ 'n3', 'n7', 'n5', 'n2' , 'n9' ],
        'Cust2' : [ 'n5' ],
        'Cust3' : [ 'n2' , 'n3' ],
        'Cust4' : [ 'n4' ],
        'Cust5' : [ 'n3' , 'n4' , 'n3' , 'n5' , 'n7' , 'n4' ] }
// what drinks serves who?
serving = fold( mix.keys , dict() ) as {
   values = mix[$.o]
   for ( v : values ){
     if ( v @ $.p ){
       $.p[v] += $.o 
     } else {
       $.p[v] = set($.o)
     }
   }
   $.p // return     
} 
// next, sort by no of customer a drink serves, and union off, 
// go with greedy mode. if adding a drink does not increase customer base
// no need to add it.. thus...
l = list ( serving ) as { [ $.key , $.value ] }
sortd(l) where { size($.left[1]) < size($.right[1])  }
drinks = set( l[0][0] )
catered = set( l[0][1] )
n = size(catered)
i = 1 
while ( n != size( mix ) ){ 
   catered |= l[i][1]
   if ( size(catered) > n ){
      n = size(catered) 
      drinks += l[i][0]
   }
   i += 1
}
println(drinks)
// caution, the problem is bin packing. Greedy is NOT a gurantee.
// en.wikipedia.org/wiki/Bin_packing_problem#Exact_algorithm
// A gurantee is, of course NP Hard.
```

## Q39 
### Problem 
 >Given a set find its power set. (This question is from CTCI) Interviewer then discussed the complexity of my solution. He asked me to explain him how the complexity is 2^n? As per my solution, I was iterating over an arraylist which contained the set elements so the size of list was getting doubled in every iteration. Hence the complexity (2^0 + 2^1 + 2^2 +.....+2^n-1 = 2^n) 


This question originally appeared [here](https://careercup.com/question?id=4862471882932224).
Tagged with : Bloomberg LP,Software Engineer / Developer,Algorithm 

### Solution 

```scala
/* 
  ZoomBA powerset is simply done by 
  the following 
*/
my_vals = [ 0, 1, 2, 3 ]
// sequences() function generates all possible sub sequences
// which is the power set :) 
fold ( sequences(my_vals) ) -> { println($.o) }

// in a sensible way, this is how you can do the same
// also explains why it is Theta( 2^n )
def power_set( arr ){
  len = size( arr ) 
  n = 2 ** len
  for ( bn : [0:n] ){ // this is why it is 2^n 
    bs = str( bn, 2 ) // binary string format 
    // to make it sized n binary digits 
    bs = ( '0' ** ( len - size(bs) ) ) + bs // pad 0's to the left 
    // select the index i's item when bs contains 1 at index i 
    subseq = list( bs.value ) -> { continue( $.o == _'0' ) ; arr[$.i] }
    println ( subseq )
  } 
}
power_set( my_vals )
```
```scala
my_vals = [ 0, 1, 2, 3 ]
// minimizing use of English and more math 
def power_set( arr ){
  len = size( arr ) 
  // this is why it is 2^n 
  list ( [0 : 2 ** len ] ) -> { 
    bs = str( $.o , 2 ) // binary string format 
    // to make it sized n binary digits 
    bs = ( '0' ** ( len - size(bs) ) ) + bs // pad 0's to the left 
    // select the index i's item when bs contains 1 at index i 
    select( bs.value ) :: { $.o == _'0' } -> { arr[$.i] }
  } 
}
println ( power_set( my_vals ) )
```

## Q40 
### Problem 
 >Write a program to print all permutations of a given string. 


This question originally appeared [here](https://careercup.com/question?id=4799405803700224).
Tagged with : JP Morgan,Software Engineer / Developer,Problem Solving 

### Solution 

```scala
// ZoomBA
s = "test"  
l = s.value 
r = [0:#|l|]
permutations = set()
join( @ARGS = list(r) as { l } ) where {
    continue ( #|set($.o)| != #|l| ) // mismatch and ignore 
    v = str($.o,'') as { l[$.o] }
    continue ( v @ permutations ) // if it already occurred 
    permutations += v // add them there  
    false // do not add  
}
println( permutations )
```

## Q41 
### Problem 
 >Given a set of values 0-9, return all permutations of that set of length n.  Example:  n=2, set ={2,3,4} Return: {2,2}, {3,3}, {4,4}, {2,3}, {3,2}, {3,4}, {4,3}, {2,4}, {4,2} 


This question originally appeared [here](https://careercup.com/question?id=5101575825522688).
Tagged with : Google,Algorithm 

### Solution 

```scala
// ZoomBA permutations 
def permutate ( n ){
   join( @ARGS = list([0:n]) -> { [0:n] } ) :: {
      #|set($.o)| == #|$.o|
   }
}
println( permutate(3) )
```

## Q42 
### Problem 
 >Given String is "a4b2c2a3f1g2" and out put should be "aaaabbccaaafgg" . the number after char is the occurance of that character in string. 


This question originally appeared [here](https://careercup.com/question?id=4869410629091328).
Tagged with : Techlogix,Intern 

### Solution 

```scala
// ZoomBA
/* 
We are assuming only alpha.
Thus, the general regex for the string is :
word -> [a-zA-Z][\d]+ 
s = ( word )+
Thus, the problem can be solved by tokenization of word
and then expanding the word
*/
word = '[a-zA-Z][\\d+]'
string = "a4b2c2a3f1g2"
println(string)
// tokenize on string 
l = tokens( string , word ) -> { 
  // extract letter 
  letter = (tokens($.o, '[a-zA-Z]' ))[0]
  // extract frequency
  frequency = (tokens($.o, '\\d+' ))[0]
  // expand the letter with frequency : a ** 2 -> aa 
  letter ** int(frequency) 
} 
// finally catenate over the list 
println( str(l,'') )
```

## Q43 
### Problem 
 >2/5 Round at Uber Bar raiser - Behavioral questions. Coding: Find if a set of meetings overlap. Meeting has a starttime and an endtime with accuracy to minute. All meetings take place in the same day. Do this in O(n) time. 3/5 Round at Uber Coding: Subset sum. Follow-up: Optimize the solution. 


This question originally appeared [here](https://careercup.com/question?id=5698714680164352).
Tagged with : Uber,Software Engineer,Algorithm 

### Solution 

```scala
// radix_sort is Theta(n)
def radix_sort(meetings){
  // all of these, by definition will have same no digits :: max 4  
  no_digits = size ( meetings[0].start, 10 )
  for ( inx : [no_digits-1:-1] ){
    bucket = mset ( meetings ) as { int ( (str($.start))[inx] ?? 0 ) }
    meetings = fold( [0:10] ,list() ) as { 
      continue( $.o !@ bucket )  
      $.p += bucket[$.o] }
  }
  meetings // return 
}
def is_overlap(meetings){
  meetings = radix_sort(meetings)
  exists ( [1:size(meetings)] ) where {
    meetings[$.o - 1].end >= meetings[$.o].start   
  }
}
```

## Q44 
### Problem 
 >For a given string and dictionary, how many sentences can you make from the string, such that all the words are contained in the dictionary. // eg: for given string -> "appletablet"// "apple", "tablet"// "applet", "able", "t"// "apple", "table", "t"// "app", "let", "able", "t"// "applet", {app, let, apple, t, applet} => 3// "thing", {"thing"} -> 1 


This question originally appeared [here](https://careercup.com/question?id=5645065735110656).
Tagged with : Uber,Software Engineer / Developer,Algorithm 

### Solution 

```scala
//ZoomBA : Imperative Style , almost 
def part_word( word , dictionary ){
  len = #|word| 
  for ( n : [ 0 : 2 ** ( len-1) ] ){
    bm = str(n,2)
    bm = '0' ** ( len -  #|bm| -1 ) + bm 
    println( bm )
    last = len - 1 
    start = 0 
    splits = list()
    for ( i = len-2 ; i >=0 ; i -= 1){
       if ( bm[i] == '1' ){  
          splits += word [ i + 1: last ]
          last = i  
       }
    }
    splits += word[ start : last ]
    println ( splits )
    successful = !exists(splits) :: { ! ( $.o @ dictionary ) }
    if ( successful ) return true  
  }
  return false 
}
```

## Q45 
### Problem 
 >A program P reads in 500 integers in the range (0,100) representing the scores of 500 students. It then prints the frequency of each score above 50. Implement program P in C language. 


This question originally appeared [here](https://careercup.com/question?id=5736601259868160).
Tagged with : Skill Subsist Impulse Ltd,Software Engineer / Developer,C 

### Solution 

```scala
// ZoomBA
//A program P reads in 500 integers in the range (0,100)
scores = list([0:500]) -> { random(100) }
map = mset( scores )
lfold( map ) -> { continue( $.o.key <= 50 ) 
  printf( '%d -> %d\n', $.o.key , $.o.value  ) 
}
```

## Q46 
### Problem 
 >Given a n-nery tree and its deep copy, a node in the original tree,  return the corresponding node in the copypublic TreeNode getCopyNode(TreeNode root, TreeNode copy, TreeNode node) 


This question originally appeared [here](https://careercup.com/question?id=5689887448629248).
Tagged with : Facebook,SDE1 

### Solution 

```scala
String find_path(TreeNode root, TreeNode node)
 // returns the path to node in root, empty string otherwise.  TreeNode apply_path(TreeNode root, String xPath)
 // returns the node to the path in root, null otherwise.
```
```scala
See my comments above.
def _recurse_( root, node, path ){
   if ( empty(root.children) ){
       return ( root == node ? path : '') 
   }
   for ( inx : [0: size(root.children) ] ){
       path = _recurse_(root.children[inx], node, path + '/' + inx )
       if ( !empty(path) ){
           return path
       }
   }
   return ''
}
def find_path(root, node){
   _recurse_(root,node,'')
}
def apply_path( root, path ){
   if ( empty(path) ) return null
   indexes = path.split('/') 
   cur = root 
   for ( inx : indexes ){
      cur = cur[inx] ?? null 
      if ( empty(cur) ) return null 
   }
   cur // reuturn 
}
def facebook_problem( root_orig, node_orig, root_deep_copy){
    path = find_path(root_orig,node_orig)
    apply_path( root_deep_copy, path )
}
```

## Q47 
### Problem 
 >Given a string, find all possible combinations of the upper 
and lower case of each Alphabet char, keep the none Alphabet char as it is.
example1 s = "ab", return: "Ab", "ab", "aB", "AB"
example2 s = "a1b", return: "A1b", "a1b", "a1B", "A1B"
public List<String> getPermutation(String s){
	
} 


This question originally appeared [here](https://careercup.com/question?id=5666765251018752).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/* 
What you want is cardinal product
Given a string is c_0,c_1,c_2, ....
We need a cardinal product of 
[lower(c_0),upper(c_0)] X [lower(c_1),upper(c_1)] X ...  
*/
word = 'ab'
// generate the options 
args = list( word.value ) as {  [ str($.o).toLowerCase(), str($.o).toUpperCase() ]  }
// use the options for join - and map the tuple into a word by using str with seperator ''
result = join( @ARGS = args ) as { str($.o,'') }
println(result)
```

## Q48 
### Problem 
 >Find the maximum consecutive 1's in an array of 0's and 1's.Example: a) 00110001001110 - Output :3 [Max num of consecutive 1's is 3]b) 1000010001  - Output :1 [Max num of consecutive 1's is 1] 


This question originally appeared [here](https://careercup.com/question?id=5719035229503488).
Tagged with : Amazon,Quality Assurance Engineer,Coding 

### Solution 

```scala
/* 
Classic Kerninghan & Ritchie
Use states 
   - in_1s
   - max_1s 
   - current_count of 1s   
*/
def count_max_1s( arr ){
  // max_1s , in_1s, current_count 
  #(max_1s,in_1s) = fold ( arr, [ 0, false ,0 ] ) -> {
    #(max_1s , in_1s, current_count) = $.p 
    if ( $.o == 0 ){
      if ( in_1s ){
        if ( current_count > max_1s ){
          max_1s = current_count 
        }
        current_count = 0 
        in_1s = false 
      }
    } else {
      current_count += 1
      in_1s = true 
    }
    [ max_1s, in_1s, current_count ]
  } 
  max_1s // return 
}
arr = [0,0,1,1,0,0,0,1,0,0,1,1,1,0]
println ( count_max_1s(arr) )
```

## Q49 
### Problem 
 >How to find total number of greater number after all number in an array ?Eg. Given array is,5 3 9 8 2 6o/p3 3 0 0 1 0 


This question originally appeared [here](https://careercup.com/question?id=6211333377753088).
Tagged with : Walmart Labs,Software Developer 

### Solution 

```scala
// ZoomBA : Dance with it 
def count_bigger_right( arr ){
  arr_len = #|arr|
  rfold( [0: arr_len] , list() ) as {
     cur = arr [ $.o ] 
     num_greater = sum ( [$.o+1: arr_len ] ) as {  arr[$.o] > cur ? 1 : 0 }
     $.p.add ( 0 , num_greater )
     $.p 
  }
}
```

## Q50 
### Problem 
 >Feb On-site Google Print all numbers satisfying the expression 2^i * 5^i (where i, j are integers i >= 0 and j >= 0) in increasing order up to a given bound N. 2^i stands for power(2, i). 


This question originally appeared [here](https://careercup.com/question?id=5529855880855552).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
/* 
aonecode's complexity is O(n) always.
That is kind of ok, because we can make the complexity much smaller.
The factors are 2,5, and thus suppose:
l2 = floor ( log(N,2) )
l5 = floor ( log(N,5) )
Then, all possible 2^i * 5^j are nothing but tuples from 
[0:l2] * [0:l5]
which would be significatly less. Try putting 1 million as N.
(zoomba)log(1000000,2)
19.931568569324174 // Double
(zoomba)log(1000000,5)
8.584059348440359 // Real
(zoomba)
Thus, the operations over loop will be 19 * 8.
It can be well argued that the computation to generate the number 2^i * 5^j 
will be great. Or will it be? We can always compose the number as simply 
of the form ( 10^a * 2^i ) or ( 10^a * 5^j ) , which clearly can be  
string manipulation of padding up powers of 2 or 5 padded with 0's.
This is something that I am not planning to do now. But of course we can 
pre compute it, based on these l2,l5 numbers.

Finally, can we avoid the set? Probably. But do we want to?
A simpler solution is using a sorted set, where we simply add the 
results of teh tuple selected from the ranges [0:l2] * [0:l5]
which, then are already sorted.

It is obvius that we can eliminate sets, by simply 
sorting the result in a list, which, given they are integers and prone to be binary,
can be radix sorted in base 10 in O( l2 * l3 * 10 ) time yielding 
a final complexity : O( log(N,2) * log(N,5) ), 
significantly less than that of O(N).
Space complexity is same in both the cases
Of course we can optimize a bit more, but we rememebr Knuth, 
and stop optimisation before we become evil. 
*/

def sorted_list_till( N ){
  l2 = int( log(N,2) )
  l5 = int( log(N,5) )
  s = sset() // sorted set 
  join ( [0:l2] , [0:l5] ) where {  
    x = ( 2 ** $.0 )  * ( 5 ** $.1 )
    continue ( x > N )
    s += x 
    false // do not add to list  
  }
  println( s )
}
// to prove some point...
sorted_list_till( 100000000 )
```

## Q51 
### Problem 
 >There is a DNA Strand having values as A , T , C , G.All combinations are present in the the file.Write a method which takes starting mutation string , ending mutation string and string bank and calculates the minimum mutation distance required. But the condition is that either of the start or end must be present in the bank.Input:AATTGGCC is starting and TTTTGGCA is ending then mutation distance will be 3.AATTGGCC - TATTGGCC - TTTTGGCC - TTTTGGCA   as it takes three mustaion for start to reach the end string and for this , all intermediate string and final string must be present in the bank. static int findMutationDistance(String start, String end, String[] bank) {    } 


This question originally appeared [here](https://careercup.com/question?id=5072846746288128).
Tagged with : Twitter,Intern,Java 

### Solution 

```scala
// ZoomBA
def is_edit_distance_1( string1, string2 ){
  n1 = #|string1| ; n2 = #|string2| 
  if ( #|n1 - n2| > 1 ) return false 
  if ( n1 == n2 ) return test_same_length( string1, string2 )
  if ( n1 > n2 ) return test_one_more ( string1 , string2 )
  return test_one_more ( string2 , string2 ) 
}

def test_same_length( string1, string2 ){
  n = #|string2|
  ( lfold ( [0:n] , 0 ) ->{
     $.partial += (string1[$.o] == string2[$.o] ? 0 : 1 )  
     break ( $.partial > 1 )
     $.partial 
  } ) == 1 
}

def test_one_more( large, small ){
   n  = #|small|
   first_diff = index( [0:n] ) :: {
      small[ $.o ] != large[ $.o ]
   }
   if ( first_diff < 0 ) return true
   // match the suffixes 
   ! exists ( [ first_diff : n ] ) :: {
       large[$.o+1] != small[$.o]
   }
}
```

## Q52 
### Problem 
 >With given Binary Tree below, traverse the tree and print the tree from bottom up order	1        / \      2   3     /\   /\    4  5 6  7output: 4567231Hint: the traverse  is called "Level Order Tree Traversal"I hope someone memorize this traversal and pass the exam and all these companies judging canditates with one algorithm could think they hired the best canditate. 


This question originally appeared [here](https://careercup.com/question?id=5721669436964864).
Tagged with : Amazon,Senior Software Development Engineer 

### Solution 

```scala
{
  "value" : 1 , "children" : [
    { "value" : 2 , "children" : [ 
       { "value" : 4 , "children" : [] },
       { "value" : 5 , "children" : [] }
    ] } ,
    { "value" : 3 , "children" : [ 
       { "value" : 6 , "children" : [] },
       { "value" : 7 , "children" : [] }
    ] }
  ]
}  /* 
A Level order traversal.
The key is to understand that, 
you need to maintain two queues, one for current,
another for next level.
After a level is exhausted, 
one needs to replace that queue by the one from built up.
As we can see, this won't solve Amazon's problem,
Which is a reverse level order traversal.
So, we need to use a stack to store the per level nodes 
which are to be stored in a queue! Thus, 
The solution becomes , storing the queue in a stack!
But then, who needs queues? We can simply use a list! 
*/
def reverse_level_order( root ){
  cur = list( root ) // current level 
  stack  = list() ; stack.push( list( cur ) )    
  while ( !empty(cur) ){
     next = list() 
     for ( node : cur ){
        for ( child : node.children ){
           next += child 
        }
     }
     // push the level into the stack 
     stack.push ( list( next) )
     cur = next 
  }
  // now unwind the stack 
  while ( !empty(stack) ){
    level = stack.pop()
    for ( node : level ){
      printf( ' %s ' , node.value )
    }
  }
  println()
}
// call it 
reverse_level_order( json( 'tree.json' ,true ) )
```

## Q53 
### Problem 
 >FB on-site two weeks ago last round of coding . (This problem is also in the internal question bank of G.)There is a robot in a room. The initial location of the robot is unknown. The shape or area of the room is unknown.You have a remote that could walk the robot into any of the four directions - up, left, down or right.Here is the move method: boolean move(int direction), direction: 0, 1, 2, 3. If the robot hits the wall, the method returns false.Otherwise returns true and the robot moves on to the direction given.Find out the area of the room. e.g.  XXXX  X  XXXXX         'X' marks the floor plan of the room. A room like such has an area of 10. 


This question originally appeared [here](https://careercup.com/question?id=4902310128910336).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
/* Starting point is (0,0).
Given the new point (x,y) is not already visited,
add 1 to the area. Else fallback. 
Bonus should be, print the Area properly */
// the magical move function 
def move( dir ){ 
    /* making it interesting 
    - so we bias it against expanding out.
    - this is how Einstein figured out brownian motion.
    - spread of the area can be calculated
    - you can easily remove recursion. */
    random(100) > 70  
}
// The function, fully declarative 
def recursion_is_divine( x, y, visited ){
   pos_key = str('%d#%d', x,y)
   /* I have already visited this - no point visiting again */
   if ( pos_key @ visited ){ return; }
   visited += pos_key // set it up
   // move up  
   if ( move( 0 ) ){ recursion_is_divine( x, y+1, visited ) }
   // move down 
   if ( move( 1 ) ){ recursion_is_divine( x, y-1, visited ) }
   // move left 
   if ( move( 2 ) ){ recursion_is_divine( x-1, y, visited ) }
   // move right 
   if ( move( 3 ) ){ recursion_is_divine( x+1, y, visited ) }
}
def find_area( ){
    visited = set()
    recursion_is_divine(0,0,visited)
    // this would have the whole floor plan 
    println('The floor plan:')
    println( visited )
    // this would be the area 
    size(visited)
}
// every time, different results
a = find_area()
println( 'Area is :' + a)
```
```scala
/* Starting point is (0,0).
Given the new point (x,y) is not already visited,
add 1 to the area. Else fallback. 
Bonus should be, print the Area properly */
def Robot : {
    $$ : def(max=100){
        $.max_area = max 
        $.cur_area = 1
    },
    move : def ( dir ){ 
    /* 
     spread of the area can be controlled by a global variable */
     $.cur_area += 1
     $.cur_area < $.max_area && random(true)  
   }
}
robot = new ( Robot )
// The function, with recursion removal 
def find_area(){
    // positions: x#y -> visited 
    positions = dict()
    positions['0#0'] = false
    while ( true ){
        not_traversed = find ( positions ) :: { !$.value }
        break ( not_traversed.nil ) // not found such position
        #(sx,sy) = not_traversed.value.key.split('#')
        positions[not_traversed.value.key] = true // traversed it
        x = int(sx) ; y = int(sy)
        printf('%d,%d\n',x,y)
        if ( robot.move( 0 ) ){ positions[str('%d#%d', x,y+1)] = false }
        if ( robot.move( 1 ) ){ positions[str('%d#%d', x,y-1)] = false }
        if ( robot.move( 2 ) ){ positions[str('%d#%d', x+1,y)] = false }
        if ( robot.move( 3 ) ){ positions[str('%d#%d', x-1,y)] = false }
    }
    println( positions.keys )
    size(positions)
}
// every time, different results
a = find_area()
println( 'Area is :' + a)
```

## Q54 
### Problem 
 >Create a function to calculate the height of an n-ary tree. 


This question originally appeared [here](https://careercup.com/question?id=5762815223660544).
Tagged with : Google,Software Engineer,Trees and Graphs 

### Solution 

```scala
/* 
ZoomBA : Using Level Order Traversal 
stackoverflow.com/questions/1894846/printing-bfs-binary-tree-in-level-order-with-specific-formatting
*/
def traverse(rootnode){
  thislevel = list( rootnode )
  height = 1 
  while ( !empty( thislevel ) ){ 
    nextlevel = fold( thislevel , list() ) ->
      $.p += list( $.o.children )->{ $.o }
    }
    thislevel = nextlevel
    height += 1 
  }
  height 
}
```

## Q55 
### Problem 
 >Round4 Starting from num = 0, add 2^i (where i can be any non-negative integer) to num until num == N. Print all paths of how num turns from 0 to N. For example if N = 4, Paths printed are [0,1,2,3,4], [0,1,2,4], [0,1,3,4], [0,2,4], [0,2,3,4], [0,4]. [0,2,4] is made from 0 + 2^1 + 2^1. [0,1,3,4] from 0 + 2^0 + 2^1 + 2^0 


This question originally appeared [here](https://careercup.com/question?id=5736911233613824).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
def _recurse_(target,path){
  if ( target == path[-1] ){
    println( path )
    return
  }
  diff = target - path[-1]
  power_options = list([0:diff]) as { break( 2 ** $.o > diff) ; $.o } 
  for ( pow_2 : power_options ){
    new_cur = path[-1] + ( 2 ** pow_2 )
    _recurse_ ( target, path + new_cur )
  } 
}

def print_paths(n){
  _recurse_(n,[0])
}
print_paths(4)
```

## Q56 
### Problem 
 >find the isomorphic pairs of string !! a string is said to be isomorphic if its each alphabets can be replaced by another alphabetfor ex "abca" and  "zxyz" are isomorphic but "abca" and "pqrs" is not isomorphicconditions :1 - A character can be replaced by itself. for ex   "abcd" and "pbfg" are isomorphic .2- No two characters can be replaced by same character . for ex "abcd" and "bbcd" are not isomorphic . 


This question originally appeared [here](https://careercup.com/question?id=5685636615897088).
Tagged with : AppPerfect,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
def is_isomorphic( s1, s2 ){
  if ( (l = size(s1))!= size(s2) ) return false 
  m = dict()
  for ( i : [0:l] ){
    if ( s1[i] @ m ){
      y = m[ s1[i] ]
    } else {
      y = m[ s1[i] ] = s2[i]
    }
    if ( y != s2[i] ) return false
  }
  true
}
println( is_isomorphic('','' ))  // true 
println( is_isomorphic('aabaac','xxtxxw' )) // true 
println( is_isomorphic('ac','xxt' )) // false 
println( is_isomorphic('acaa','xxtw' )) // false
```

## Q57 
### Problem 
 >Given input - xyzonexyztwothreeeabrminusseven as a string, return an integer as the sum of all numbers found in the stringOutput -  1 + 23 + (-7) = 17 


This question originally appeared [here](https://careercup.com/question?id=5766700180963328).
Tagged with : Microsoft,Software Engineer 

### Solution 

```scala
// create a name lookup table 
_names_ = { 'zero' : 0 , 'one' : 1 , 'two' : 2 ,
    'three' : 3 , 'four' : 4 , 'five' : 5 ,
    'six' : 6 ,'seven' : 7 ,'eight' : 8 ,
    'nine' : 9 }
// a regex like zero|nine|six|four|one|seven|two|three|five|eight
_int_pattern_ = str( _names_.keys , '|' ) -> { $.o }
// regex (minus)?(zero|nine|six|four|one|seven|two|three|five|eight)+
_base_pattern_ = '(minus)?(' + _int_pattern_ +')+'
// convert the word into integer 
def convert_int( word ){
    sign_num = 1 
    // if the word starts with minus then 
    if ( word #^ 'minus' ){
       sign_num = -1 // change sign 
       word = word[5:-1] // substring 
    }
    // extracts digits by simple mapper - when a word match a digit 
    digits = tokens( word , _int_pattern_ ) -> { _names_[$.o] }
    // convert the digits into integer and multiply by sign 
    int( str(digits,'') ) * sign_num 
}
// the actual function 
def count_nums_as_words_in_string( string ){ 
   // sum of generated integers out of matched tokens 
   sum( tokens( string, _base_pattern_ ) -> { convert_int($.o) })
}
input = "xyzonexyztwothreeeabrminusseven" 
println( count_nums_as_words_in_string( input ) )
```

## Q58 
### Problem 
 >If a string is matched to any filter, it is in the black list, otherwise not.Design a data structure and implement following two functions.addFilter(filter)isInBlackList(string)filters are in the form of“a*b”“abc”“aa*b”having at most one star, which matches 0 or more chars. 


This question originally appeared [here](https://careercup.com/question?id=5651434751131648).
Tagged with : Amazon,Software Engineer 

### Solution 

```scala
ab* ==> ^ab.*$
```

## Q59 
### Problem 
 >Find sets of values in array whose sum is equal to some number. 


This question originally appeared [here](https://careercup.com/question?id=5727449196265472).
Tagged with : Facebook,Research Scientist,Algorithm 

### Solution 

```scala
/* This is how to ZoomBA */
a = [1, 1, 3, 4, 1, 1, 2, 3]
target = 4 
range_a = [0:size(a)].asList
// sequences generates all possible subset of a set 
options = from ( sequences( range_a ), set() ) as {
  // map back the opt to actual
  list( $.o ) -> { a[$.o] }
  // the where clause filter around it 
} where {  sum($.o) == target }
// prints it 
println(options)
```

## Q60 
### Problem 
 >list1 -->aaa,bbb,ddd,xyxz,...list2-->bbb,ccc,ccc,hkp,..list3> ddd,eee,,ffff,lmn,..Inside a list the words are sortedI want to remove words  which are repeated across  the list and print in sorted orderIf the words are repeated in same list its valid.In the above caseit should print aaa-->ccc-->ccc-->eee--->fff-->glk-->hkp-->lmn-->xyxz 


This question originally appeared [here](https://careercup.com/question?id=6298536510488576).
Tagged with : Amazon,Software Engineer,Algorithm 

### Solution 

```scala
l1 = [ 1,1,1,1,1,1,2]
l2 = [1,2]
l3 = [ ]  l1 = [ 1,1,1,1,1,1,2]
l2 = [1,2,2,2,2,2,2]
l3 = [ ]  l1 = [ 1,2]
l2 = [2,3]
l3 = [1,3]
```

## Q61 
### Problem 
 >write a function called reverse to print the reverse of the digit entered by the user. 


This question originally appeared [here](https://careercup.com/question?id=5705453955710976).
Tagged with : HTC Global Services,Software Developer,C 

### Solution 

```scala
// ZoomBA
str_of_n = str( n ) // convert a num into string 
str_of_n ** -1  // reverse a string
```

## Q62 
### Problem 
 >You have rating (0-10) of the hotels per user in this format: scores = [ {'hotel_id': 1001, 'user_id': 501, 'score': 7}, {'hotel_id': 1001, 'user_id': 502, 'score': 7}, {'hotel_id': 1001, 'user_id': 503, 'score': 7}, {'hotel_id': 2001, 'user_id': 504, 'score': 10}, {'hotel_id': 3001, 'user_id': 505, 'score': 5}, {'hotel_id': 2001, 'user_id': 506, 'score': 5} ] Any given hotel might have more than one score. Implement a function, get_hotels(scores, min_avg_score) that returns a list of hotel ids that have average score equal to or higher than min_avg_score.get_hotels(scores, 5) -> [1001, 2001, 3001] get_hotels(scores, 7) -> [1001, 2001] */ How to solve this in C++ and Python? 


This question originally appeared [here](https://careercup.com/question?id=5651833474252800).
Tagged with : Google,Software Engineer 

### Solution 

```scala
// ZoomBA
def get_hotels( scores_file, avg_score ){
  scores = json ( scores_file , true )
  listing = mset ( scores ) -> { $.o.hotel_id }
  avg_listing = dict ( listing ) -> {
     key = $.o.key  
     items = $.o.value 
     total = sum ( items ) -> { $.o.score } 
     [ key, total / float( #|items| ) ] // do average 
  }
  selected_hotels = select ( avg_listing ) :: { 
    $.o.value >= avg_score } -> { $.o.key }
}
```

## Q63 
### Problem 
 >You are given set of strings, you have to print out the could of each distinct patterns. Please consider anagrams as same pattern and even the  char count does not matter.Ex:abbbaabbaabcdabdcadbcaabddcoutput:ab: 3abcd: 4 


This question originally appeared [here](https://careercup.com/question?id=6212618952900608).
Tagged with : Bloomberg LP,Software Engineer / Developer,Algorithm 

### Solution 

```scala
// ZoomBA. enjoy.
a = [ "abbba", "ab", "ba", "abcd", "abdc", "adbc", "aabddc" ] 
m = mset( a ) :: {  str(sset($.o.value),'') }
println( str(m.entrySet(),'\\n') -> { str( '%s:%s', $.key, size($.value) )} )
```

## Q64 
### Problem 
 >Given a list of words with lower and upper cases.   Implement a function to find all Words that have the same unique character set .  For example:May  student  students dog studentssess god  Cat act tab bat flow wolf lambs  Amy Yam balms looped poodle john aliceoutput:may amy  student  students  studentssessdog godcat acttab batflow wolflambs balmslooped, poodle 


This question originally appeared [here](https://careercup.com/question?id=5724734743379968).
Tagged with : Google 

### Solution 

```scala
// ZoomBA 
words = ['May', 'student', 'students', 'dog', 'studentssess', 
'god', 'Cat', 'act', 'tab', 'bat', 'flow', 'wolf', 'lambs', 
'Amy', 'Yam', 'balms', 'looped', 'poodle', 'john', 'alice' ]
// now do magic 
m = mset( words ) -> {  k = $.o.toLowerCase() ; str(sset(k.value) ,'') }
fold ( m ) -> { println( $.value ) }  $ zmb tmp.zm 
[student, students, studentssess]
[tab, bat]
[Cat, act]
[john]
[alice]
[lambs, balms]
[May, Amy, Yam]
[looped, poodle]
[dog, god]
[flow, wolf]
```

## Q65 
### Problem 
 >I was given a questions during an interview which I was not able to solve, please help me in finding the solution.Ques : - Divide the set in two partition such that both the partition has minimum difference of their sum. If we add an element to the left subset during partitioning than the value of that number will automatically increases by 1, but it will not increase by 1 if I add it to the right side. Find the minimum difference between both the subsets : -ex :- {1,2,3,4,5}
 leftSubset = {3,4} , rightSubset = {1,2,5}
 effective sum of leftSubset = 3+4+2(number of elements)
 effective sum of rightSubset = 1+2+5 = 8
 difference of left and right = (9-8)=1 =, min differencesolution : (1,2,3} {4,5} 


This question originally appeared [here](https://careercup.com/question?id=5742035901349888).
Tagged with : Goldman Sachs,Applications Developer,Algorithm 

### Solution 

```scala
def solve( my_list ){
  // 0 is left partition, 1 is right partition 
  // a bit pattern of size(my_list) = n signifies the selection 
  // 0 1 0 1 0  --> pick the 0 for left, 1 for right 
  // 1 2 3 4 5 --> left : [1,3,5], right : [2,4] 
  // this is the bitmap trick 
  n = size(my_list)
  N = 2 ** n 
  // infinity with null, lame!
  min_diff = { 'value' : num('inf') , 'partitions' : null }  
  for ( my_num :  [1:N] ){
    string_rep = str(my_num,2) // in binary 
    // left pad 0 
    string_rep = '0' ** ( n - size(string_rep) ) + string_rep 
    // partition them using when 0 comes left, 1 comes right 
    #(left_items, right_items ) = partition ( [0:n] ) where { string_rep[$.o] == _'0' }
    left_sum = sum ( left_items ) 
    right_sum = sum ( right_items )
    // this is pretty straight forward 
    diff =  #|left_sum - right_sum |
    if ( diff < min_diff.value ){
      min_diff.value = diff 
      min_diff.partitions = [ left_items, right_items]
    }
  }
  min_diff // return 
}

l = [1,2,3,4,5]
md = solve( l )
println( md )
```

## Q66 
### Problem 
 >PUZZLE		Please solve the following puzzle.		Solving Earth		N O R T H +		E A S T +		S O U T H +		W E S T =		E A R T H				There are 10 unique letters in the above puzzle, assign a unique number from 0 to 9 to each letter, so that the equation holds true.  An example of a positive result:				SAMPLE SOLUTION		S = 0, R = 1, W = 2, A = 3, H = 4, N = 5, O = 6, E = 7, T = 8, U = 9				Sample Solution		5 6 1 8 4		7 3 0 8		0 6 9 8 4		2 7 0 8		7 3 1 8 4				REQUIREMENTS		Write a computer program, in any language or code you prefer, that will find and output all the possible combinations or solutions to the above puzzle.  One solution was provided above, you need to find how many correct solutions exist.  Your code should output all the permutations where the equation is solved correctly.  The faster the code executes the better it will be.  A good benchmark is to try and get your code to complete in less than 50ms.  Please bring your code to the interview and expect to walk us through it. 


This question originally appeared [here](https://careercup.com/question?id=5694888686387200).
Tagged with :  

### Solution 

```scala
/*
'NORTH' + 
'0EAST' + 
'0WEST' + 
'SOUTH' 
===========
'EARTH'
2(H + T) % 10 = H // constraint 1 
(2(T + S) + 2(H+T)/5) % 10 = T constraint 2 
Solving these two reduces the problem from 10! into 7! Baam.
That is heavily reduced.
Now let's see how H, T, S are there.
*/
def is_valid( map ){
  north = map.h + 10 * ( map.t  + 10 * ( map.r  + 10* ( map.o + 10 * map.n )))
   east = map.t + 10 * ( map.s  + 10 * ( map.a  + 10* ( map.e ) ) )
   west = map.t + 10 * ( map.s  + 10 * ( map.e  + 10* ( map.w ) ) )
  south = map.h + 10 * ( map.t  + 10 * ( map.u  + 10* ( map.o + 10 * map.s ) ) )
  earth = map.h + 10 * ( map.t  + 10 * ( map.r  + 10* ( map.a + 10 * map.e ) ) )
  earth == ( north + east + west + south )
}

digits = [0:10].asList
// find constraint 1 
c1_pairs = join(digits,digits) where { ($.l != $.r) && ( $.l == 2 * sum($.o) % 10 )  }
// get next set of constrant 2 ?
c2_tuples = join(digits,c1_pairs) where { 
   #(H,T) = $.r 
   S = $.l 
   S !@ $.r && ( ( 2 *( T + S ) + 2*(H+T)/10 ) % 10 == T )
} as { d = { 'h' : $.r.0 , 's' : $.l , 't' : $.r.1 }  }
rest_of_lettrers = [ 'a' , 'e' , 'n', 'o', 'r', 'u' ,'w' ]
// now that we have established the 3 items, 
// rest 7 has to be chosen who are not ... in the group
for ( t : c2_tuples ) {
  other_digits = digits - t.values 
  // now permuatate the other_digits 
  for ( p : perm( other_digits ) ){
    // assign into map 
    d = dict( rest_of_lettrers ) as { [ $.o, p[$.i] ] }
    d |= t // the whole map 
    if ( is_valid( d ) ){
        println( d )
    }
  }
}  {a=1, r=6, s=0, t=8, e=5, u=3, w=7, h=4, n=2, o=9}
{a=2, r=5, s=0, t=8, e=6, u=1, w=7, h=4, n=3, o=9}
{a=3, r=1, s=0, t=8, e=7, u=9, w=2, h=4, n=5, o=6}
{a=3, r=5, s=0, t=8, e=7, u=9, w=2, h=4, n=6, o=1}
{a=0, r=5, s=1, t=6, e=7, u=2, w=4, h=8, n=3, o=9}
{a=3, r=2, s=1, t=6, e=7, u=9, w=4, h=8, n=5, o=0}
{a=3, r=4, s=1, t=6, e=7, u=9, w=0, h=8, n=5, o=2}
{a=9, r=5, s=1, t=6, e=3, u=7, w=4, h=8, n=2, o=0}
{a=9, r=0, s=1, t=6, e=7, u=3, w=2, h=8, n=5, o=4}
{a=3, r=1, s=2, t=5, e=9, u=7, w=6, h=0, n=4, o=8}
{a=4, r=1, s=2, t=5, e=8, u=7, w=6, h=0, n=3, o=9}
{a=7, r=8, s=2, t=5, e=9, u=3, w=4, h=0, n=6, o=1}
{a=8, r=9, s=2, t=5, e=4, u=7, w=6, h=0, n=1, o=3}
{a=8, r=6, s=2, t=5, e=7, u=4, w=1, h=0, n=3, o=9}
{a=9, r=8, s=2, t=5, e=4, u=6, w=7, h=0, n=1, o=3}
{a=9, r=6, s=2, t=5, e=7, u=3, w=8, h=0, n=4, o=1}
{a=9, r=6, s=4, t=1, e=7, u=3, w=0, h=8, n=2, o=5}
{a=2, r=3, s=5, t=8, e=7, u=9, w=1, h=4, n=0, o=6}
{a=2, r=6, s=5, t=8, e=7, u=9, w=3, h=4, n=1, o=0}
{a=2, r=6, s=5, t=8, e=9, u=7, w=1, h=4, n=3, o=0}
{a=3, r=7, s=5, t=8, e=6, u=9, w=1, h=4, n=0, o=2}
{a=6, r=0, s=5, t=8, e=9, u=3, w=1, h=4, n=2, o=7}
{a=7, r=1, s=5, t=8, e=9, u=2, w=6, h=4, n=3, o=0}
{a=9, r=1, s=5, t=8, e=6, u=3, w=7, h=4, n=0, o=2}
```

## Q67 
### Problem 
 >given a string, characters can be shuffled to make a paliandrome.What is the minimum possible number of insertions to original string needed so that it will be a palindrome (after shuffling, if required).InputT -> number of test casesT number of Strings in different linesimport java.util.Arrays;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Xsquare{
	public static void main (String[] args) throws Exception{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int limit = Integer.parseInt(br.readLine());
		
		int [] alphabets = new int[26];
		
		while(limit-- >0){
			String input = br.readLine();
			Arrays.fill(alphabets,0);
			char [] inpChar = input.toCharArray();
			int sum = 0;
			
			for (int i=0;i<input.length();i++){
					int pos = (int)inpChar[i] - (int)'a';
					alphabets[pos]+=1;
			}
			
			for(int i=0;i<26;i++){
				if(alphabets[i]%2==0)
					sum+=0;
				else
					sum+=1; 
			}
			
			if(sum<=0)
				sum=0;
			else 
				sum-=1;
			System.out.println(sum);
		}
	}
}This is the code I submitted online. But it was not accepted as solution. What is the correct approach to this question? 


This question originally appeared [here](https://careercup.com/question?id=5636317922721792).
Tagged with : McAfee,Applications Developer,Coding 

### Solution 

```scala
// ZoomBA
def count_more_char( string  ){
  m = mset ( string.value )
  // find all chars with odd frequency 
  odds = select ( m ) :: { $.o.value % 2 != 0 }
  len = size(odds)
  // every char freq is even means 
  //can be shuffled into a palindrome
  if ( 0 == len ) return 0  
  // because only one odd freq can be allowed 
  //for palindrome, rest needs to be made into even 
  len - 1  
}
```

## Q68 
### Problem 
 >Given a mapping from digits to list of letters and a string of digits of arbitrary length determine all possible ways to replace the digits with letters.Input Mapping = {'1' → ['A', 'B', 'C'],'2' → ['D', 'E', 'F'],...}Avg length of map values - N Input String = “12” - K Expected Output = [“AD”, “AE”, “AF”, “BD”, “BE”, “BF”, “CD”, “CE”, “CF”] 


This question originally appeared [here](https://careercup.com/question?id=5154795351441408).
Tagged with : Facebook,Front End Web Development 

### Solution 

```scala
__mapping__ = {
    1 : [ 'A', 'B', 'C' ], 2 : [ 'D', 'E', 'F' ],
    3 : [ 'G', 'H', 'I' ], 4 : [ 'J', 'K', 'L' ],
    5 : [ 'M', 'N', 'O' ], 6 : [ 'P', 'Q', 'R' ],
    7 : [ 'S', 'T', 'U' ], 8 : [ 'V', 'W', 'X' ],
    9 : ['Y', 'Z'] }

def map_back(n){
    args = list( str(n).value ) as { __mapping__[int($.o)] }
    join(@ARGS = args ) where { true } as { str($.o,'') }
}
println(map_back(12))  zmb tmp.zm
[ AD,AE,AF,BD,BE,BF,CD,CE,CF ]
```

## Q69 
### Problem 
 >Given an array of integers of unknown size, how to reverse the order of the positive integers? Ex [4 3 8 9 -2 6 10 13 -1 2 3 .. ] => [ 9 8 3 4 -2 13 10 6 -1 3 2] 


This question originally appeared [here](https://careercup.com/question?id=5756186739605504).
Tagged with : unknown,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA :: reverse consecutive +ve no within -ve 
def reverse_between_indices( arr , i, j){
   len = ( j - i + 1 )/2 
   lfold( [i : i + len ] ) ->{
      l = arr[$.o]
      arr[$.o] = arr[ j - $.i ]
      arr[ j - $.i ] = l  
   }
}
def reverse_pos( arr ){
   previous = 0
   in_ve = false  
   for( [0:#|arr|] ) {
      continue ( !in_ve && arr[$] < 0 ){
         in_ve = true 
         reverse_between_indices( arr, previous, $ - 1)
         previous = #|arr| - 1 // set to last 
      }
      continue ( in_ve && arr[$] >= 0 ){
         previous = $ 
         in_ve = false
      }
   }
   reverse_between_indices( arr, previous, #|arr| -1 )
}
arr = [4, 3, 8, 9, -2, 6, 10, 13, -1, 2, 3]
reverse_pos ( arr )
println( arr )
```
```scala
// ZoomBA :: reverse consecutive +ve no within -ve :: better style  
def reverse_between_indices( arr , i, j){
   len = ( j - i + 1 )/2 
   lfold( [i : i + len ] ) ->{
      l = arr[$.o]
      arr[$.o] = arr[ j - $.i ]
      arr[ j - $.i ] = l  
   }
}
def reverse_pos( arr ){ 
   end = 0
   while ( true ){ 
      previous = index ( [end:#|arr|] ) :: { arr[$.o] >= 0 } 
      break ( previous < 0 )
      previous += end 
      end = index ( [previous:#|arr|] ) :: { arr[$.o] < 0 }
      break ( end < 0 )
      end += previous 
      reverse_between_indices ( arr, previous, end - 1 )
   } 
   if ( previous >= 0 ){
      reverse_between_indices ( arr, previous, #|arr| - 1 )
   }
}
arr = [4, 3, 8, 9, -2, 6, 10, 13, -1, 2, 3]
println( arr )
reverse_pos( arr )
println( arr )
```

## Q70 
### Problem 
 >Find the anagrams from a list of stringsInput : {"tea", "ate", "eat", "apple", "java", "vaja", "cut", "utc"}Output : {"tea", "ate", "eat","java", "vaja", "cut", "utc"} 


This question originally appeared [here](https://careercup.com/question?id=5687065971785728).
Tagged with : Twitter,Senior Software Development Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
m = mset ( words ) -> {  
  a = $.o.toCharArray ; sorta( a )
  str(a,'') 
}
anagrams = select(m) :: {  #|$.o.value| > 1 } -> { $.o.value }
```

## Q71 
### Problem 
 >For a given string sentence, reverse it.Input : Hello WorldOutput : Dlorw OllehInput: How Are You Doing TodayOutput: Yadot Ginod Uoy Era Woh 


This question originally appeared [here](https://careercup.com/question?id=5195490611167232).
Tagged with : IBM,Software Engineer / Developer,Java 

### Solution 

```scala
// ZoomBA 
// whole string 
def reverse( string , si, ei ){
   fold( [ si : (si + ei + 1)/2 ] ) ->{
     t = string.value[ $.item ]
     string.value[ $.item ] = string.value[ ei - $.index ]
     string.value[ ei - $.index ] = t
   }
}
// If you want to reverse the words, 
def reverse_words( string ){
  reverse( string , 0, #|string| - 1 )
  si = 0 ; ei = 0
  len = #|string|
  while ( true  ){
    while ( si < len && string[si] ==' ' ) { si += 1 }
    break ( si >= len )
    ei = si
    while ( ei < len && string[ei] !=' ' ) { ei += 1 }
    break ( ei >= len ){ reverse( string, si, len-1 )}
    reverse( string, si, ei - 1 )
    si = ei
  }
}
```

## Q72 
### Problem 
 >how to sort a list with dictionary as variables.Sort should be based on dictionary value.for example :a = [{'b':2},{'b':1},{'b':5}{'b':4]now the sort should be based on value. 


This question originally appeared [here](https://careercup.com/question?id=5667895162961920).
Tagged with : Software Engineer in Test,Python 

### Solution 

```scala
(zoomba)x = a = [{'b':2},{'b':1},{'b':5},{'b':4}]
@[ {b=2},{b=1},{b=5},{b=4} ] // ZArray
(zoomba)sorta(x) :: { $.l.b < $.r.b }
true // Boolean
(zoomba)x
@[ {b=1},{b=2},{b=4},{b=5} ] // ZArray
(zoomba)
```
```scala
(zoomba)x = [{'b':2},{'b':1},{'b':5},{'b':4}]
@[ {b=2},{b=1},{b=5},{b=4} ] // ZArray
(zoomba)sorta(x) :: { $.l.entrySet().iterator.next.value < $.r.entrySet().iterator.next.value }
true // Boolean
(zoomba)x
@[ {b=1},{b=2},{b=4},{b=5} ] // ZArray
(zoomba)
```

## Q73 
### Problem 
 >Phone screen Q: String encoding and decoding: Design a method that converts a list of strings into a single string which can be later converted back to the list. 


This question originally appeared [here](https://careercup.com/question?id=5721184525090816).
Tagged with : Google,Software Engineer,Programming Skills 

### Solution 

```scala
a = [  'hi am cool', 'boo hahah' ]
es = str(a,'_') -> { hash( 'e64' , $.o ) }
println ( es )
ds = tokens ( es , '[^_]+' ) -> { hash ( 'd64', $.o )  }
println ( ds )  a = [  'hi am cool', '' , 'boo hahah' , ''  ]
es = str( a , '_' ) -> { empty( $.o ) ?'' : hash( 'e64', $.o )  }
println(es)
ds = list ( es.split('_',-1) ) -> { empty($.o) ? '' : hash('d64', $.o ) }
println ( ds )
```

## Q74 
### Problem 
 >Google full-time phD candidate w/ work experience.Q1. On a 1 meter walkroad, randomly generate rain. The raindrop is 1 centimeter wide.Simulate how raindrops cover the 1 meter [0~1] road. How many drops does it take to fully cover the 1meter?Q2. Find out the maximum number of isosceles triangles you can make from a plane of points(cannot reuse points)Q3.Longest holiday - Every city has a different days of holidays every week. You may only travel to another city at the weekends. What is the max days of holiday you can get this year.Q4.Design merchandising product data storage service 


This question originally appeared [here](https://careercup.com/question?id=5710511673966592).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
// This is discrete simulate 
def discrete_simulate(){
    scale = 10
    count = 0 
    s = set() 
    max = 100 * scale 
    span = max - scale + 1 
    while ( size(s) != max ){
        pos = random(span) 
        for ( [pos:pos + scale ] ){ s+= $ }
        count += 1 
    }
    count // return this 
}  def merge2(slice1, slice2 ){
   if ( slice1.x2 < slice2.x1 || slice2.x2 < slice1.x1 ) { 
       return null // not overlap
   } 
   if ( slice1.x1 <= slice2.x1 ){
      #(min,max) = minmax( slice1.x2 , slice2.x2  )
      return { 'x1' : slice1.x1 , 'x2' : max }
   } else {
      #(min,max) = minmax( slice1.x2 , slice2.x2 ) 
      return { 'x1' : slice2.x1 , 'x2' : max }
   }   
}
def merge_into( new_slice, sorted_slices ){
   if ( empty(sorted_slices) ){
       sorted_slices += new_slice 
       return sorted_slices
   }
   // from left 
   i = index ( sorted_slices ) where { new_slice.x1 <= $.o.x2 }
   if ( i < 0 ){
      sorted_slices += new_slice 
      return sorted_slices
   }
   slice = merge2 ( new_slice, sorted_slices[i] )
   if ( slice == null ){
       sorted_slices.add(i, new_slice )
       return sorted_slices
   }
   sorted_slices[i] = slice 
   if ( i + 1 < size(sorted_slices) ){
       slice = merge2 ( sorted_slices[i], sorted_slices[i+1])
       if ( slice != null ){
           sorted_slices[i] = slice 
           sorted_slices.remove(i+1)
       }
   }
   sorted_slices 
}

def simulate(){
   sorted_slices = list()
   tot = 0.0
   count = 0 
   while ( 1.0 - tot > 0.0001 ){
       pos = random(0.0)
       x1 = pos - 0.005 
       x1 = x1 < 0.0 ? 0.0 : x1 
       x2 = pos + 0.005 
       x2 = x2 > 1.0 ? 1.0 : x2 
       slice = { 'x1' : x1, 'x2' : x2 }
       sorted_slices = merge_into( slice, sorted_slices )
       //println(slice)
       //println(sorted_slices)
       //thread().sleep(10)
       tot = sum( sorted_slices ) as {  $.o.x2 - $.o.x1 }
       count += 1
       //printf('>>>>> Count : %d Total %f %n', count, tot)
   }
   //println( sorted_slices )
   return count // well  
}
```

## Q75 
### Problem 
 >Give a tree (any tree, can be a binary. I told the interviewer that I assume it is binary tree and he said that is fine). Print the tree content on the screen one tree level per linei.e.if a tree is like this:	a       /  \     b    c    /      /   e     fThe output would beabcefToo bad, I was only able to make it print on one line instead of separate line. Until after the interview is over. Then I figure the final answer. 


This question originally appeared [here](https://careercup.com/question?id=5169384622391296).
Tagged with : Cloudera,SDET,Algorithm,Data Structures 

### Solution 

```scala
/* ZoomBA.
Level order traversal.
The key is to understand that, 
you need to maintain two queues, one for current,
another for next level.
After a level is exhausted, 
one needs to replace that queue by the one from built up  
*/

def level_order( root ){
  cur = list( root ) // current level 
  while ( !empty(cur) ){
    // get one and go along with it 
    node = cur.dequeue() 
    printf(' %s ', node.value ) // print it 
    next = list()
    for ( child : node.children ){
       next.enqueue( child ) // queue it 
    }
    println() // completes the level 
    cur = next // switch the level 
  }
}
```

## Q76 
### Problem 
 >You are given a positive integer number and you have to return a boolean telling whether the input number is a tidy number or not. A tidy number is a number whose digits are in non-decreasing order. For example, 1234 is a tidy number, 122334 is also a tidy number but 143567 is not a tidy number. 


This question originally appeared [here](https://careercup.com/question?id=5136136486780928).
Tagged with : FreshoKartz,Software Engineer Intern,Bit Manipulation 

### Solution 

```scala
// ZoomBA
/* tidy no. */
def is_tidy( n ){
  x = n % 10
  y = n / 10
  while ( y != 0 ){
    break ( y % 10 > x )
    x = y % 10 
    y = y / 10 
  }
  return ( y == 0 ) 
}
println( is_tidy( @ARGS[0] ) )
```

## Q77 
### Problem 
 >Given an array of both positive and negative integers , find all pairs whose sum is equal to zero. 


This question originally appeared [here](https://careercup.com/question?id=5921450319937536).
Tagged with : xyz,Software Engineer,Algorithm,Arrays 

### Solution 

```scala
// ZoomBA : non optimal Theta(n^2)
arr = [ -1, 0 , 1, 0 , 2, - 2 , 3 , 10 ] 
r = [0:#|arr|]
pairs = join( r, r ) :: {
  continue ( $.o.0 >= $.o.1 )
  arr[$.o.0] + arr[$.o.1] == 0 
  } -> { [ arr[$.o.0] , arr[$.o.1] ] }
println( pairs )
// ZoomBA : optimal Theta(n) with Theta(n) space 
#(s,l) = lfold( arr , [ set() , list() ] ) ->{
   s = $.p.0 ; l = $.p.1 
   if ( -$.o @ s ){ l.add ( [ $.o, -$.o ] ) ; continue }
   s += $.o 
   $.p = [ s, l ]
}
println(l)
```

## Q78 
### Problem 
 >Delete files of size more than 100mb in a folder which are older  than 90 days. 


This question originally appeared [here](https://careercup.com/question?id=6291013069963264).
Tagged with : Amazon,SDE1,Unix 

### Solution 

```scala
critical_time = time() - 'P30d'
/* Extremely simple */
for( file('my_folder_path') ) { 
  if( $.file && time($.lastModified) < critical_time ){ $.delete }
}
```

## Q79 
### Problem 
 >Assume there are 10000 stars in sky, how would you find which star is closest to the earth? in C 


This question originally appeared [here](https://careercup.com/question?id=5687886394425344).
Tagged with : Software Engineer / Developer,C 

### Solution 

```scala
// ZoomBA
#(min,max) = minmax(stars) :: { 
  $.o.0.distance_from_earth < $.o.1.distance_from_earth 
}
println( min )
```

## Q80 
### Problem 
 >You have a string aaabbdcccccf, transform it the following way  => a3b2d1c5f1ie: aabbaa -> a2b2a2 not a4b2 


This question originally appeared [here](https://careercup.com/question?id=5662443448565760).
Tagged with : Salesforce,Intern,String Manipulation 

### Solution 

```scala
// ZoomBA
def sales_force( s ){
  len = #|s|
  if ( len == 0 ) return ''
  #(last,cnt, ret) = fold ( [1:len] , [s[0], 1, '' ] ) -> {
    #(prev, count, res ) = $.prev
    if ( prev != s[$.item] ){
        res += ( str( prev ) + count )
        count = 1 
    }else{
      count +=1 
    }
    [ s[$.item] , count , res ]
  }
  ret +=  ( str( last ) + cnt )
}
s = 'aaabbdcccccf'
println( sales_force( s ) )
```

## Q81 
### Problem 
 >count the duplicates in a array of strings?? 


This question originally appeared [here](https://careercup.com/question?id=5683991668588544).
Tagged with : Amazon,Testing / Quality Assurance,Algorithm 

### Solution 

```scala
// ZoomBA
mset ( array_of_strings )
```

## Q82 
### Problem 
 >Write a program to find the number of ways can one climb a staircase with 'n' steps, taking one or two steps at a time. 


This question originally appeared [here](https://careercup.com/question?id=5663509909078016).
Tagged with : Skill Subsist Impulse Ltd,Software Developer,C 

### Solution 

```scala
// ZoomBA
$count = 0 
def count_stais( cur,path, max ){
   if ( cur > max ) return 
   if ( cur == max ){
     println( path ) ; $count += 1  
     return 
   }
   count_stais( cur + 1 , path + '->1' , max )
   count_stais( cur + 2 , path + '->2' , max )
} 
count_stais( 0 , '' , 4 )
println ( $count )
```

## Q83 
### Problem 
 >Write a pseudocode for a function that does the following.Given a node in the tree datastructure, it determines whether there exists a path from the given node to a leaf node where the sum of values along the path equals the given sum.signature :boolean dse(Node node, int sum)The following assumptions maybe made1.Each node in the tree may have anywhere from zero to many children2. the three can have any arbitray depth3. each node the tree has a paositive integer value assigned to it.A can have children B,C,DB Children are E,FC children GD Children H,JH Children KJ Children L, MA= 5,B=2, C=3,D=8,E=1,F=4,G=6,H=2,J=1,K=3,L=7,M=8Input and output should bedse(F,4) = true dse(F,1) = falsedse(B,6) = truedse(B,2)= falsedse(b,7) = falsedse(A,14) = truedse(A,22) = truedse(A,29) = false 


This question originally appeared [here](https://careercup.com/question?id=5699018040541184).
Tagged with : Algorithm 

### Solution 

```scala
// ZoomBA : Using Anonymous answer - fixing bugs 
node = {  'value' : 0 , 'children' = list() }

def dse(node, current_sum , target_sum) {
  current_sum  += node.value;
  if ( empty( node.children ) ) {
    // only when the leaf 
    return current_sum == target_sum
  }
  for ( child : node.children) {
    if ( dse(child, current_sum , target_sum) ) {
      return true; // no need to pursue other paths 
    }
  }
  return false
}
```

## Q84 
### Problem 
 >write a program to count number of prime number between two numbers entered by the user 


This question originally appeared [here](https://careercup.com/question?id=5765388680822784).
Tagged with : HTC Global Services,Software Developer,C 

### Solution 

```scala
// ZoomBA
// gitlab.com/non.est.sacra/zoomba/wikis/03-Iteration#prime-number-detection
def count_primes(m,n){
  primes_upto_n = select([2:n+1]) :: { !exists($.p) :: { $.o /? $.$.o } } 
  #|primes_upto_n| -  rindex ( primes_upto_n )  :: {  $.o <= m   } 
}
```

## Q85 
### Problem 
 >Implement delete operation for N-ary tree. Your function should return a list of roots after deletion operation. Notice that your delete function only delete one node instead of a subtree. The delete function takes a list of nodes to be deleted.private class TreeNode {
   int val;
   TreeNode[ ] child;
}

List<TreeNode> delete(TreeNode root, HashSet<TreeNode> set) { } 


This question originally appeared [here](https://careercup.com/question?id=5704108573982720).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
def TreeNode : { 
  val : null , // value 
  child : list() // list of children 
}

def find_parents( cur, nodes , map ){
  // check if cur.child and nodes has intersection ?
  children = cur.child & nodes 
  if ( !empty( children ) ){
    // append to the map : key -> child, value -> parent 
    map |= dict( children ) -> { [ $.o , cur ] }
    nodes -= children // remove parented children 
  } 
  for ( child : cur.child ){
    find_parents ( child, nodes, )
  }
}

def delete( root , nodes ){
  parents = dict()
  // populate parents 
  find_parents ( root, nodes, parents )
  // populated now 
  for ( pair : parents ){
    // remove the child 
    pair.value.child -= pair.key
    // re-parent childs children into parent  
    pair.value.child += pair.key.child 
  }
}
```

## Q86 
### Problem 
 >Define and implement a function that takes two binary numbers represented as strings and returns their sum as another binary number which is again represented as a string. The result should not have any leading zeroes. In case the result is zero, it should be the string "0". Test input "111 1" 


This question originally appeared [here](https://careercup.com/question?id=5738404839948288).
Tagged with : JP Morgan,Applications Developer,Bit Manipulation 

### Solution 

```scala
// ZoomBA
def bin_add(a, b){
  str( int(a,2) + int(b,2) , 2 )
}
```

## Q87 
### Problem 
 >A "derangement" of a sequence is a permutation where no element appears in its original position. For example ECABD is a derangement of ABCDE, given a string, may contain duplicate char, please out put all the derangementpublic List<char[]> getDerangement(char[]){} 


This question originally appeared [here](https://careercup.com/question?id=5643438226669568).
Tagged with : Facebook,SDE1 

### Solution 

```scala
// ZoomBA.
// Basic trick, iterate over permutations, and collect elements 
s = 'abcdc'
len = size(s)
sa = s.toCharArray 
sorta(sa) // the base template 
// iterate and select over all permutation indices  
// specify the collector - classic example of from syntax 
deranged = from ( perm( [0:len].asList ) , set() ) :: {
  // when no element exists such that there is same char in place 
  !exists( $.o ) :: { sa[ $.o ] == s[ $.i ] } 
  // finally map permutation index back to the string 
} -> { str( $.o ,'' ) -> { sa[ $.o ] } }
println( deranged )
```

## Q88 
### Problem 
 >Given a comma separated file print the last but one column of every line.e.g:a,b,c,d,e,f1,2,3,4w,x,y,zoutput should bee3y 


This question originally appeared [here](https://careercup.com/question?id=5751993332137984).
Tagged with : Amazon,SDE1,Unix 

### Solution 

```scala
/* 
First - the standard way 
*/
for( file('my_csv_file.csv') ) { 
  x = $.split(',') 
  println( size(x) > 1 ? x[-2] : '' ) 
}
/* The shell way:
unix.stackexchange.com/questions/17064/how-to-print-only-last-column
 */
```

## Q89 
### Problem 
 >Given an array of numbers, for every index i, find nearest index j such that a[j] > a[i].If such an index doesn’t exist for i, ­1 should be printed. 


This question originally appeared [here](https://careercup.com/question?id=5757497241501696).
Tagged with : Uber,SDE1,Algorithm,Data Structures 

### Solution 

```scala
// ZoomBA :: Simple but non-optimal way to solve it
def find_nearest ( arr , i ){
  li = index ( [0:i] ) :: { arr[ $.o ] > arr[i] }
  ri = rindex ( [i+1: #|arr|] ) :: { arr[ $.o ] > arr[i] }
  if ( li == -1 && ri == -1 ) return -1
  if ( li == -1 ) return ri 
  return li 
}
lfold ( arr ){  
  printf ('%d -> %d\n', $.i, find_nearest(arr, $.i))  
}
```

## Q90 
### Problem 
 >Write a program to get a string and to convert the 1st letter of it to uppercase ? 


This question originally appeared [here](https://careercup.com/question?id=5103608778457088).
Tagged with : HTC Global Services,Software Developer,C++ 

### Solution 

```scala
// ZoomBA
     s.value[0] = (str(s[0]).toUpperCase)[0]
```

## Q91 
### Problem 
 >write a program to implement following logicIf the price of the book in between 100 to 500 book is “Normal Book”If the price is between 501 and 5000 book is “Costly book”If greater than 5000 than book is “precious book” 


This question originally appeared [here](https://careercup.com/question?id=5709299897597952).
Tagged with : HTC Global Services,Software Developer,C 

### Solution 

```scala
// ZoomBA
def book_type(price){
     if ( 100 <= price && price <= 500 ) return "Normal Book"
     if ( 501 <= price && price <= 5000 ) return "Costly Book"
     if ( price >  5000 ) return "Precious Book"
     return "Unknown Variety" 
}
```

## Q92 
### Problem 
 >Given a string, add some characters to the from of it so that it becomes a palindrome. E.g.1) If input is "abc" then "bcabc" should be returned. 2) input -> "ab" output -> "bab" 3) input -> "a" output -> "a" 


This question originally appeared [here](https://careercup.com/question?id=5708537557680128).
Tagged with : Bloomberg LP,Software Engineer / Developer,Algorithm 

### Solution 

```scala
x + ( x ** -1 ) // is the answer
```

## Q93 
### Problem 
 >Given a list of files. Return all the unique lines from all files. 


This question originally appeared [here](https://careercup.com/question?id=5183998058823680).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
def get_all_unique( files ){
  from( files, set() ) -> {
    read($.o,true)
  }
}
// cool?
x = get_all_unique( [ 'samples/scratch.zm' , '/Codes/zoomba/wiki/tmp.zm' ]  )
println(x)
```

## Q94 
### Problem 
 >A number is special if it is possible to remove some digits from it to get a number having 3, 5 or 6 only.For example, 38597 is special since it is possible to remove digits 8, 9, 7 to get 35. You cannot remove all the digits.You can remove digits from left, right or middle.Write a program in C which given a number N, calculate how many divisors of N are special 


This question originally appeared [here](https://careercup.com/question?id=5849698404401152).
Tagged with : .Net/C 

### Solution 

```scala
N -> [0-9]+  N --> [0-9]* [0-9]? [0-9]*  special_digits = '356'.toCharArray
all_digits = ['0':'9'].string.toCharArray 
drop_digits = all_digits - special_digits

def is_special_no( n ){
  // set minus : to get the left over digits after subtracting drop_digits
  left =  set ( str(n).toCharArray ) - drop_digits
  // when left is non empty and is a proper subset of special_digits then true 
  !empty(left) && left <= special_digits
}

println( is_special_no(104) )  special_digits = set ( '356'.toCharArray )
def is_special_no( n ){
  !empty ( select ( str( n ).toCharArray ) where { $.o @ special_digits } )
}

println( is_special_no(134) )
```

## Q95 
### Problem 
 >Consider that your office provides an app to book meeting rooms. You provide the start and end time of the meeting. The app list the available rooms for that slot and you select a room and confirm your booking.All meeting happen between 9am - 6pm.Write a method for getAvailableRooms(startTime, endTime). Use appropriate data structures. 


This question originally appeared [here](https://careercup.com/question?id=5688860014018560).
Tagged with : Amazon,SDE-3,Algorithm 

### Solution 

```scala
// ZoomBA
//gitlab.com/non.est.sacra/zoomba/wikis/09-samples#book-meeting-room

// find meeting rooms 
def find_meetin_rooms( start_time, end_time ){
  possible_rooms = dict( rooms ) :: {
      room = $.o
      continue ( empty(room.schedules) ) { [ room.name , 0 ] } 
      continue( end_time <= room.schedules[0][0] ){  [ room.name , 1 ]  }
      // only when more than 1 at least 
      sorta(room.schedules) :: { $.o.0 < $.o.1 } 
      pos = index ( [1: #|room.schedules|] ) :: {  cur_inx = $.o 
          room.schedules[cur_inx-1].1 <= start_time && end_time <= room.schedules[cur_inx].0  
      }
      continue ( pos < 0 )
      [ room.name , pos ]
   } 
}
```

## Q96 
### Problem 
 >Write a Java Program in which a class takes four integer arguments as input(a, b, c and d). Do addition of (a+b) on one thread, addition of (c+d) on another thread and multiplication of(a+b) * (c+d)) on main thread.Like: Thread1 = (a+b)Thread2 = (c+d)Main Thread = (Thread1 * Thread2) 


This question originally appeared [here](https://careercup.com/question?id=5690995132858368).
Tagged with : JP Morgan,Senior Software Development Engineer,Java 

### Solution 

```scala
/*  
 This is where other we seriously do some awesome stuff  
*/
def do_non_sense_using_thread(a,b,c,d){
  t1 = thread()->{ println('I am in thread with id :' +  $.i) ; a + b }
  t1.join()
  t2 = thread()->{ println('I am in thread with id :' +  $.i) ; c + d }
  t2.join()
  t1.value * t2.value 
}
println ( do_non_sense_using_thread( 1,2,3,4) )
/* The result : LOL  
I am in thread with id :10
I am in thread with id :11
21
*/
```

## Q97 
### Problem 
 >You know result of a soccer match, print all the possible ways that this game ends up with this result.Example: final score 1 - 1:0 - 0 0 - 11 - 10 - 01 - 01 - 1Another example if the final score is 2 - 3 there are many possibilities for reaching to that score:2 - 30 - 01 - 02 - 02 - 12 - 22 - 30 - 01 - 01 - 11 - 22 - 22 - 30 - 00 - 10 - 20 - 3... 


This question originally appeared [here](https://careercup.com/question?id=5640253559799808).
Tagged with : unknown,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA : Showcasing the recursion. Minimal, and effective.
def traverse(path, destination){
   current = (path.split("#"))[-1] 
   if ( current == destination ){
      println ( path.replace('#', '->') )
      return 
   }
   #(l,r) = current.split(':')
   #(L,R) = destination.split(':')
   if ( int(l) > int(L) || int(r) > int(R) ){ return }
   // notice conformance : left casting of integer from string 
   traverse ( str('%s#%s:%s' , path , l , 1 + r ) , destination )
   traverse ( str('%s#%s:%s' , path , 1 + l , r ) , destination )
}
traverse ( '0:0' , '2:2' )
```

## Q98 
### Problem 
 >You get a password and a mapping for each character of the password. Print out all the possible mutations for the password.E.g. password: "pass"mapping:p-> $p-> Pa-> As-> /s-> Ss-> &Possible mutations would be for example "PASS" or "$A&S" 


This question originally appeared [here](https://careercup.com/question?id=5730743486513152).
Tagged with :  

### Solution 

```scala
mapping = { "p" : [ "$", "P" ],
            "a" :  [ "A" ], 
            "s" :  [ "/" ,"S" , "&" ] }

def solutions(word, mapping){
  arg = list( word.value ) -> { mapping[str($.o)] }
  join(@ARGS=arg) -> { str($.o,'') }   
}
println( solutions('pass', mapping) )  careercup git:(master) ✗ zmb tmp.zm
[ $A//,$A/S,$A/&,$AS/,$ASS,$AS&,$A&/,$A&S,$A&&,PA//,PA/S,PA/&,PAS/,PASS,PAS&,PA&/,PA&S,PA&& ]
➜  careercup git:(master) ✗
```

## Q99 
### Problem 
 >find first not-repeating character by iterating through the length of the string only once and by using constant space. 


This question originally appeared [here](https://careercup.com/question?id=5672914578833408).
Tagged with : Amazon,SDET,Algorithm 

### Solution 

```scala
// ZoomBA
def first_non_repeat(string){
  // use sorted dictionary to keep insertion order
  d = fold ( string.value , sdict() ) ->{
    if ( $.item @  $.partial ){ $.partial[ $.item] += 1 
      } else { $.partial[ $.item] = 1 } 
    $.partial
  } // this is now an multi set : search for first entry with count 1
  r = find ( d ) :: { $.item.value == 1 }
  // Option Monad, either exists or does not 
  println ( r.nil ? 'Nothing' : r.value.key )
}
first_non_repeat ( "aaaabbbbacccdbbbbeeeg" )
```

## Q100 
### Problem 
 >You are given an array of integers. Write an algorithm that brings all nonzero elements to the left of the array, and returns the number of nonzero elements. The algorithm should operate in place, i.e. shouldn't create a new array.The order of the nonzero elements does not matter. The numbers that remain in the right portion of the array can be anything. Example: given the array [ 1, 0, 2, 0, 0, 3, 4 ], a possible answer is [ 4, 1, 3, 2, ?, ?, ? ], 4 non-zero elements, where "?" can be any number. Code should have good complexity and minimize the number of writes to the array. 


This question originally appeared [here](https://careercup.com/question?id=5682803648757760).
Tagged with : Facebook,Solutions Engineer,Coding 

### Solution 

```scala
a = list( [ 0:10 ] ) -> { random(true)? 0 : random(10) + 1 }

def move_0_right(arr){
  first_zero = 0 
  last_non_zero = size(arr) - 1 
  while ( true ){
    while ( arr[last_non_zero] == 0 ) { last_non_zero -= 1 }
    while ( arr[first_zero] != 0 ) { first_zero += 1 }
    break( first_zero > last_non_zero )
    arr[first_zero] = arr[last_non_zero]
    arr[last_non_zero] = 0
  }
  last_non_zero + 1 
}
println(a)
x = move_0_right(a)
println(a)
println(x)
```

## Q101 
### Problem 
 >Given a series of parenthesis and bracket with the possible pairs of (), {}, [], write a method that will return true if the series is balanced, and false otherwise.for example i.e. "()" => true "({})" => true"([]()){}" => true"([)]{}" => false, the square bracket '[' does not have a matching closing bracket ']' 


This question originally appeared [here](https://careercup.com/question?id=5651818618028032).
Tagged with : Algorithm 

### Solution 

```scala
// ZoomBA
config = { '(' : 0 , '{' : 0 , '[' : 0 }
match = { ')' : '(' , '}' : '{' , ']' : '[' }
lfold( string.value ) -> {
  c = str($.o) 
  continue ( c @ config ){  config[c] += 1  }
  continue ( !(c @ match) ) 
  config[ match[c] ] -= 1 
  break ( config[ match[c] ] < 0 )
} 
!exists ( config ) :: { $.o.value != 0 }
```

## Q102 
### Problem 
 >Given a singly linked list of integers, write a function in java that returns true if the given list is palindrome, else returns false 


This question originally appeared [here](https://careercup.com/question?id=5072809853190144).
Tagged with : Microsoft,Intern,Java,Linked Lists 

### Solution 

```scala
1 -> 2 -> 3 -> 42 -> 3 -> 2 -> 1  1#2#3#42#3#2#1
```

## Q103 
### Problem 
 >Find the first N prime numbers where N is a positive integer.Note: This question is to find all first N prime numbers, but not determinate whether N is prime or not. 


This question originally appeared [here](https://careercup.com/question?id=5662895208660992).
Tagged with : Algorithm 

### Solution 

```scala
primes_upto_n = select( [2 : n  +1] , set() ) where { 
   !exists( $.partial ) where { $.$.o % $.o == 0  }  
}
```

## Q104 
### Problem 
 >Imagine a man reading a book.He can perform only 2 possible actions of reading: 1) read a page in a minute (careful reading), 2) read two pages in a minute (look through).Nothing else is permitted.Calculate the number of all possible combinations of book reading ways with given number of pages.Example: given 3 pages.Answer is 3 combinations, as follows: 1st: Careful reading (1) - careful reading (1) - careful reading (1),2nd: Careful reading (1) - look through (2),3rd: Look through (2) - careful reading (1). 


This question originally appeared [here](https://careercup.com/question?id=5726654544478208).
Tagged with : SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA calculating Fibonacci( n )  :: beat that  
#(l,s) = lfold([2:n+1] , [1,0] ) ->{
   [ $.p.0 + $.p.1 , $.p.1]
}
println( l ) // n'th fib
```

## Q105 
### Problem 
 >You are given a scrambled input sentence. Each word is scrambled independently, and the results are concatenated. So:'hello to the world'might become:'elhloothtedrowl'You have a dictionary with all words in it. Unscramble the sentence. 


This question originally appeared [here](https://careercup.com/question?id=5732347262533632).
Tagged with : Google,Site Reliability Engineer,String Manipulation 

### Solution 

```scala
// ZoomBA : Imperative solution, because people may not like the declarative one
/* 
 Two phases to do it.
 1. Generate all possible splitting - partition of the input string
 2. Given a partition, sort the individual words in the split 
 3. Check if all the words belongs to the dictionary by matching them 
 4. Against a pre word sorted dictionary  
*/
def do_googles_bidding( string , sorted_dict ){
  len =  #|string| 
  for ( n : [ 0 : 2 ** (len - 1)] ){
     sn = str(n,2)
     sn = '0' ** ( len - #|sn| ) + sn // 0 padded 
     end = len - 1 ; start = end 
     partitions = list()
     failed = false 
     while ( start >= 0 ){
        if ( sn[start] == _'1' ){
           p = string[start:end]
           // generate the key by sorting chars, and checking 
           lc = p.toCharArray() ; sorta(lc) ; lc = str(lc,'')
           break ( !(lc @ sorted_dict) ) { failed = true }
           partitions.add(0, lc ) // add the sorted key 
           end = start - 1 
        }
        start -= 1 
     }
     if ( !failed && end >= 0 ){
       p = string[0:end]
       lc = p.toCharArray() ; sorta(lc) ; lc = str(lc,'')
       if ( lc @ sorted_dict ) { 
          partitions.add(0, lc )
          println( partitions )
          return partitions 
       }
     }
  }
  return []
}
// generate the sorted dict 
sorted_dict = mset ( read( '/usr/share/dict/words' ) ) -> { 
  lc = $.o.toLowerCase() ; sorta( lc.value ) ; lc }
result = do_googles_bidding( 'elhloothtedrowl' , sorted_dict )
println ( result )
```
```scala
/* 
 1. Start with the substring from start which is anagram of a word
 2. Now match recursively the rest
 3. Test if everyone can generate a non empty  word list or not
 Doing it recursively, push to stack if need be : string, words_until_now 
 */
def do_googles_bidding( string , sorted_dict , words_until_now ){
   for ( end : [0:#|string|] ){ // making it non trivial 
      s = string[0:end] // splice 
      sorta( s.value ) // sort and gen key 
      if ( s @ sorted_dict ){
         if ( end == #|string| - 1 ){ // in this case, we have found a match 
          // note : I am not translating back the actual words, that is trivial 
          println ( words_until_now + ',' + s ) ; return }
          // now, do on the suffix string 
         do_googles_bidding( string[end+1:-1] , sorted_dict ,  words_until_now  + ',' + s )
      }
   }
}
// generate the sorted dict 
sorted_dict = mset ( file( '/usr/share/dict/words' ) ) -> { 
  lc = $.o.toLowerCase() ; sorta( lc.value ) ; lc }
do_googles_bidding( 'elhloothtedrowl' , sorted_dict , '')
```

## Q106 
### Problem 
 >Given numbers 1 through 52, take 5 unique numbers and determine if the number 42 can be made using any combination of addition (+), subtraction (-), multiplication (*), and division (/) on those 5 numbers 


This question originally appeared [here](https://careercup.com/question?id=5166049396785152).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/* 
Step 1: Pick 5 numbers from 1...52. That is al combinations of C(52,5)
achivable using comb([1:53],5) 
Now, pick any of these 4 ops [ +,-,*,/] on these 5 numbers.
Use floating point to be exact
Each permutation has then 4^4 ways of assigning operators.
stringify it, do an eval.
Check it out if result is 42
*/
__OPS__ = { _'0' : '+' , _'1' : '-' , _'2' : '*' , _'3' : '/' }

def do_facebook(){
   total_ops = 4 ** 4 
   // create the operator varities 
   ops = list()
   for ( k : [0:total_ops] ){
       s = str(k,4) 
       s = '0' ** (4 - size(s)) + s // left pad by '0'
       ops += s
   } 
   // select tuple from C(52,5) :: for demo.. we have to 8 
   for ( nums : comb( [1:8] , 5 ) ){
       // select operators.... 
       for ( op : ops ){
          s = ''
          for ( i = 0; i < 4; i+= 1 ){
             s += ( str(nums[i]) + '.0' +  __OPS__[ op[i] ] ) 
          }
          s += ( str(nums[4]) + '.0' )
          // do eval ?
          x = #'#{s}' // yes
          if ( x == 42.0 ) {
              // the answer to life, universe and everything...
              println(s)
          }
       }
   }
}

do_facebook()
```

## Q107 
### Problem 
 >Write a program to calculate the following i want a c++program for this condition 1+4+9+16+….+100 


This question originally appeared [here](https://careercup.com/question?id=5666558731878400).
Tagged with : HTC Global Services,Software Developer,C++ 

### Solution 

```scala
#include <iostream>

unsigned long  calculate_series(unsigned long n){
   unsigned long count = 0 ;
   for ( unsigned long i = 1 ; i <= n ; i++ ){
      count += i * i ;
   }
   return count ;
}

int main() {
    std::cout << calculate_series(100) << std::endl;
    return 0;
}
```
```scala
// ZoomBA
sum ( [1:n+1] ) -> { $.item ** 2 }
```

## Q108 
### Problem 
 >Write code for the following: given a text file containing this information (Date the customer is logged in, tab, customer id)04/11/2017 /t 000304/12/2017 /t 000304/13/2017 /t 000404/13/2017 /t 0003How to get the list of those customers that log in on three consecutive days. 


This question originally appeared [here](https://careercup.com/question?id=5140007057620992).
Tagged with : Amazon,Software Engineer 

### Solution 

```scala
/*
 This is the one which should do it
*/
def has_3_consecutive( dates ){
  if ( size(dates) < 3 ) return false
  ld = list(dates)
  // in long form 3 consecutive date are (D - d), D, (D + d) 
  exists([2: size(dates)]) where { 2 * ld[$.o -1 ] == ( ld[$.o-2] + ld[$.o] )  } 
}

ms = mset( file( 'logfile.txt' ) ) as { #(date,key) = $.o.split('\t') ; key }
users_cons = select( ms ) where {
  dates = sset ( $.o.value ) as {  #(date,key) = $.o.split('\t') ;  int( time(date,'MM/dd/yyyy') ) } 
  has_3_consecutive( dates )
} as { $.o.key }
```

## Q109 
### Problem 
 >write a program to add all the non-diagonal elements of a 2D array. 


This question originally appeared [here](https://careercup.com/question?id=5744765321609216).
Tagged with : HTC Global Services,Software Developer,C 

### Solution 

```scala
// ZoomBA
def add_non_diag( arr2d ){ 
   num_rows = size( arr2d )
   num_cols = size ( arr2d[0] )
   sum = 0 
   for ( i = 0 ; i < num_rows ; i += 1 ){
      for ( j = 0 ; j < num_cols ; j += 1 ){
          continue(i==j)
          sum += arr2d[i][j]
      }
   }
   return sum 
}
```

## Q110 
### Problem 
 >Write a program that takes an integer and prints out all   ways to multiply smaller integers that equal the original number, without repeating sets of factors. In other words, if your output contains 4 * 3, you should not print out 3 * 4 again as that would be a repeating set. Note that this is not asking for prime factorization only. Also, you can assume that the input integers are reasonable in size; correctness is more important than efficiency. Eg: PrintFactors(12) 12 * 1 6 * 2 4 * 3 3 * 2 * 2 


This question originally appeared [here](https://careercup.com/question?id=5759894012559360).
Tagged with : Linkedin,Software Developer,Algorithm 

### Solution 

```scala
// ZoomBA
def print_factors(n, factors){
  if ( n == 1 ){
    println( str( factors + 1 , '*' ) ) ; return 
  }
  last = empty( factors )?  n : factors[-1]
  for ( f : [last:1] ){
     if ( f /? n ){
        print_factors ( n/f , factors + f )
     }
  }
}
```

## Q111 
### Problem 
 >A company's organizational structure is represented as 1: 2, 3, 4 In the above employees with id 2, 3 and 4 report to 1Assume the following hierarchy.1: 2, 3, 43: 5, 6, 75: 8, 9, 10Given an employee Id, return all the employees reporting to him directly or indirectly 


This question originally appeared [here](https://careercup.com/question?id=5638836259389440).
Tagged with : Bloomberg LP,Senior Software Development Engineer,Coding 

### Solution 

```scala
org_map = { 1: [2, 3, 4], 
            3: [5, 6, 7], 
            5: [8, 9, 10] }

def find_all_minions( gru ){
  // gru before the minions ?
  if ( !(gru @ org_map) ){ return [] }
  minions = list()
  // cool, let gru get's his groove back 
  despicable_me = list ( org_map[gru] )
  while ( !empty(despicable_me) ){
    // yes, bob was always faithful 
    bob = despicable_me.dequeue()
    if ( bob @ org_map ){
      // enqueue all the lesser minions
      despicable_me += org_map[bob]
    }
    minions += bob // add bob the minions 
  }
  minions // return value  
} 
printf( '%s -> %s %n', 1, find_all_minions( 1 ) )
printf( '%s -> %s %n', 2, find_all_minions( 2 ) )
printf( '%s -> %s %n', 3, find_all_minions( 3 ) )
```

## Q112 
### Problem 
 >Given a string return the longest palindrome that can be constructed by removing or shuffling characters.Example:'aha'  -> 'aha''ttaatta' -> ' ttaaatt''abc' -> 'a' or 'b' or 'c''gggaaa' -> 'gaaag' or 'aggga'Note if there are multiple correct answers you only need to return 1 palindrome. 


This question originally appeared [here](https://careercup.com/question?id=5631060781039616).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
def max_palindrome( string ){
  // create a mapping of char -> count
  cb = mset ( string.value )
  #(m,M) = minmax( cb ) :: {
    #(l,r) = $.item ; (2 /? l.value) || (l.value < r.value) }
  middle = (2 /? M.value) ? '' : str( M.key )
  // generate palindrome by appending v to left and right
  fold ( cb , middle ) -> {
    // no need to take care of even n odd because, (2x+1)/2 -> x anyways
    v = str( $.item.key ) ** ($.item.value /2 )
    v + $.partial + v
  }
}
s = 'akjhadajsoadjaoiwjewaodjaosid'
println ( max_palindrome( s ) )
```

## Q113 
### Problem 
 >Giving a string and an string dictionary, find the longest string in dictionary which can formed by deleting some characters of the giving string.eg:S = abpcplea, Dict = {ale, apple, monkey, plea}, the return "apple"I was thinking of the following approach,Build a Trie for all words in the Dict - O( n * k) where k is the longest string in the dictFor each character c, in S check if there is a word in Trie that starts with it and has the letters that appear in S after c. We can short circuit based on remaining characters and the length of longest string found so far.This should take O( N * k) where N is length of S. 


This question originally appeared [here](https://careercup.com/question?id=5757216146587648).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
def find_max_len( words, S ){
  max_len = 0
  result = ''
  for ( w : words ){
    // list contains comparision
    if ( w.value <= S.value && size(w) > max_len ){
      result = w ; max_len = size(w)
    }
  }
  return result
}
S = 'abpcplea'
words = set( 'ale', 'apple', 'monkey', 'plea' )
println ( find_max_len(words,S) )
```

## Q114 
### Problem 
 >You are given a positive integer number and you have to return the greatest smaller tidy number of this input number. If the input number itself is tidy, then, it become the answerExampleInput: 1234output: 1234input: 100output: 99input 143456output: 139999.PS.A tidy number is a number whose digits are in non-decreasing order. 


This question originally appeared [here](https://careercup.com/question?id=5640873527214080).
Tagged with : FreshoKartz,Software Engineer Intern,Bit Manipulation 

### Solution 

```scala
def find_unoptimal( n ){ 
   while( !is_tidy(n) ){ n -= 1 }
   n // return   
}
```
```scala
/* 
The find next lower tidy number is 
not a classically trivial one.
Obvious trivial solution is :

def find_unoptimal( n ){ while( !is_tidy(n) ){ n -= 1 } }

But that is a mess.
A better solution will be:
1. Search for inversion of order ( d[i] > d[i+1] ) from left.
2. If on the inversion, we can down the digit:
   Down(x) = x - 1 
   and d[i-1] < d[i] - 1 then we can down the digit, replace rest by 9. 
3. If we can not do that ( 12222334445555123 ), 
   we search for a digit change from right: ( d[i]<d[i+1] ) 
   where we can change d[i+1] to d[i] and replace all right digits by 9
*/

def find_last_tidy( n ){
  sn = str(n)
  l = size(sn)
  i = index( [0:l-1] ) :: { sn[$.o] > sn[$.o+1] }
  if ( i < 0 ) return n // this is first step 
  // is there a left digit? if not... 
  if ( i == 0 ) return int( '' + ( int(sn[0]) - 1 ) + ( '9' ** (l - 1) ) )
  // there is 
  // if it is like 12222334445555|123 ?
  // then we need to check j is the repeat size 
  j = index( [i:-1] ) :: { sn[$.o] != sn[i] } 
  ns = sn[0:i-j] + ( int(sn[i]) - 1 ) + ( '9' ** (l - i + j - 2 ) ) 
  int ( ns )
}
println( find_last_tidy(@ARGS[0]) )
```

## Q115 
### Problem 
 >We need a functionality to block an user who makes more than 10 requests in last 5 minutes. You need to implement the following function.{{boolean block_user(int user_id){//TODO: return true in case u want to block user}}} 


This question originally appeared [here](https://careercup.com/question?id=5140513473691648).
Tagged with : anonymous,SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA
def block_user( user_id ) {
    user_session  = session[ user_id ] 
    if (  size ( user_session.requests ) < 10 ) return false 
    last_10 =  user_session.requests[-10:-1]  // splice the last 10 requests 
   // ZoomBA time subtraction gives millisecs 
    return  ( last_10[-1].call_time - last_10[0].call_time  < 300000  )
}
```

## Q116 
### Problem 
 >Given an input string "aabbccba", find the shortest substring from the alphabet "abc".In the above example, there are these substrings "aabbc", "aabbcc", "ccba" and "cba". However the shortest substring that contains all the characters in the alphabet is "cba", so "cba" must be the output.Output doesnt need to maintain the ordering as in the alphabet.Other examples: input = "abbcac", alphabet="abc"  Output : shortest substring = "bca". 


This question originally appeared [here](https://careercup.com/question?id=5753530796212224).
Tagged with : Facebook,Software Engineer,String Manipulation 

### Solution 

```scala
/* 
The idea is to isolate all pair of (start,end) indices
such that : length is >= alphabet
sort ascending by length   
then find first match - that will ensure shortest.
There are other alternatives.
just find pairs, ignore all which are less than size of alphabet, and keep a min.
This avoids the sorting.   
*/
def find_min_alpha_sub_str( string , alphabet ){
  r = [0:size(string)]
  max_len = size(alphabet)
  pairs = join ( r , r ) :: { $.o.1 - $.o.0 + 1 >= max_len }
  fold( pairs , string ) :: {  
    s = string[$.o.0:$.o.1]
    // ignore when not a sub set or current size is >= min size 
    continue ( !( alphabet.value <= s.value && size(s) < size($.p) ) )
    $.p = s // got the min one 
  }
}
s = find_min_alpha_sub_str ( "abbcac", "abc" )
println( s )
```

## Q117 
### Problem 
 >Write a method to count the number of 2s between 0 and n.* 


This question originally appeared [here](https://careercup.com/question?id=5688041801777152).
Tagged with : Amazon,Software Developer,Coding 

### Solution 

```scala
//ZoomBA
def count_2( n ){
   sum( [2:n+1] ) ->{
      sum ( str($.o).value ) ->{ $.o == '2' ? 1 : 0 }
   }
}
```

## Q118 
### Problem 
 >Given a list of integers (array or list), write a function that returns true if the list can be split into two lists that have an equal sum.Example: {4,2,2,0,-1, 1} returns true {4}, {2,2,0,-1,1}and {3,3,1} returns false.Hints by interviewer:- Complexity is 2^n 


This question originally appeared [here](https://careercup.com/question?id=5095048703115264).
Tagged with : Microsoft,SDE-3,Algorithm 

### Solution 

```scala
// ZoomBA
l = [ 4, 2, 2, 0, -1, 1 ]
possible = exists ( [1: #|l|] ) :: {
   inx = $.o 
   sum( l[0:inx-1] ) == sum ( l[inx:-1] )
}
println( possible )
```

## Q119 
### Problem 
 >int bits(unsigned char v);Which returns number of set bits in v.A) Optimize for memory usage:B) Optimize for speed: 


This question originally appeared [here](https://careercup.com/question?id=5636545181646848).
Tagged with : Fortinet,Software Engineer / Developer,Algorithm 

### Solution 

```scala
1  Initialize count: = 0
   2  If integer n is not zero
      (a) Do bitwise & with (n-1) and assign the value back to n
          n: = n&(n-1)
      (b) Increment count by 1
      (c) go to step 2
   3  Else return count
```

## Q120 
### Problem 
 >/*# There's a room with a TV and people are coming in and out to watch it. The TV is on only when there's at least a person in the room.# For each person that comes in, we record the start and end time. We want to know for how long the TV has been on. In other words:# Given a list of arrays of time intervals, write a function that calculates the total amount of time covered by the intervals.# For example:# input = [(1,4), (2,3)]# > 3# input = [(4,6), (1,2)]# > 3# input = [(1,4), (6,8), (2,4), (7,9), (10, 15)]# > 11*/ 


This question originally appeared [here](https://careercup.com/question?id=5707346257903616).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA avoids coding... thus :
def do_fb( l ){
  sorta ( l ) :: {  $.left.0 < $.right.0  }
  // merge and sum intervals 
  #(s,last) = fold ( [1:#|l|] , [ 0 , l[0] ] ) -> {
     prev = $.prev[1] ; cur = l[$.item] 
     if ( prev.1 >= cur.0  ){ // there is overlap 
       // but, is current fully inside previous?
       if ( prev.1 < cur.1 ){
         // merge them 
         $.prev[1] =  [ prev.0 , cur.1 ] 
       }
     } else {
        // update sum 
        $.prev.0 += ( prev.1 - prev.0 )
        $.prev.1 = cur 
     } 
     $.prev 
  }
  s += ( last.1 - last.0 )
}
input = [[1,4], [6,8], [2,4], [7,9], [10, 15]] 
println ( do_fb( input ) )
```

## Q121 
### Problem 
 >Write a program to test whether a string and all strings that can be made using the characters of that string are palindrome or not.Eg:Input		Outputmmo			Trueyakak		Truetravel		FalseNote : Please do not use any inbuilt functions. 


This question originally appeared [here](https://careercup.com/question?id=5635989726822400).
Tagged with : Amazon,Quality Assurance Engineer,Coding 

### Solution 

```scala
// ZoomBA one liner :
#|select ( mset(s.toCharArray) ) :: {  $.o.value % 2 != 0 }| <= 1
```

## Q122 
### Problem 
 >Interview Question: essentially given a bunch of sets in an array, print out the cross product of all of those sets 


This question originally appeared [here](https://careercup.com/question?id=5685978980155392).
Tagged with : Facebook,Software Engineer Intern 

### Solution 

```scala
si = set(1,2,3)
sf = set(1.0,2.0,3.0)
sb = set( true, false )
// generate cross product? very well :
p = si * sf * sb 
// not satisfied ? 
p = join ( si, sf, sb ) 
// want to load from array ?
p = join ( @ARGS = [ si, sf, sb ] )
// so, you wan to seriously code? Not cool.  // full iterative version, w/o any stack or recursion, should be good to watch... 
static boolean next(Object[] tuple, List<Iterator> states, Iterable[] cc) {
        boolean carry = true;
        for (int i = cc.length - 1; i >= 0; i--) {
            if (!carry) break;
            Iterator iterator = states.get(i);
            if (iterator.hasNext()) {
                tuple[i] = iterator.next();
                carry = false;
            } else {
                if (i == 0) return true;
                carry = true;
                iterator = cc[i].iterator();
                tuple[i] = iterator.next();
                states.set(i, iterator);
            }
        }
        return false;
    }

public static Collection join(Collection into, Function predicate, Function map, Iterable[] cc) {
        if (cc.length < 2) return into;
        int index = 0;
        Object[] tuple = new Object[cc.length];
        List<Iterator> myStates = new ArrayList<>();
        for (int i = 0; i < cc.length; i++) {
            Iterator iterator = cc[i].iterator();
            if (!iterator.hasNext()) return into;
            tuple[i] = iterator.next();
            myStates.add(iterator);
        }
        boolean carry = false;
        while (!carry) {
            Function.MonadicContainer result = predicate.execute(index, tuple, cc, into);
            if (!result.isNil() && ZTypes.bool(result.value(), false)) {
                result = map.execute(index, tuple, cc, into);
                extractResult( into, result, tuple );
            }
            if (result instanceof Break) {
                extractResult( into, result, tuple );
                break;
            }
            carry = next(tuple, myStates, cc);
            index++;
        }
        return into;
    }
```
```scala
def do_join( tuple, arr, result ){
  i = size(tuple)
  if ( i == size(arr) ){
     result.add ( tuple )
     return
  }
  for ( item : arr[i] ){
     do_join ( tuple + item, arr, result )
  }
}

s1 = [0,1,2]
s2 = [true, false]
arr = [s1, s2]

result = list()
do_join( [], arr, result )
println ( result )
```

## Q123 
### Problem 
 >You are given an integer, print its 4th least significant bit 


This question originally appeared [here](https://careercup.com/question?id=5749560358993920).
Tagged with : Expedia,Software Engineer / Developer,Bit Manipulation 

### Solution 

```scala
(zoomba)x = str(20,2)
10100 // String
(zoomba)x[-4]
0 // Character
(zoomba)
```

## Q124 
### Problem 
 >Given a target sum, populate all subsets, whose sum is equal to the target sum, from an int array.For example:Target sum is 15.An int array is { 1, 3, 4, 5, 6, 15 }.Then all satisfied subsets whose sum is 15 are as follows:15 = 1+3+5+615 = 4+5+615 = 15 


This question originally appeared [here](https://careercup.com/question?id=5724941042319360).
Tagged with : Google,Software Developer 

### Solution 

```scala
select from whatever where sum( all columns ) == S
```

## Q125 
### Problem 
 >Remove any number containing 9 like 9, 19, 29, ... 91, 92, ... 99, 109...    Write a function that returns the nth number. E.g.  newNumber(1) = 1     newNumber(8) = 8, newNumber(9) = 10 


This question originally appeared [here](https://careercup.com/question?id=5729483920244736).
Tagged with : Facebook,SDE1 

### Solution 

```scala
removed_9 = seq ( 0 ) ->{
    next = $.p[-1] + 1
    while ( '9' @ str(next) ){ next += 1 }
    next //return 
}
def newNumber(inx){
   if ( inx <= 0 ) return ''
   while ( inx  >= size ( removed_9.history ) ){
       removed_9.next 
   } 
   removed_9.history[inx]
}
println( newNumber(1) )
println( newNumber(8) )
println( newNumber(9) )
println( removed_9.history )  ➜  zoomba99 git:(master) ✗ zmb tmp.zm
1
8
10
[ 0,1,2,3,4,5,6,7,8,10 ]
➜  zoomba99 git:(master) ✗
```

## Q126 
### Problem 
 >Write a program to print pascal triangle using binomial formula. 


This question originally appeared [here](https://careercup.com/question?id=5706974911004672).
Tagged with : Skill Subsist Impulse Ltd,Software Developer,C 

### Solution 

```scala
// ZoomBA
FACTORIALS = list(1,1,2,6,24)
def factorial ( n ){
  if ( n <= size(FACTORIALS) ) return FACTORIALS[n]
  for ( [size(FACTORIALS):n+1] ){
    FACTORIALS += FACTORIALS[-1] * $
  }
  FACTORIALS[n]
}
println( factorial(10) )
```

## Q127 
### Problem 
 >There is a process sequence that contains the start and end of each process. There is a query sequence asking how many processes are running at a certain point in time. Please return the query result of the query sequence.ExampleGiven logs = [[1, 1234], [2, 1234]], queries = [2], return [2].Explanation:There are 2 processes running at time 2.Given logs = [[1, 1234], [2, 1234]], queries = [1, 1235], return [1, 0].Explanation:There is a process running at time 1, and 0 processes running at time 1235. 


This question originally appeared [here](https://careercup.com/question?id=5701990720995328).
Tagged with : Amazon,Java Developer 

### Solution 

```scala
/* A naive solution is mark the time index and increment.
When we add a [start,end] we add and mark all time slices
between these -- but terrible usage of memory : call it version 1
*/
def mark_and_inc( history, log_slice ){
  for ( t = log_slice.start ; t <= log_slice.end ; t+= 1 ){
    if ( t !@ history ){
      history[t] = 0
    }
    history[t] +=1
  }
}
// create history by repeating mark and add 
def create_history( logs ){
  history = dict()
  for ( l : logs ){
    mark_and_inc( history, l)
  }
  history // return 
}
// create history 
history = create_history( logs )
// query is as straight forward as 
def process_count_at( t ){
  t @ history ? history[t] : 0
}
```

## Q128 
### Problem 
 >Generate a random 4-digit even number : the adjacent 2 digits must be different.public int getNumber(){} 


This question originally appeared [here](https://careercup.com/question?id=5657872336683008).
Tagged with : Google,Software Engineer 

### Solution 

```scala
/* 
4 digit Even.
That gives :
A | B | C | D = [0 2 4 6 8]
Such that D  != C etc etc.
*/
__digits__ = list ( [0:10] )  
__l_digits__ = list ( [1:10] )  

def four_digit_even_unopt(){
  D = random( [0,2,4,6,8] )
  C = random( __digits__ - D )
  B = random( __digits__ - C )
  A = random( __l_digits__ - B )
  1000 * A + 100 * B + 10 * C + D 
}
println( four_digit_even_unopt() )
```

## Q129 
### Problem 
 >Given a length n, return the number of strings of length n that can be made up of the letters 'a', 'b', and 'c', where there can only be a maximum of 1 'b's and can only have up to two consecutive 'c'sExample: findStrings(3) returns 19since the possible combinations are: aaa,aab,aac,aba,abc,aca,acb,baa,bac,bca,caa,cab,cac,cba,cbc,acc,bcc,cca,ccband the invalid combinations are:abb,bab,bba,bbb,bbc,bcb,cbb,ccc 


This question originally appeared [here](https://careercup.com/question?id=5717453712654336).
Tagged with : Google,Software Engineer / Developer,Algorithm 

### Solution 

```scala
// ZoomBA
n = 3 
$count = 0 
args = list( [0:n] ) -> { [ 'a' ,'b' ,'c' ] }
join ( @ARGS = args ) :: {
  s = str( $.item, '') 
  continue ( s !~ '[^b]*(b)?[^b]*' || s =~ '.*ccc.*' )
  $count += 1 // add up  
  false 
}
println( $count )
```

## Q130 
### Problem 
 >Given input which is vector of log entries of some online system each entry is something like (user_name, login_time, logout_time), come up with an algorithm with outputs number of users logged in the system at each time slot in the input, output should contain only the time slot which are in the input. For the example given below output should contain timeslots[(1.2, 1), (3.1, 2), (4.5, 1), (6.7, 0), (8.9, 1), (10.3,0)] /*                                                                              [                                                                               ("Jane", 1.2, 4.5),                                                             ("Jin", 3.1, 6.7),                                                              ("June", 8.9, 10.3)                                                              ]                                                                                                                                                               =>                                                                                                                                                              [(1.2, 1), (3.1, 2), (4.5, 1), (6.7, 0), (8.9, 1), (10.3, 0)]                                                                                                   */ 


This question originally appeared [here](https://careercup.com/question?id=5145104823091200).
Tagged with : Uber,SDE-2 

### Solution 

```scala
events = [ ["Jane", 1.2, 4.5],  ["Jin", 3.1, 6.7], ["June", 8.9, 10.3] ]
// sort them with field index 1 : e.g. start time 
sorta( events ) :: { $.left.1 < $.right.1 }
// create a list of time slots 
left = {  's' : events[0].1 , 'e' : events[0].2 , 'c' : 1 } // first one
col = fold ( [1 : #|events| ], list( left ) ) -> {
   right = events[$.o] 
   /*  There can be two cases.
     1. right is included in left
     2. right and left are independent, with no overlap 
   */
   left = $.p[-1] // last one is the left 
   if (  left.e >= right.1  ) { // overlap
      // completely included 
      continue ( left.e  >= right.2 ){ left.c += 1 }
      // split the interleaving 
      left_half = { 's' : left.s , 'e' : right.1 , 'c' : left.c }
      middle_half = { 's' : right.1 , 'e' : left.e , 'c' : left.c + 1 }
      right_half = { 's' : left.e , 'e' : right.2 , 'c' : 1 }
      $.p.pop() // pop the last item added to the list 
      // add them up 
      $.p += [ left_half , middle_half , right_half ]
      
   }else{ // non overlap 
     $.p += { 's' : left.e , 'e' : right.1 , 'c' : 0 }
     $.p += { 's' : right.1 , 'e' : right.2 , 'c' : 1 }
   }
   // return partial 
   $.p 
}
col += { 's' : col[-1].e , 'e' : num('inf') , 'c' : 0 }
// now print 
println( str( col , ' , ' ) -> {  str('(%s,%s)', $.o.s, $.o.c ) } )
```

## Q131 
### Problem 
 >You are given 2 lists - List 1: List<Demand> is a list of Demand objects.List 2: List<Supply> is a list of Supply objects.Return a result fulfillment List<Demand,List<Supply>>.This means each demand could be satisfied by more than one supplies.class Demand
{
Date startDate;
Date expirationDate;
int quantity;
}

class Supply
{
Date startDate;
Date expirationDate;
int quantity;
}The Demand and Supply refers to that of groceries. You must map supplies to a demand only if the supply still has at least 3 days remaining to its expiration before the demand can be fulfilled.A demand is said to be fulfilled 24 hours after all demands have been mapped to correspondingly available supplies. 


This question originally appeared [here](https://careercup.com/question?id=5655380106412032).
Tagged with : Amazon,SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA
def conforms( demand, supply ){
  // write code here 
  // this is a predicate filter 
  // should it subtract too? No idea 
  demand.quantity <= supply.quantity && 
  time ( supply.expirationDate ) - "P3D" > demand.expirationDate 
  // note the use of en.wikipedia.org/wiki/ISO_8601#Durations
}
// this template code, does not need to change 
dict ( demands ) -> { 
   demand = $.item 
   // read : select all supplies such that they conform to the demand 
   conformed_supplies = select ( supplies ) :: { conforms ( demand , $.item ) }
   [ demand , conformed_supplies ]
}
// this is how business should code it. ALAS.
```

## Q132 
### Problem 
 >Given a string s, return all the palindromic permutations ( without duplicates), of it. Return an empty array if no palindromic combinations can be formed. 


This question originally appeared [here](https://careercup.com/question?id=5704589056671744).
Tagged with : Uber,Software Engineer,Algorithm 

### Solution 

```scala
//ZoomBA : gitlab.com/non.est.sacra/zoomba/wikis/09-samples#permutation-of-a-list-containing-repeated-items
l = string.toCharArray() 
r = [0:#|l|]
permutations = set()
join( @ARGS = list(r) as { l } ) where {
    continue ( #|set($.o)| != #|l| ) // mismatch and ignore 
    v = str($.o,'') as { l[$.o] }
    continue ( v @ permutations ) // if it already occurred 
    continue ( exists( [0 : #|v|/2 ] ) :: { v[$.o] != v[-1 - $.o] } )   
    permutations += v // add them there  
    false // do not add  
}
```

## Q133 
### Problem 
 >Given an array of integers. Find the surpasser count of each element of the array."A surpasser of an element of an array is a greater element to its right"ex - Input:  [2, 7, 5, 3, 0, 8, 1]Output: [4, 1, 1, 1, 2, 0, 0] 


This question originally appeared [here](https://careercup.com/question?id=5683398594002944).
Tagged with : Yahoo,Software Engineer / Developer,Algorithm 

### Solution 

```scala
// ZoomBA
def surpasser_count( arr ){
  len = #|arr|
  list( [0:len] ) -> {
    n = arr[ $.item ]
    sum( [$.item + 1 : len] ) -> { arr[ $.item ] > n ? 1 : 0 }
  }
}
```

## Q134 
### Problem 
 >Given a list of ranges of Length N, where start and end are inclusive in the range [(5,7),(1,4),(2,3),(6,8),(3,5)]and given any number K , lets say K=8in an array of length K+1 find how many times each index is contained within a range in the Listlets say K=8, we will have an array (arr) from index 0 to index 8arr[0]=0; arr[1]=1; {1 is only contained in (1,4)} arr[2]=2; {2 is contained in (1,4) (2,3) } arr[3]=3; {3 is contained in (1,4) (2,3) (3,5) } arr[4]=2; {4 is contained in (1,4) (3,5)} arr[5]=2; {5 is contained in (3,5) (5,7)} arr[6]=2; {6 is contained in (5,7) (6,8)} arr[7]=2; {7 is contained in (5,7) (6,8)} arr[8]=1; {8 is contained in (6,8)}I am looking for a O(N+K) solutio 


This question originally appeared [here](https://careercup.com/question?id=5709181349789696).
Tagged with : Algorithm 

### Solution 

```scala
/* 
Note that to way to do it most optimally is
to generate a indices map from the ranges, 
which does not require a sorting.
Once the dictionary is created, 
all we do is traverse over the indices range given 
[0,k] ; or in ZoomBA [0:k+1] -> and print counts
or zero if the index is not found.
*/
range = [ [5,7],[1,4],[2,3],[6,8],[3,5] ] 
// this takes ( min to max )
d = fold( range , dict() ) -> {
   #(b,e) = $.o // items are tuples
   for ( i : [b:e+1] ){
    // add indices and store counts 
    if ( i @ $.p ){
      $.p[i] += 1
    }else{
      $.p[i] = 1
    }
  }
  $.p
}
println ( d )
// produce the result 
ar = [0:9] // from 0 to 8 indices 
fold ( ar ) -> {  printf ( '%d -> %d\n', $.o , $.o @ d ? d[$.o] : 0 )  }
```

## Q135 
### Problem 
 >Apple phone interview Given an API to find all IPv4 addresses in a log file, find all IPs that occurred only once. Follow-up: What if the log comes from a data stream. Follow-up: If the machine has 4GB RAM, is there going to be a problem? 


This question originally appeared [here](https://careercup.com/question?id=6196514408890368).
Tagged with : Apple,Backend Developer,Algorithm 

### Solution 

```scala
./yourscript_to_isolate_ip.sh | sort | uniq -u
```

## Q136 
### Problem 
 >Given a dictionary and an char array print all the valid words that are possible using char from the array.Ex- char[] arr = {'e','o','b', 'a','m','g', 'l'}Dict - {"go","bat","me","eat","goal", "boy", "run"}Print - go, me, goal.We can pre-compute as much we want but the query time must be optimal. 


This question originally appeared [here](https://careercup.com/question?id=5707619374202880).
Tagged with : Microsoft,SDE-2,Algorithm 

### Solution 

```scala
DICT = [ "go","bat","me","eat","goal", "boy", "run" ]
list_of_msets = list ( DICT ) -> {  [ mset( $.o.value ) , $.o ] }
LETTERS = [ _'e' , _'o' , _'b', _'a' , _'m', _'g' , _'l' ]
mset_letters = mset( LETTERS )
words = select( list_of_msets ) :: {  $.left <= mset_letters } -> { $.right }
println( words )
```
```scala
/*
 The problem can be solved by first
 making the given dictionary a dictionary with :
 key -> sorted letters of a word as string
 values -> all words who are permutations of the same key

 Thus, given an array of letters:
 1. we should sort it first.
 2. Find all possible sub sequences of the same sorted array
 3. For each sub sequence (which would be sorted)
   we should check existence as key in the dictionary
   and if it does exist, print all words (values) for that key
*/

DICT = [ "go","bat","me","eat","goal", "boy", "run" ]
sorted_dict = mset( DICT ) -> { x = $.o.toCharArray ; sorta( x ) ; str(x,'') }
LETTERS = [ 'e' ,'o' , 'b' ,'a' ,'m' ,'g' , 'l' ]
sorta(LETTERS)
for ( sequences ( LETTERS ) ) {
  key = str( $,'')
  if( key @ sorted_dict ){
    println ( sorted_dict[key] )
  }
}
```

## Q137 
### Problem 
 >You are given an array of  return the sum of the two elements closest to zero Example  input {10,20,30}     O/P : 30again give the input input  {-5,-13,5}   output  : 0 


This question originally appeared [here](https://careercup.com/question?id=5717738531061760).
Tagged with : Persistent Systems 

### Solution 

```scala
// ZoomBA :: Trivial in it 
h = heap(2) ::{ #|$.o.0| < #|$.o.1| }
h += [ -5, 13, 5 ]
println ( sum(h) )
```

## Q138 
### Problem 
 >Look at the following sequence:3, 5, 6, 9, 10, 12, 17, 18, 20....All the numbers in the series has exactly 2 bits set in their binary representation. Your task is simple, you have to find the Nth number of this sequence. 1 <= T <= 10^6 1 <= N <= 10^14 I tried to implement it as follows but it is giving me wrong answer for some hidden cases.#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#define MAX 10000
#define MOD 1000000007

long long int m[MAX+1];
long long int sum;

long long int binary_search(long long int n)
{
    long long int low=0,high=MAX,mid;
    while(low<high)
    {
        mid=low+(high-low+1)/2;
        if(m[mid]<=n)
            low=mid;
        else
            high=mid-1;
    }
    return low;
}

int main() 
{
    m[0]=1;
    for(long long int i=1;i<=MAX;i++)
        m[i]=m[i-1]+i;
    
    long long int n,k,l;
    int t;
    scanf("%d",&t);
    for(int test=0;test<t;test++)
    {
        scanf("%lld",&n);
        k=binary_search(n);
        //cout<<m[k]<<" ";
        l=n-m[k];
        cout<<((1<<k+1)%MOD+(1<<l)%MOD)%MOD<<"\n";
    }
    return 0;
} 


This question originally appeared [here](https://careercup.com/question?id=5735144200601600).
Tagged with : TCS CodeVita,Bit Manipulation 

### Solution 

```scala
// NOT FOR Competitive stuff - it is meaningless for practical purposes.
// but cool...
/* 
Given S is then sorted sequence of numbers such that 
Binary representation contains only 2 bits set to 1.
Find S(n). 
We solve it by creating an iterator.
*/
s = seq( 3 ) -> {
  bs = str($.p[-1],2)
  li = rindex(bs.value, _'1' )
  if ( li == 1 ){
     r = 2 ** #|bs| + 1 
  } else {
     bs.value[li] = _'0'
     bs.value[li-1] = _'1'
     r = int(bs,2,0)
  }
  r 
}
fold ( [1:9] ) -> { println( s.next ) }
```

## Q139 
### Problem 
 >Given k numbers as strings. The numbers may be very large (may not fit in long long int), the task is to find sum of these k numbers. Example S1 = “100”S2 = “10”S3 = “1”Return “111”public string addNumbers(String[] nums){} 


This question originally appeared [here](https://careercup.com/question?id=5659282035965952).
Tagged with : Amazon,Backend Developer 

### Solution 

```scala
sum ( file( 'input.txt' ) ) as { int(  $.o ) } // this is ZoomBA  sum(int(v) for v in open('input.txt') ) # Python
```

## Q140 
### Problem 
 >An ABC notation in a tree is defined as folllows:1. "0" means travel left2. "1" means travel right3. "Undefined" means hit the root4. "Not Found" means not present in treeGiven a BST insertion order, {5,2,8,3,6,9,1} find the ABC notation for 6, 1, 10, 2 which is "10","00","NotFound", "0" 


This question originally appeared [here](https://careercup.com/question?id=5690270969495552).
Tagged with : Apple,Software Engineer 

### Solution 

```scala
def BST : {
    $$ : def(){
        $.root = null
    },
    add_node : def(v){
      new_node = {'v' : v , 'l' : null , 'r' : null }  
      if ( empty($.root) ){
          $.root = new_node
          return $.root 
      } 
      parent = $.root
      current = v < parent.v  ? parent.l : parent.r 
      while ( current != null ){
         parent = current
         current = v < parent.v  ? parent.l : parent.r   
      } 
      if ( v < parent.v ){
          parent.l = new_node
      } else if ( parent.v < v ){
          parent.r = new_node
      }    
    },
    find_abc : def(key){
      current = $.root 
      path = ''
      while ( current != null && current.v != key ){
         if (  key < current.v ){
            path += '0'  
            current = current.l 
         } else if ( current.v < key ){
            path += '1'
            current = current.r 
         }
      }
      (!empty(current) && current.v == key)? path : 'Not Found'
    }
}
//now play 
bst = new ( BST )
values = [5,2,8,3,6,9,1]
// add nodes 
for ( v : values ){ bst.add_node(v) }
keys =  [6, 1, 10, 2]
// print ABC ... 
for ( v : keys ){ printf( '%s -> %s%n', v, bst.find_abc(v) ) }
```

## Q141 
### Problem 
 >Given a pattern and a string - find if the string follows the same pattern Eg: Pattern : [a b b a], String : cat dog dog cat 


This question originally appeared [here](https://careercup.com/question?id=5684019468435456).
Tagged with : Google,Software Engineer / Developer,Algorithm 

### Solution 

```scala
/* 
 1. we start with the values, and assigning them the keys from pattern 
 2. If we find a key whose value does not match the word, we failed
 3. Else we have succeeded 
*/

def matches( pattern , str_list ){
  if ( size(pattern) != size(str_list) ) return false 
  keys = dict()
  fold ( str_list , true ) -> {
    key = pattern[$.i]
    word = $.o 
    if ( key @ keys ){ // key exists 
      // if the value pointed by the key is the word?
      // then ok, else fail 
      break ( keys[key] != word ) { false }
    } else {
      // this is new word, make a key,value out of it 
      keys[key] = word
    }
    $.p
  }
}
pattern = [ 'a' , 'b', 'b' , 'a'  ] 
string = [ 'cat', 'dog', 'dog', 'cat' ]
println ( matches ( pattern, string ) )
```

## Q142 
### Problem 
 >Need to traverse below n-ary in postorder and throw error message if the node are cyclic

Node001
/ | \
/ | \
/ \ \
Node002 Node003 Node004
/ / | \
/ / | \
Node005 Node006 Node007 Node008
/
/
Node009
\
\
Node003
In above case it should through error because Node009 has child Node003 which is derived from it. 


This question originally appeared [here](https://careercup.com/question?id=5718937200230400).
Tagged with :  

### Solution 

```scala
// ZoomBA
node = { 'id' : 0 , 'children' : list() }
visited_nodes = set()
def post_order(node){
   // awesomeness of ZoomBA is linear error handling mechanism
   // if condition is true, panic is raised with the message
   panic ( node.id @ visited_nodes , 'I have visited this node before!' )
   visited_nodes += node.id // appends it
   for ( child : node.children ){
      post_order( child )
   }
   println ( node ) // visits it
}
```

## Q143 
### Problem 
 >It was asked in Chargebee off campus interview. Needed solution for this problem in javaGiven a string say s and k denotes the number of commas and the output should be like when you insert the comma in the string at different places and find the maximum number.Test case 1say s = 999 and k = 1 so the choice would be 9,99 or 99,9 in either case the maximum number is 99Test case 2say s=999 and k =2 so the choice will be like 9,9,9 so output will be 9Test case 3say s = 857 and k = 1 the choice would be 85,7 or 8,57 so the output will be like 85 


This question originally appeared [here](https://careercup.com/question?id=4831996474818560).
Tagged with : StartUp,Java Developer,Arrays,Brain Storming,String Manipulation 

### Solution 

```scala
/* 
Observe that the problem is that of finding locations where 
we can cut the string.
Given a string of length 's' has exact s-1 locations where we can cut it 
That s-1 becomes the number of choices = n.
Clearly then the number of comma = k is the cut locations to be selected.
It is obvious that the order in which the cut was imposed does not matter.
Thus, the choices are C(n,k).
Thus, the problem is:
1. finding combinations of cut
2. Applying the cuts 
3. Maintaining a max out of the cuts
*/

def do_fancy(string, k){
    n = size(string) - 1
    range = [0:k+1]
    max = 0
    for ( t : comb(range,k) ){
        start_inx = 0
        words = list()
        for ( i = 0; i < k ; i+= 1 ){
          words += int( string[start_inx:t[i]] )
          start_inx = t[i] + 1
        }
         words += int( string[start_inx:-1] )
        #(m,M) = minmax( words )
        #(m,max) = minmax( max, M )
    }
    max // return 
}

println( do_fancy( '89769957',3 ) )
```

## Q144 
### Problem 
 >you have a array nums as input. For any i from 0 to length - 1. should print product of whole array except nums[i]For example: nums = [2,3,1,4,3,2]output:              72             48             144              36              48              72 


This question originally appeared [here](https://careercup.com/question?id=5755558775750656).
Tagged with : Yahoo,Intern,Arrays 

### Solution 

```scala
// ZoomBA
nums = [  2,3,1,0, 4,3,2 ]
def do_yahoo( nums ){
  #(num_zeros, product)  = lfold ( nums ,[0, 1] ) -> {  
    if ( $.o == 0 ){ $.p.0 += 1 } else { $.p.1 *= $.o } 
    $.p  
  }
  product = num_zeros > 1 ? 0 : product 
  for ( nums ) {
    println ( $ == 0 ? product : (product/$) )
  }
}
do_yahoo ( nums )
```

## Q145 
### Problem 
 >Given an Array of N elements and Integer K, write a function that returns true if the sum of any 2 elements of Array is K, false otherwise. 


This question originally appeared [here](https://careercup.com/question?id=6285101383024640).
Tagged with : Facebook,Android Engineer,Algorithm 

### Solution 

```scala
// ZoomBA.
def has_sum( arr, K ){
  /* create a dictionary where the key is K - current_item 
    and the value is the current items index */
  subs = dict( arr ) -> { [ K - $.item , $.index ] }
  /* You would expect that when an item in the array is present
  as the key in the subs dictionary, you are good. 
  But you are wrong, what about :
     there is an element e such that : K = e + 2 = 2e ?  
  In that case, we check i am not matching the same element again 
  as I am checking the index */
  exists ( arr ) :: { $.item @ subs && subs[$.item] != $.index }  
}
// no 2*e form 
println( has_sum ( [1,0,4,10,12] , 14 ) )
// there is a 2*e form 
println( has_sum ( [1,0,4,7,12] , 14 ) )
// 2* e form, but there is a duplicate so...
println( has_sum ( [1,0,4,7,7,12] , 14 ) )
```

## Q146 
### Problem 
 >Given a list of list of positive integers, find all pairs of list which are having more than 3 elements overlapping.What is the complexity. 


This question originally appeared [here](https://careercup.com/question?id=5695873477509120).
Tagged with : Practo,SDE-2,Algorithm 

### Solution 

```scala
num_lists = 5
// create a list of lists
ll = list( [0: num_lists ] ) -> {
   list([0:10]) -> { random(30) }
}
println ( ll )
// naive approach
pairs = join ( [0:num_lists], [0:num_lists] ) :: {
   i = $.0 ; j = $.1
   continue ( i >= j )
   intersection = ll[i] & ll[j]
   size( intersection ) >= 3 // that is the condition ?
}
println ( pairs )
```
```scala
num_lists = 5
// create a list of lists
ll = list( [0: num_lists ] ) -> {
   list([0:10]) -> { random(30) }
}
println ( ll )
// smarter approach
d = fold( ll, dict() ) -> {
      for ( inx : [0:size(ll)] ){
         l = ll[inx]
         for ( item : l ){
           if ( item @ $.p ){
              $.p[item] += inx
           } else {
              $.p[item] = set(inx)
           }
         }
      }
      $.p
   }
// find all items that existed in more than one lists
entries = select ( d ) :: {  size( $.o.value ) >= 2  }
println ( entries )
// make pairwise
all_pairs = fold ( entries , dict() ) -> {
    list_indices = $.o.value
    // create pair from values
    pair = comb( list_indices , 2 )
    for ( p : pair ){
      k_p = str(p,',')
      if  ( k_p @ $.p ){
        $.p[k_p] += $.o.key
      } else {
         $.p[k_p] = list( $.o.key )
      }
    }
    $.p
}
println ( all_pairs )
// select all entries with value length more than 2
valid_list_pairs = select ( all_pairs ) :: { size( $.o.value ) > 2 }
println ( valid_list_pairs )
```

## Q147 
### Problem 
 >Write code to decode strings. For example, String str = "3[a2[bd]g4[ef]h]", the output should be "abdbdgefefefefhabdbdgefefefefhabdbdgefefefefh".My solution is as follows.public class StringDecoder {
	
	public static void main(String[] args){
		
		String s = "3[a2[bd]g4[ef]h]";
		
		System.out.println(decode(s));
		
	}
	
	public static String decode(String s){
		
		if(s == null || s.length()==0) return s;
		
		int indexOfFirstNumber = findIndexOfFirstNumber(s);
		int indexOfFirstBracket = findIndexOfFirstBracket(s);
		int indexOfClosingBracket = findIndexOfClosingBracket(s, indexOfFirstBracket);
		
		if(indexOfFirstNumber == -1) return s;
		
		String subStr1 = s.substring(0, indexOfFirstNumber);
		String subStr2 = decode(s.substring(indexOfFirstBracket+1, indexOfClosingBracket));
		String subStr3 = decode(s.substring(indexOfClosingBracket+1, s.length()));
		
		int duplicates = Integer.parseInt(s.substring(indexOfFirstNumber, indexOfFirstBracket));
		
		StringBuilder sb = new StringBuilder();
		sb.append(subStr1);
		
		while(duplicates>0){
			sb.append(subStr2);
			duplicates--;
		}
		
		sb.append(subStr3);
		
		return sb.toString();
	}
	

	public static int findIndexOfFirstNumber(String s){
		
		int index = -1;
		for(int i=0; i<s.length(); i++){
			
			char c = s.charAt(i);
			if(c>=47 && c<=58){
				index = i;
				break;
			}
			
		}
		
		return index;
	}
	
	public static int findIndexOfFirstBracket(String s){
		
		int index = -1;
		for(int i=0; i<s.length(); i++){
			
			char c = s.charAt(i);
			if(c=='['){
				
				index = i;
				break;
			}
			
		}
		
		return index;
	}
	
	public static int findIndexOfClosingBracket(String s, int indexOfBracket){
		int index = -1;
		
		int numberOfBracket = 1;
		
		for(int i=indexOfBracket+1; i<s.length(); i++){
			
			char c = s.charAt(i);
			if(c == '[') numberOfBracket++;
			
			if(c==']'){
				numberOfBracket--;
				if(numberOfBracket==0){
					index = i;
					break;
				}
			}
		}
		
		return index;
	}
} 


This question originally appeared [here](https://careercup.com/question?id=5692396030394368).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
/* 
Problem. We should think in terms of grammar rules.
Production Rules are :
  G -> ( S )*
  S -> <num> [ E+ ]
  E -> S | <string>
  There is no ambiguity, because Rule S starts with a number.
  This is context free grammar. 
  I require a Stack/ But I would recurse 
*/
def decode( encoded ){
   cur_string = ''
   inx = 0 ; len = size(encoded)
   times = ''
   while ( inx < len ){
      switch ( encoded[inx] ){
        case @$  @ '0123456789' :
          // <num> rule 
          times += @$ 
        case _'[' :
          // inside recursive rule (S)
          right_bra = inx
          bra_count = 1 
          while ( bra_count != 0 ){
            right_bra += 1 
            char = encoded[right_bra]
            if ( char == _']' ){ bra_count -= 1 }
            if ( char == _'[' ){ bra_count += 1 }
          }
          tmp = decode( encoded[ inx + 1 : right_bra -1 ] ) 
          cur_string += ( tmp ** int( times ) )
          times = ''
          inx = right_bra 
        case @$:
         // default case : we add up the string (<string>)
          cur_string += @$
      }
      inx += 1 
   }
  return cur_string
}
string =  "3[abc2[ef]]"
println ( decode ( string ) )
```

## Q148 
### Problem 
 >Problem StatementGiven an integer N, express it as the sum of at least two consecutive positive integers. For example:10 = 1 + 2 + 3 + 424 = 7 + 8 + 9If there are multiple solutions, output the one with the smallest possible number of summands.Input FormatThe first line of input contains the number of test cases T. The descriptions of the test cases follow:Each test case consists of one line containing an integer N (1 ≤ N ≤ 109).Output FormatFor each test case, output a single line containing the equation in the format:N = a + (a + 1) + ...+ bas in the example. If there is no solution, output a single word ‘IMPOSSIBLE’ instead.Sample Input381024Sample OutputIMPOSSIBLE10 = 1 + 2 + 3 + 424 = 7 + 8 + 9 


This question originally appeared [here](https://careercup.com/question?id=5158367691538432).
Tagged with : IIT-D,Student,C 

### Solution 

```scala
/* 
en.wikipedia.org/wiki/Arithmetic_progression
x = na + n*( n - 1 )/2 
examples :
  x = 6 
  a = 1 , n = 3 
  6 = 1*3 + 3*(3-1)(3-2)/2  
  x = 10 
  a = 1, n = 4 
  10 = 1 * 4 + (4 -1)(4)/2 
Thus in ZoomBA :: We have this identity 
*/
def find_ap( x ){
  upto = ciel ( x ** 0.5 )
  solved = join ( [1 : upto + 1 ] , [ 2 : upto + 1 ] ) :: {
     a = $.o.0 ; n = $.o.1 
     break ( n * ( a + ( n - 1 )/ 2.0  ) == x ) {
        printf ( 'a :%d, n: %d\n', a , n ) 
        [ a , n ]
     }
  } 
  if ( empty(solved) ){ println('Impossible' ) } 
}
find_ap( 10 )
```

## Q149 
### Problem 
 >input[] ={"ab","cd","f","ab","ef","abc"} if we search in string s ="ab"; then output  3 


This question originally appeared [here](https://careercup.com/question?id=5101425353818112).
Tagged with : Persistent Systems,Java Developer 

### Solution 

```scala
// ZoomBA
input = ["ab", "cd", "f", "ab", "ef", "abc"]
println ( rindex  ( input , 'ab') )
```

## Q150 
### Problem 
 >There is a conference room. N people are joining the conference. You have the start time and end time of each of them visiting it. You are asked to determine the maximum number of people that can be inside the room.Example – Four people are visiting the conferencePerson  	     A	  B	C	D	Start (hour)   1	  3	2	5		End (hour)    4	  5	7	10Answer will be – 3 


This question originally appeared [here](https://careercup.com/question?id=5719984029302784).
Tagged with : Amazon,SDE1,dp , matrix ,recursion , flipkart 

### Solution 

```scala
Start = [1,3,2,5] 
End = [4,5,7, 10] 
// there can be multiple in same time 
starts = mset( Start )
ends = mset( End )
// sorted set of timings 
timings = sset( Start ) 
timings += End  
max = 0 ; cur = 0 
for ( inx : timings ){
  cur += (inx @ starts ? starts[inx] : 0)
  cur -= (inx @ ends ? ends[inx] : 0)
  max = cur > max ? cur : max 
}
println(max)
```

## Q151 
### Problem 
 >Alex has recently decided to learn about how to design compilers. As a first step he needs to find the number of different variables that are present in the given code.So Alex will be provided N statements each of which will be terminated by a semicolon(;). Now Alex needs to find the number of different variable names that are being present in the given statement. Any string which is present before the assignment operator denotes to a variable name.Input Format: :The first line contains a single integer NEach of the next  N lines contains a single statement.It is guaranteed that all the given statements shall be provided in a valid manner according to the format specified.Output Format: :Print the number of different variable name that are present in the given statements.Sample Input2foo = 3;bar = 4;Sample Output2ExplanationFoo and Bar are only two variables used inside the statements so answer is 2. 


This question originally appeared [here](https://careercup.com/question?id=5681183171018752).
Tagged with : Aricent,Software Engineer,Java 

### Solution 

```scala
<ID> := [a-zA-Z_][a-zA-Z_0-9]* 
assignment -> ID  '='  [0-9]+  ';'
```

## Q152 
### Problem 
 >For a given Sum and N print all the combinationsFor Example Sum = 16 and N=2 Then Answer :16,015,114,213,312,411,510,69,78,87,96,105,114,123,132,141,150,16private void recursivePrint(int sum, int n, int data[], int len, int originalSum) {

		if (n == 0) {

			int sum1 = 0;

			for (int j = 0; j < len; j++) {

				sum1 += data[j];
			}

			if (sum1 == originalSum)

			{
				for (int j = 0; j < len; j++) {

					System.out.print(data[j] + " ");
				}
				System.out.println();
			}

			return;
		}

		for (int i = 0; i <=sum; i++) {

			// System.out.println(" len: "+len+" sum: "+sum+" i: " + i+" n:"+n)
			data[len] = i;

			recursivePrint(sum - i, n - 1, data, len + 1, originalSum);
		}
	}

}Need a better solution so that we can store previous results by using hashmaps 


This question originally appeared [here](https://careercup.com/question?id=4917596202729472).
Tagged with : Microsoft,Software Developer 

### Solution 

```scala
/* 
For a given Sum and N print all the combinations .
This is integer partition problem.
Constrained to number of partitions := N 
Given an integer is n, imagine 
1 1 1 1 ... 1 ( n 1's)
Now, there can be n-1 cut positions.
Choosing or not choosing a cut position 
will create a partition.
Thus, create a binary string with n-1 digits.
When 1, we choose the cut, when 0, we do not.
This will produce all the cuts and thus, all partitions.
The problem is constrained by number of cuts := N-1
*/
def partition_int( n , N ){
  n_splits = n - 1
  max = 2 ** n_splits 
  partitions = set()
  for ( x : [1:max] ) {
    bs = str(x,2)
    // do 0 padding on left
    bs = '0' ** ( n_splits - size(bs) ) + bs 
    nums = list()
    c = 1
    for ( b : bs.value ){
      if ( b == _'1' ){
        nums += c
        c = 1
      } else {
        c += 1
      }
    } 
    nums += c
    continue ( size(nums) != N )
    // to make the partitions unique 
    sorta(nums)
    ps = str(nums,',')
    continue ( ps @ partitions )
    // only when does not exist 
    partitions += ps 
    // now print 
    printf('%s -> %s \n', bs, ps )
  }
}
partition_int( @ARGS = list( @ARGS ) -> { int($.o) } )  zmb tmp.zm 16 2
000000000000001 -> 1,15 
000000000000010 -> 2,14 
000000000000100 -> 3,13 
000000000001000 -> 4,12 
000000000010000 -> 5,11 
000000000100000 -> 6,10 
000000001000000 -> 7,9 
000000010000000 -> 8,8  zmb tmp.zm 10 4
000000111 -> 1,1,1,7 
000001011 -> 1,1,2,6 
000010011 -> 1,1,3,5 
000010101 -> 1,2,2,5 
000100011 -> 1,1,4,4 
000100101 -> 1,2,3,4 
000101010 -> 2,2,2,4 
001001001 -> 1,3,3,3 
001001010 -> 2,2,3,3
```

## Q153 
### Problem 
 >Round3 For N light bulbs, implement two methods I. isOn(int i)  - find if the ith bulb is on or off. II. toggle(int start, int end) 


This question originally appeared [here](https://careercup.com/question?id=5667270754828288).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
/* A clean solution using Java BigInteger.
We need to have N bulbs, so N binary digits we need 
To have so, we must have a N+1 size integer.
*/
def Bulbs{
   def $$(N){
     s = str(2 ** ( N + 1 ))
     $.underlying = new ( 'java.math.BigInteger' , s )
   }
   def is_on(i){
    // tutorialspoint.com/java/math/biginteger_testbit.htm 
    $.underlying.testBit(i)
   }
   def toggle(start,end){
     $.underlying = fold([start:end+1],$.underlying){
      // tutorialspoint.com/java/math/biginteger_flipbit.htm
      $.p.flipBit($.o)
     }
   }
}
```

## Q154 
### Problem 
 >Given a sorted array with "n" elements, distributed them into "k" nearly equally weighing buckets.Space is not constraint.Ex: [1,3,6,9,10]bucket size: 3output:[10],[9,1],[3,6] 


This question originally appeared [here](https://careercup.com/question?id=5725965217955840).
Tagged with : Amazon,SDE-2,Dynamic Programming 

### Solution 

```scala
a = [ 0 , 1000, 10000] // k = 1  
 a = [ 0 , 1000, 10000] // k = 3  M = sum ( abs( sum(bucket_i) - sum(bucket_j)  ) )
```
```scala
int[] arr = new int[]{-1, 1, 1, 1, 10, 8 };
   int K = 2;
```
```scala
k = 2
M = [ -1,1,1,1,8,10 ]
N = size(M) 
last = k ** N // the largest one k to the power N 
min = num('inf') // well, infinity 
min_p = null // nothing 
// now we iterate - recursion only cool for divine
for ( n : [0:last] ){
  s = str(n,k) // representing number n in base k 
  // reject when all the k digits do not exist in base k rep  
  continue( size(set(s.value) ) != k )
  // left pad with zeros thus making the string N size  
  s = '0' ** ( N - size(s)) + s 
  // collect into k partitions ( change the char into int as key )
  p = mset( M ) as def(inx){ int(s[inx]) }
  // now generate total - calculating all pairs between 0,k-1
  tot = sum ( comb( [0:k] , 2 ) ) as def(inx, pair){ 
    // size(x) is abs(x)
    size( sum(p[pair[0]]) - sum(p[pair[1]])) 
  }
  // search for min m 
  if ( tot < min ){  
   min = tot
   min_p = p
  }
}
// well...
println(min)
println(min_p)
// and Finally, Galaxy has peace!
```

## Q155 
### Problem 
 >Find unique integers from list of integers# Question
# Write a function that will return an array of integers that occur exactly once in a given array of integers.
# e.g. For a list [1,2,3,5,2,2,3,4], return [1,5,4] since they appear once (order does not matter).

def once_integers(integers):Follow up:Optimize the code if input is sorted.# What if the input is sorted, such as [1,2,2,2,3,3,4,5], could the algorithm be further optimized
# (e.g. space complexity)?

def once_integers_sorted(integers): 


This question originally appeared [here](https://careercup.com/question?id=5750859611766784).
Tagged with : Linkedin,Software Developer 

### Solution 

```scala
// ZoomBA
select ( mset( numbers ) ) :: {  $.o.value == 1 } -> {  $.o.key  }
```
```scala
// From ZoomBA , with Love 
def not_sorted( a ){
   select ( mset( a ) ) :: { $.o.value == 1 } -> { $.o.key }
}
def sorted_imp( a ){
  lfold ([0: #|a| ], [ a[0] , 1 , list() ] ) -> { 
  if ( $.p.0 == a[$.o] ){ 
    $.p.1 += 1 
  } else{ 
     if ( $.p.1 == 1 ) { $.p.2 += $.p.0 }
     $.p.0 = a[$.o]
     $.p.1 = 1 
  }   
  $.p
}
```

## Q156 
### Problem 
 >There are around 40 million files in a directory which needs to be transferred to another system via FTP in order of oldest file first. What's the ideal way to iterate over files and store it in a data structure from where it can be transferred? 


This question originally appeared [here](https://careercup.com/question?id=5760945461657600).
Tagged with : Algorithm 

### Solution 

```scala
ls -1t <directory>
```

## Q157 
### Problem 
 >Given an non-negative int array and target number, check if the target can be equal to the sum of non-negative multiples of the numbers in the array.For example, I have three numbers 6,9,20. Ex: n = 47 then it can be determined that 47 = 9*3 + 20n=23 then there are no combinations. public boolean combinationSum(int[] nums, int target) {} 


This question originally appeared [here](https://careercup.com/question?id=5662358396469248).
Tagged with : Google,SDE1 

### Solution 

```scala
/* 
A better way to look it using sequences
Define a Sequence S, such that it is strictly increasing 
and generated by the rule of sum of non-negative multiples of the numbers in the array.
Thus, S(0) = 0 and we go in a = [6,9,20]
   S(1) = 6 
   S(2) = 9 
   S(3) = 12 = 6 * 2  
   S(4) = 15 = 6 + 9 
   S(5) = 18 = ( 3 * 6 , 9*2 ) 
We use ZoomBA to solve it and show a nice pattern.
This is solvable by adding 6,9,20 to each item encountered before in the sequence, 
and check if the current item is minimum of the larger item than the current max 
Then the next item is generated. Thus, the problem is solved when we have 
   Target n is such that 
   S(k) < n <= S(k+1)
To generate the this array a from bases is easy, and can be left as an exercise to the reader.
Hint: use the same algorithm and start with 0,min_of_base 
*/
bases = [6,9,20]
// first few items of the sequence from the bases till 20 
a = [ 0, 6, 9, 12 , 15 , 18, 20 ] 
s = seq( a ) -> {
  cached = $.p // previous items 
  last_no = cached[-1] // last item 
  maxes = list ( bases ) -> {
    item = $.o // store the individual base items 
    ix = index ( cached ) :: { $.o + item > last_no }
    cached[ix] + item  // we find where we max - so store it 
  }
  #(min,Max) = minmax( maxes ) // find min of the maxes 
  min // return min as the next item in the sequence 
}
// now call 
def find_some( n ){
   if ( n <= s.history[-1] ) return n @ s.history // obvious 
   while ( s.history[-1] <= n ){ s.next } // iterate over 
   return n @ s.history // and then is trivial 
}
println( find_some(47) )
println( find_some(23) )
```

## Q158 
### Problem 
 >Programming Challenge Description: Develop a service to help a client quickly find a manager who can resolve the conflict between two employees. When there is a conflict between two employees, the closest common manager should help resolve the conflict. The developers plan to test the service by providing an example reporting hierarchy to enable the identification of the closest common manager for two employees. Your goal is to develop an algorithm for IBM to efficiently perform this task. To keep things simple, they just use a single relationship "isManagerOf" between any two employees. For example, consider a reporting structure represented as a set of triples:Tom isManagerOf MaryMary isManagerOf BobMary isManagerOf SamBob isManagerOf JohnSam isManagerOf PeteSam isManagerOf KatieThe manager who should resolve the conflict between Bob and Mary is Tom(Mary's manager). The manager who should resolve the conflict between Pete and Katie is Sam(both employees' manager). The manager who should resolve the conflict between Bob and Pete is Mary(Bob's manager and Pete's manager's manager). Assumptions:There will be at least one isManagerOf relationship.There can be a maximum of 15 team member to a single managerNo cross management would exist i.e., a person can have only one managerThere can be a maximum of 100 levels of manager relationships in the corporationInput: R1,R2,R3,R4...Rn,Person1,Person2 R1...Rn - A comma separated list of "isManagerOf" relationships. Each relationship being represented by an arrow "Manager->Person". Person1,Person2 - The name of the two employee that have conflictOutput: The name of the manager who can resolve the conflict Note: Please be prepared to provide a video follow-up response to describe your approach to this exercise.Test 1:Test Input Frank->Mary,Mary->Sam,Mary->Bob,Sam->Katie,Sam->Pete,Bob->John,Bob,Katie Expected Output Mary Test 2:Test Input Sam->Pete,Pete->Nancy,Sam->Katie,Mary->Bob,Frank->Mary,Mary->Sam,Bob->John,Sam,JohnExpected Output Mary 


This question originally appeared [here](https://careercup.com/question?id=5737305298960384).
Tagged with : IBM,Software Engineer / Developer,Coding,Java,Python,String Manipulation 

### Solution 

```scala
// ZoomBA
ip1 = "Frank->Mary,Mary->Sam,Mary->Bob,Sam->Katie,Sam->Pete,Bob->John,Bob,Katie"
ip = "Sam->Pete,Pete->Nancy,Sam->Katie,Mary->Bob,Frank->Mary,Mary->Sam,Bob->John,Sam,John"
values = ip.split(",")
mgr = dict( [0: #|values| - 2 ] ) ->{
   #(k,v) = values[$.item].split('->') // mgr : employee
   if ( k @ $.partial ){
      $.partial[k] += v
   }else{
      $.partial[k] = list(v)
   }
   [k, $.partial[k] ]
}
emp = [ values[-2] , values[-1] ]
//root, the employee who never comes as value as managed
rs = mgr.keys  - fold ( mgr.values , list() ) -> { $.partial += $.item }
assert ( size(rs) == 1 ,'How come multiple Organizations?' )
root = for ( rs ){ $ } // idiomatic extraction of root
def get_path (emp, node, path ){
   if ( emp == node ) return path
   if ( !(node @ mgr) ) return ''
   for ( c : mgr[node] ){
      r = get_path ( emp, c, path + '/' + c )
      if ( !empty(r) ) return r
   }
   return ''
}
// now find paths and compare :: because every time tree changes
x = get_path (emp.0, root, root )
println(x)
y = get_path (emp.1, root, root )
println(y)
x = x.split('/') ; y = y.split('/')
#(n,M) = minmax( #|x| , #|y| )
i = index ( [0:n] ) :: { x[ $.item ] !=  y[ $.item ] }
println ( x[i-1] )
```
```scala
import java.util.HashMap;
import java.util.Map;

/**
 * Created by NoOne on 18/10/16.
 */
public class Main {

    static String s = "Frank->Mary,Mary->Sam,Mary->Bob,Sam->Katie,Sam->Pete,Bob->John,Bob,Katie" ;

    static Map<String,String> createDict(String[] items){
        Map<String,String> m = new HashMap();
        for ( int i = 0 ; i < items.length - 2 ; i++ ){
            String[] pair = items[i].split("->");
            m.put( pair[1], pair[0] );
        }
        return m;
    }

    static String[] pathToRoot( String employee, Map<String,String> tree){
        StringBuffer buf = new StringBuffer();
        while ( tree.containsKey( employee ) ){
            buf.append( employee ).append( "\t") ;
            employee = tree.get( employee );
        }
        buf.append( employee );
        return buf.toString().split( "\t" );
    }

    static void doIBM( String s ){
        String[] values = s.split(",");
        Map<String,String> tree = createDict( values );
        String emp1 = values[ values.length -2 ] ;
        String emp2 = values[ values.length -1 ] ;
        String[] path1 = pathToRoot( emp1 , tree ) ;
        String[] path2 = pathToRoot( emp2 , tree ) ;
        int len = path1.length > path2.length ? path1.length : path2.length ;
        String resolver = "" ;
        for ( int i = 0; i <  len ; i++ ){
            if  ( !path1[ path1.length - 1 - i ].equals( path2[ path2.length -1 - i] ) ) {
                break;
            }
            resolver = path1[ path1.length - 1 - i ];
        }
        System.out.println(resolver);
    }

    public static void main(String[] args){
        doIBM(s);
    }
}
```
```scala
def path_2_root( emp, tree ){
  path = ''
  while ( emp @ tree ){
    path += ( emp + '\t' )
    emp = tree[emp]
  }
  path += emp 
  return path.split('\t')
}

def do_ibm( s ){
  values = s.split(",")
  tree = dict ( [0: size(values) - 2 ] ) -> { 
    pair =  values[ $.item ].split('->') 
    [ pair[1] , pair[0] ] 
  }
  emp1 = values[-2]
  emp2 = values[-1]
  path1 = path_2_root( emp1 , tree )
  path2 = path_2_root( emp2 , tree )
  len = size( path1 ) > size( path2 ) ? size( path1 ) : size( path2 )  
  resolver = fold( [1:len] , '' ) -> {
    break ( path1[ - $.item ] != path2[ - $.item ] )
    $.prev = path1[ - $.item ]
  }
}
```

## Q159 
### Problem 
 >// Imagine you have an array of N messages which is consistently having new messages added to it. // When you loop through the array, the messages are guaranteed to be ordered chronologically by timestamp.// Write some code that loops through the array of messages and when a unique user_id has two messages within one second of each other, call a method too_fast that takes in two parameters, the first is the older message, the second is the newer message.// In the message examples below we would expect to call the too_fast method with the first and third message, and then again with the third and fifth message.// message example 1 = { "container_id": 123, "item_id": 456, "success": true, "timestamp": 1499351653, "user_id": 789 }// message example 2 = { "container_id": 111, "item_id": 222, "success": false, "timestamp": 1499351654, "user_id": 333 }// message example 3 = { "container_id": 444, "item_id": 555, "success": true, "timestamp": 1499351654, "user_id": 789 }// message example 4 = { "container_id": 123, "item_id": 456, "success": true, "timestamp": 1499351655, "user_id": 999 }// message example 5 = { "container_id": 123, "item_id": 456, "success": true, "timestamp": 1499351655, "user_id": 789 }// Within the loop you can access attributes by using message.getContainerId() or message.getItemId for example, message.getTimestamp() 


This question originally appeared [here](https://careercup.com/question?id=4889325677314048).
Tagged with : Amazon 

### Solution 

```scala
def fire_func( iterator ){
  map = dict()
  while ( iterator.hasNext ){
    item = iterator.next()
    if ( item.user_id @ map ){
      // i found it in map, so check old time 
       diff = item.timestamp - map[user_id].timestamp 
       if ( diff <= critical ){
        // that is the requirement anyways 
         too_fast( map[user_id], item )  
       }
    } 
    // new entry, store new time - always
    map[user_id] = item 
    // now, expire items in the map ?
    cur_time = int( time() )
    // a base delay beyond which message will not come 
    critical_diff = diff * 10  
    map = dict ( map ) as { 
      continue( cur_time - $.value.timestamp > critical_diff )
      $.o // else, preserve  
    }
  }
}
```

## Q160 
### Problem 
 >1. Find if two strings are palindrome2. Given a list of strings, check if concatenating any two string would form a palindrome. Brute force way to do this is O(n^3), he asked ways to optimize this. 


This question originally appeared [here](https://careercup.com/question?id=5646004041809920).
Tagged with : Software Engineer / Developer,Algorithm 

### Solution 

```scala
/* ZoomBA
Note that  sivapraneethalli is kind of correct.
A much better way to achieve this is to see this :
xy is a palindrome iff :
 y = ( x ** -1 ) or 
 y = ( x[0:-2] ** -1 )   

As an example : 
1.  x = abc 
    y = cba 
2.  x = abc 
    y = ba 

Thus, a better algo is to store the potential y's as keys to 
to a map, and value being that of x in a list.
Hence, we can get it done in two passes.     
*/

def find_pair_palindromes( strings ){
  map = fold( strings , dict() ) -> {
    x = $.o 
    key = str( x ** -1 )
    $.p[ key ] = $.i // store index
    if ( size(x) > 1 ){
      key = str( x[0:-2] ** -1 )
      $.p[ key ] = $.i // store index
    }
    $.p // return 
  }
  fold ( strings ) -> { 
    y = $.o 
    if ( y @ map ){ printf( '%s %s\n', strings[ map[y] ] , y ) }
  }
}

listOfWords = list(  "shiva" ,  "and" , "are" , "vihs" , "avihs" )
find_pair_palindromes( listOfWords )
```

## Q161 
### Problem 
 >This questing was something related to parse trees. I really don't remember the semantics but needed to extract the complete sentences from the provided parse tress.Input: A full sentence: (S (NP (NNP James)) (VP (VBZ is) (NP (NP (DT a) (NN boy)) (VP (VBG eating) (NP (NNS sausages))))))Output: James is a boy eating sausagesInput: (NNS Sausages)Output: SausagesInput: (NP(DT a) (NN boy))Output: a boy 


This question originally appeared [here](https://careercup.com/question?id=5640880330375168).
Tagged with : IBM,Software Engineer / Developer,Java 

### Solution 

```scala
// ZoomBA
tree =  { "NP" : [ { "DT" :  "a" } , { "NN" : "boy" } ] }
def traverse( node , s  ){
    names = list( node.keySet )
    name = names.0 // yes, we did it ourselves
    value = node[name]
    if ( value isa [ ] ){
       for ( child : value ){
          traverse ( child , s )
       }
    }else{
       s += value
    }
}
s = list()
traverse ( tree, s )
println( str(s, ' ') )
```
```scala
//ZoomBA
s = "(S (NP (NNP James)) (VP (VBZ is) (NP (NP (DT a) (NN boy)) (VP (VBG eating) (NP (NNS sausages))))))"
def simplified_solution(s){
   // observe that every ')' previous to that is the word to add, so :
   cur = 0
   len = #|s|
   words = list()
   while ( cur < len  ){
     r = index ( [cur: len] ) :: { s[$.item] == ')' } + cur
     break ( r >= len )
     l = rindex ( [ cur:r ] ) :: { s[$.item] == ' ' } + cur
     break ( l >= len )
     w = s[l+1:r-1]
     if ( !empty( w.trim() ) ) { words += w }
     cur = r + 1
   }
   println ( str ( words , ' ' ) )
}
simplified_solution( s )
```

## Q162 
### Problem 
 >You have a array with integers:[ 1, -2, 0, 6, 2, -4, 6, 6 ]You need to write a function which will evenly return indexes of a max value in the array.In the example below max value is 6, and its positions are 3, 6 and 7. So each run function should return random index from the set.Try to implement with O(n) for computation and memory.Try to reduce memory complexity to O(1). 


This question originally appeared [here](https://careercup.com/question?id=5669417594650624).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
/* 
Find min,max in O(n)
Select all indices where max.
*/
a = [ 1, -2, 0, 6, 2, -4, 6, 6 ]
#(min,MAX) = minmax(a)
indices = select(a) where { $.o == MAX } as { $.index }
println(indices)
```

## Q163 
### Problem 
 >Design an algorithm to find the shortest substring in a synopsis such that it contains all the words in a provided list. So, search for the shortest substring that contains ['Hello', 'World']. 


This question originally appeared [here](https://careercup.com/question?id=5732530092244992).
Tagged with : Amazon,Software Engineer / Developer,Algorithm 

### Solution 

```scala
{ "this" , "is", "probably", "a" ,  "bad", "idea" , 
   "to", "think" , "the" , "problem" , "in", "this" , "way" }
```

## Q164 
### Problem 
 >Remove duplicates from string given " cutcopypaste " Return "uoyase" 


This question originally appeared [here](https://careercup.com/question?id=5130751994494976).
Tagged with : Accenture,Software Engineer 

### Solution 

```scala
// ZoomBA
s = " cutcopypaste " 
unique = select ( mset( s.value ) ) :: { $.o.value == 1 } -> { $.o.key }
r = str( unique , '' )
println(r)
```

## Q165 
### Problem 
 >Convert number to text. ex. 101 One hundred and one 


This question originally appeared [here](https://careercup.com/question?id=6284213138489344).
Tagged with : SDE-2 

### Solution 

```scala
/*
Should be pretty neat, 
Taking a number and making it word by word 
Using recursion, next step - removing that  
*/
_names_ = [[1,'one'], [2,'two' ], [3,'three'],[ 4 , 'four' ] , [5,'five'] ,
          [6,'six'], [7,'seven' ], [8,'eight'],[ 9 , 'nine' ] , [10,'ten'],
          [11,'eleven'], [12,'twelve' ], [13,'thirteen'],[ 14 , 'fourteen' ] , [15,'fifteen'],
          [16,'sixteen'], [17,'seventeen' ], [18,'eighteen'],[ 19 , 'nineteen' ] ,
          [20,'twenty'], [30,'thirty'], [40,'forty'], [50,'fifty'], [60,'sixty'], [70,'seventy'],
          [80,'eighty'], [90,'ninety'], [100,'hundred'] , [1000, 'thousand' ] ,
          [ 1000000 , 'million' ], [1000000000, 'billion' ] , [1000000000000 , 'trillion' ] ]

_keys_ = dict( _names_ )          
def in_words( n ){
  if ( n == 0 ) return 'zero'
  // try to find the index of the item from the right side 
  // which has the property that division by that item > 0
  i = rindex( _names_ ) :: { (n / $.0 ) > 0 }
  // i would have the index , get result and reminder 
  res = (n / _names_[i][0] )
  rem = (n % _names_[i][0] )
  // recursion, string rep of result is given by ... 
  s_res = ( res @ _keys_ ) ? ( (res != 1 || n >=100 ) ? _keys_[res] : '' ) : in_words(res)
  // recursion, string rep of the reminder is given by ... 
  s_rem = ( rem @ _keys_ ) ? _keys_[rem] : (rem != 0 ? in_words(rem) : '' )
  // finally merge them, and we have our answer - careful not to merge empty strings 
  s_res + (empty(s_res)?'' : ' ')  + _names_[i][1] + (empty(s_rem)?'' : ' ') + s_rem
}

println( in_words(0) )
println( in_words(10) )
println( in_words(1012) )
println( in_words(983) )
println( in_words(87891729817981987) )
println( in_words(4312112121121) )  zero
ten
one thousand twelve
nine hundred eighty three
eighty seven thousand eight hundred ninety one trillion seven hundred twenty nine billion eight hundred seventeen million nine hundred eigh
ty one thousand nine hundred eighty seven
four trillion three hundred twelve billion one hundred twelve million one hundred twenty one thousand one hundred twenty one
```

## Q166 
### Problem 
 >Given infinite supply of coins of denominations 25, 10, 5 and 1, find the distinct number of ways to use the coins to sum up to the given valueFind the best algorithm with the best time complexity 


This question originally appeared [here](https://careercup.com/question?id=5687285707177984).
Tagged with : Akamai,Software Developer 

### Solution 

```scala
// ZoomBA
def ways_to_dist( path, cur_sum , target_value ){
   if ( cur_sum > target_value ) return 0
   if ( cur_sum == target_value  ) {
      println( path )
      return 1
   }
   cnt = 0
   for ( inc : [1,5,10,25 ] ){
     if ( inc >= path[-1] ){
       cnt += ways_to_dist ( path + inc, cur_sum + inc, target_value)
     }
   }
   return cnt
}
println ( ways_to_dist( list(0), 0, 42  ) )
```
```scala
// ZoomBA
def ways_to_dist( target_value ){
   count = 0
   q = list() // create a queue
   q.enqueue ( [0,0,list(0)] ) // add to it
   // now the loop
   while ( !empty(q) ){
     #( prev_inc , cur_sum , path ) = q.dequeue()
     continue ( cur_sum > target_value ) // obvious, nothing else can do
     continue ( cur_sum == target_value  ) {
       println( path ) // for debug diagnostic
       count +=1 // increment possible scenarios
     }
     for ( inc : [1,5,10,25 ] ){
        if ( inc >= prev_inc ){ // ensure sorted path
           q.enqueue( [ inc , cur_sum + inc , path + inc  ] )
        }
     }
   }
   return count
}
println ( ways_to_dist( 42 ))
```

## Q167 
### Problem 
 >A = {1,2,4,-6,5,7,9,....}B = {3, 6, 3, 4, 0 .......}n = 5 -> pairs whose sum is nOutput = (1,4), (5,0).... 


This question originally appeared [here](https://careercup.com/question?id=5705773536509952).
Tagged with : Amazon,Testing / Quality Assurance,Online Test 

### Solution 

```scala
// ZoomBA
A = [1,2,4,-6,5,7,9]
B = [3, 6, 3, 4, 0 ]
res = join ( A, B ) :: { 5 == sum($.o) }
println(res)
```

## Q168 
### Problem 
 >Given a method public int getOccurence(int x,int y);where y is always a single digit number.So find the number of occurrences of number y in the range xE.g.if x=25,y=2function should return 9(as 22 contains two occurrences of 2) - 2,12,20,21,22,23,24,25 


This question originally appeared [here](https://careercup.com/question?id=5712666707361792).
Tagged with : Google,SDE-2 

### Solution 

```scala
def count_digit( num_range, digit ){
    sd = (str(digit))[0]
    sum ( [0:num_range +1] ) -> {
        sum( str($.o).value ) -> { sd == $.o ? 1 : 0 }
    }
}
println( count_digit(25,2) )
```

## Q169 
### Problem 
 >Given some email ids, and a similarity function which says whether two email ids are similar, determine all the sets of email ids that are similar to each other. 


This question originally appeared [here](https://careercup.com/question?id=5643997780377600).
Tagged with : Facebook,Software Engineer,Algorithm 

### Solution 

```scala
A similar_to B  and B similar_to C ==>(implies) A similar_to C
```

## Q170 
### Problem 
 >How can I count frequencies of all elements of an array? 


This question originally appeared [here](https://careercup.com/question?id=5656325364121600).
Tagged with : Algorithm 

### Solution 

```scala
// ZoomBA 
freq = mset ( array )
```

## Q171 
### Problem 
 >Write an algorithm to randomize nodes of a Binary Tree without using any data structures 


This question originally appeared [here](https://careercup.com/question?id=5724800203882496).
Tagged with : A9,abc,Algorithm 

### Solution 

```scala
bin_tree_node = { 'value' : value , 'left' : l , 'right' : r  }
all_nodes = list()
def serialize ( node ){
   if ( node == null ) return 
   all_nodes += node 
   serialize ( node.left )
   serialize ( node.right )
} 
// to a list 
serialize ( root )
// now randomize
shuffle( all_nodes )
// now randomize 
def randomize ( node ){
   if ( !empty(all_nodes) ){
      node.left = all_nodes.remove(0)
   }else{
      node.left = null;
   }
   if ( !empty(all_nodes) ){
      node.right = all_nodes.remove(0)
   }else{
      node.right = null;
   }
   randomize ( node.left )
   randomize ( node.right )
}
root = all_nodes.remove(0)
randomize(root)
```

## Q172 
### Problem 
 >Find all words [A-Z] in a dictionary (about 1M words) that are made of a subset (in any order) of the chars in the input parameter [A-Z].ex: input "ACRAT" (10 to 20 chars, up to 30 worst case)matching words: "A", "CAR", "ACA", "ART", "RAC".  non-matching words: "BAR", "AAA"follow up : the input is a list of words. Return a list of  words that each list is formed by exactly the characters in the input list.   For example: two lists {“DEBIT”, “CARD”} and{“BAD”, “CREDIT”}  are formed by the same exact group of characters. 


This question originally appeared [here](https://careercup.com/question?id=5643906652831744).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/*
Makarand is right.
The best way to handle it is O(nk), where: 
n is the size of the dictionary 
k is the average size of the words in the dictionary by unique characters 
Observe the use of counter, what we are trying to do is multiset.
en.wikipedia.org/wiki/Multiset
Given zoomba defaults into multiset using mset() 
and let's set operation ( subset ) over multisets 
The problem becomes trivial code.
== Appendix : Multiset Subset Operation == 
Given a list/sequence, a multiset is M = { (key,count) }
Now, M1 is subset of M2, if and only if :
for all key in M1, 
  1. key exists in M2
  2. count of key in M1 <= count of key in M2  
==
And now the solution 
*/
// step 1, load dictionary and make each entry multiset :: one time
md = list ( file('/BigPackages/words.txt') ) as { [ $.o, mset( $.o.toCharArray ) ]  }
// get input word 
input_word = 'makarand'
test_word = mset( input_word.toCharArray )
// where each word in md is a subset ( multiset sense ) of test_word O(n*k)
subset_words = select ( md ) where {  $.o.1 <= test_word } as { $.o.0 }
// we are done 
println( subset_words )  [ a,aa,aaa,aam,ad,adam,adar,adm,adman,ak,aka,akan,akra,am,ama,amadan,amar,amarna,amra,an,ana,anam,anama,and,anda,ankara,ar,ara,arad,arak,arank,ark,arm,armada,arn,arna,d,da,dak,dam,dama,daman,damar,damn,dan,dana,dank,dar,dark,darn,dk,dkm,dm,dn,dr,dram,drama,drank,,k,ka,kaama,kam,kama,kan,kana,kanara,kand,karanda,karma,karn,km,kn,knar,kr,kra,krama,kran,krna,m,ma,maad,maana,maar,mad,makar,makara,makran,man,mana,manada,manak,mand,mandar,mandra,mank,mar,mara,mark,marka,md,mk,mn,mna,mr,n,na,naa,naam,nad,nada,nak,nam,namda,nar,nard,nark,nd,nm,nr,r,ra,raad,rad,rada,radman,rakan,ram,ramada,ramadan,ran,rana,rand,rank,rd,rm,rn,rnd, ]
```

## Q173 
### Problem 
 >write a  function that randomly return only odd number in range [min, max)public int getRandomOdd(int min, int max){} 


This question originally appeared [here](https://careercup.com/question?id=5141506538078208).
Tagged with : Google,Software Developer 

### Solution 

```scala
def getRandomOdd( min, max){
  r = random()
  while ( (n = r.num(min,max)) % 2 == 0 );
  n // return n 
}  def getRandomOdd( min, max){
  r = random()
  y_min = min/2 
  if ( 2 /? max ){ max - 1 }
  y_max = max/2 
  2 * r.num( y_min, y_max ) + 1 
}
```

## Q174 
### Problem 
 >You will be given a sequence of passages, and must filter out any passage whose text (sequence of whitespace-delimited words) is wholly contained as a sub-passage of one or more of the other passages.When comparing for containment, certain rules must be followed:The case of alphabetic characters should be ignoredLeading and trailing whitespace should be ignoredAny other block of contiguous whitespace should be treated as a single spacenon-alphanumeric character should be ignored, white space should be retainedDuplicates must also be filtered - if two passages are considered equal with respect to the comparison rules listed above, only the shortest should be retained. If they are also the same length, the earlier one in the input sequence should be kept. The retained passages should be output in their original form (identical to the input passage), and in the same order.Input: For each test case a single line comprising the passages (strings) to be processed, delimited by | characters. The | characters are not considered part of any passage.Output: A single line of filtered passages in the same |-delimited format.Input1: IBM cognitive computing|IBM "cognitive" computing is a revolution| ibm cognitive computing|'IBM Cognitive Computing' is a revolution? Output1: IBM "cognitive" computing is a revolutionInput2: IBM cognitive computing|IBM "cognitive" computing is a revolution|the cognitive computing is a revolutionOutput2: IBM "cognitive" computing is a revolution|the cognitive computing is a revolution 


This question originally appeared [here](https://careercup.com/question?id=5164991108874240).
Tagged with : IBM,Software Engineer / Developer,Java 

### Solution 

```scala
s = [ "IBM cognitive computing" , 'IBM "cognitive" computing is a revolution' , 
        "ibm cognitive computing" , "'IBM Cognitive Computing' is a revolution?" ]
// shows the power of full declarative framework 
#(full,final) = fold ( s , [ '' , '' ] ) -> { 
  // token splitting of the current statement, basing the rules
  words = tokens( $.item.toLowerCase , '[a-z0-9]+') 
  key = str( words , ' ' ) // preserve single whitespace if need be 
  previous_key = $.previous.0 // the fold structure 
  previous_value = $.previous.1 // unwinding 
  continue ( !(previous_key @ key) ||  
    ( previous_key == key && #|$.item| > #|previous_value| ) ) 
  // when the continue did not happen, return the current as the latest    
  [ key, $.item ]
}
println( final )
```
```scala
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by NoOne on 17/10/16.
 */
public class Main {

    static String[] s = new String[] { "IBM cognitive computing" , "IBM \"cognitive\" computing is a revolution" ,
            "ibm cognitive computing" , "'IBM Cognitive Computing' is a revolution?" };


    static List<String> tokenize(String text, String regex){
        Pattern p = Pattern.compile( regex , Pattern.DOTALL);
        Matcher m = p.matcher( text );
        List<String> l = new ArrayList<>();
        while ( m.find() ){
            l.add(m.group());
        }
        return l;
    }

    static String keyString( List l, String sep){
        StringBuffer buf = new StringBuffer();
        for ( Object o : l ){
            buf.append( o ).append(sep);
        }
        String s = buf.toString();
        return s.substring(0,s.length());
    }

    static void doIBM( String[] s ){
        String previousKey = "" ;
        String previousValue = "" ;
        for ( String item : s ){
            List words = tokenize( item.toLowerCase() , "[a-z0-9]+");
            String key = keyString( words , " " );
            if ( key.indexOf( previousKey ) < 0 ||
            ( previousKey.equals(key) &&  item.length() > previousValue.length() ) ){
                continue;
            }
            previousKey = key ;
            previousValue = item ;
        }
        System.out.println(previousValue);
    }

    public static void main(String[] args){
        doIBM(s);
    }
}
```

## Q175 
### Problem 
 >Given a number, return the count of numbers having non-repeating digits till that number starting from 1? 


This question originally appeared [here](https://careercup.com/question?id=5725943181082624).
Tagged with : InMobi,Algorithm 

### Solution 

```scala
n = 42 
t = sum ( [1: n + 1 ] ) -> {
  x = str($.o)  
  #|set( x.value )| == #|x| ? 1 : 0  
}
println( t )
```

## Q176 
### Problem 
 >Given two big files merge the files on to a third file such that the lines interleave. 


This question originally appeared [here](https://careercup.com/question?id=5728199582416896).
Tagged with : EFI,Software Engineer,design 

### Solution 

```scala
// get file iterator - line by line 
fi1 = file('large1.txt')
fi2 = file('large2.txt')
fo = open('merged' ,'w')
// when both are non empty - interleave 
while ( fi1.hasNext && fi2.hasNext  ){
  fo.println( fi1.next )
  fo.println( fi2.next )  
}
// when one is leftover, find it and drop lines 
if ( fi1.hasNext || fi2.hasNext ){
  leftover = fi1.hasNext ? fi1 : fi2 
  while ( leftover.hasNext ){
    fo.println( leftover.next )  
  }
}
// do not forget to close 
fo.close()
```

## Q177 
### Problem 
 >x={a,b,c}, y={p,q}, z={r,s} Define aOperation, x * y * z = {{a,p,r},{a,p,s},{a,q,r},{a,q,s}......{c,q s}}Is to output all the results in the order of each subset, implementing a class iterator that has Next() and hasNext() methods 


This question originally appeared [here](https://careercup.com/question?id=5702569199403008).
Tagged with : Amazon,Backend Developer 

### Solution 

```scala
/* 
Observe the problem of cartesian product from arbitrary no.
of lists. Suppose the lists are : L1, L2, L3, ... Ln
with sizes s1, s2, s3, .. sn
Let the element in the i'th position of k'th list is Lk[i].
The configuration of such indices at any point is, then :
0 0 0 0 ... 0 // n times for the first tuple
0 0 0 0 ....1 // for the 2nd tuple...
...
0 0 0 0 ....(sn-1) // for the sn Tuple.
After than, the first column will be reset, and carry will be generated, 
so that the next tuple will be:
0 0 0 0 ...1 0  // for the ( sn + 1) Tuple.

Thus, generating next tuple's index is generating the carry, if any, 
and resetting the indices from the index which generated the carry to the end, 
and incrementing the index of the left one. This again can generate carry, 
so it is a ripple effect from rightmost side to the left most side.
Thus, hasNext() is always possible, till the leftmost list is not exhausted by carry.
next() is the tricky one, where we need to define the carry and the ripple from right to left.
When there is no carry, we can stop having the ripple.
*/
def inc_iterator( some_list, cur_iterator ){
  if ( cur_iterator.hasNext ){
    return [ false , cur_iterator ]
  } else {
    return [ true, some_list.iterator ]
  }
}
def cartesian ( list_of_list ){
  iterators = list ( list_of_list ) as { $.o.iterator } // get the iterators 
  my_tuple = select( iterators ) where { $.o.hasNext() } as  { $.o.next() }
  if ( size( my_tuple) < size(list_of_list) ) return [] // empty tuple  
  println( my_tuple )
  carry = false 
  while ( !carry ){
    for ( i : [ size(list_of_list) - 1 : -1 ] ){
       #(carry,iter) = inc_iterator( list_of_list[i], iterators[i] )
       if ( i == 0 && carry ) { return } // because now I am having carry to the left
       iterators[i] = iter 
       my_tuple[i] = iter.next 
       break ( i != 0 && !carry ) 
    }
    println( my_tuple ) 
  }
}
cartesian ( [ ['a','b', 'c'] , ['p', 'q' ], ['r','s' ] ] )
```

## Q178 
### Problem 
 >Write code for transforming equation into canonical form. An equation can be of any order. It may contain any amount of variables and parentheses.The equation will be given in the following form:P1 + P2 + ... = ... + PNwhere P1..PN - terms that look like: ax^kwhere a - floating point value;k - integer value;x - variable (each term can have many variables). For example:x^2 + 3.5xy + y = y^2 - xy + yShould be transformed into:x^2 - y^2 + 4.5xy = 0 


This question originally appeared [here](https://careercup.com/question?id=5730931131285504).
Tagged with : Coding 

### Solution 

```scala
// ZoomBA
def term_parse( s ){
  term = [ 0.0 , '' ]
  p = tokens('^[\+\-]?(\d+(\.\d+)?)?')
  m = p.matcher( s ) ; m.find() // true always 
  opts = { '' : 1.0 , '+' : 1.0 , '-' : -1.0 }
  if ( m.group @ opts ) {
    term[0] = opts[ m.group ] 
  } else {
    term[0] = float( m.group ) 
  }
  inx = index ( s.value ) :: { str($.item) =~ '[a-zA-Z]' }
  term[1] = s[ (inx < 0 ? 0 : inx) : -1 ] 
  if ( inx < 0 ){ term[1] = '' }
  term 
}
def gen_expr( s ){
  l = tokens(s, '[\+\-]?[^\+\-]+') -> { term_parse( $.item ) }
  d = mset ( l ) -> {  $.item.1 }
}
def normalize( s ){
  s = s.replace(' ','')
  #(l,r) = s.split('=')
  ld = gen_expr ( l )
  rd = gen_expr ( r )
  // merge 
  u = ld.keys | rd.keys // union 
  poly = list ( u ) -> { 
     k = $.item ; coeff = 0.0 
     if ( k @ ld ){ coeff += sum ( ld[k] )->{  $.item[0] }  }
     if ( k @ rd ){ coeff -= sum ( rd[k] )->{  $.item[0] }  }
     [ k , coeff ]
  }
  sortd ( poly ) :: { #(l,r) = $.item ; l.0 < r.0 }
  str( poly , '' ) -> { #(k,c) = $.item  ; str( '%+f%s' , c , k ) } 
}
x = normalize ( 'x^2 + 3.5xy + y = y^2 - xy + y + 10' )
println ( x )
```

## Q179 
### Problem 
 >Generate square of numbers in an array example [1,3,5] should come out as [1,9,25]. 


This question originally appeared [here](https://careercup.com/question?id=5735749430280192).
Tagged with : Facebook 

### Solution 

```scala
// ZoomBA
// Not in place 
l = list ( arr ) -> { $.o ** 2 }
// in place 
fold ( arr ) -> { $.c[$.i] = $.o ** 2 }
```

## Q180 
### Problem 
 >You are given an array of strings and are supposed to return a boolean if the words can be chained. Two words can be chained only if the difference between them is one character. For example, "cat" and "bat" can be chained but "ab" and "ba" cannot. There can be duplicates in the array and the chain can start from anywhere and end anywhere but all the words must be included. It is enough to return true when at least one such chain is found 


This question originally appeared [here](https://careercup.com/question?id=5695063272194048).
Tagged with :  

### Solution 

```scala
/* 
An edge - if hamming distance between two strings is 1
For this problem assume that the string lengths must be same.
Not an important aberration 
A chain over a collection of strings : 
   Treat each string as a node.
   Given a sequence of node exists : 
      1. such that no node repeats
      2. each node is connected to previous by an edge
      3. Span of the chain is the nodes connected by the chain 
A complete chain over a collection of strings:
    A chain exists that spans over the whole collection.
Thus, the problem : does a complete chain exist for a string collection?    
We can solve it using ZoomBA's predefined permutations perm.
It will generate all possible tuples - and if we can have one that matches 
what the description is - we catch it.
We are not saying it is optimal - we are saying it is cool!        
*/
def is_dist_1(s1, s2){
  // their length differs by max 1 ?
  diff_len = #| #|s1| - #|s2| | 
  if ( diff_len != 0 ) return false
  i = index( s1.value ) :: { s2[$.i] != $.o }
  // diff is one char only 
  rindex( s1.value ) :: { s2[$.i] != $.o } == i 
}
// this gets the chain up....
def possible_chain( strings ){
  v = find( perm( strings ) ) :: { 
    p = $.o 
    !exists( p ) :: { $.i > 0 &&  !is_dist_1( p[$.i -1], p[$.i] ) } 
  }
  v.nil?'nothing found' : v.value 
}

A = [ "abc", "aba", "bba", "tbb", "cbb" ]
B = ["abc", "aba", "bba", "tbb", "cbb", "cba"]
C = ["abc", "bba", "bbt", "aba"]
println( possible_chain(A) ) // nothing found 
println( possible_chain(B) ) // @[ abc,aba,bba,cba,cbb,tbb ] 
println( possible_chain(C) ) // @[ abc,aba,bba,bbt ]
// we are less than 42 lines, even with so many comments!
```

## Q181 
### Problem 
 >How many Fibonacci numbers exists less than a given number n.Can you find a function in terms of n , to get the number of fibonacci number less than n.Example : n = 6 Answer: 6 as (0, 1, 1, 2, 3, 5) 


This question originally appeared [here](https://careercup.com/question?id=5713892824055808).
Tagged with : Google,Software Developer,Algorithm 

### Solution 

```scala
// ZoomBA, showcasing iterator object creation.
// This is infinite iterator. 
def FibGen :{ previous : [ 0, 1] , 
      $next : def(){
      cur = $.previous[0] + $.previous[1]
      $.previous[0] = $.previous[1]
      $.previous[1] = cur 
    } 
}
fg = new ( FibGen )
n = 6 
count = lfold ( fg, 2 ) -> { break( $.o >= n ) ; $.p += 1 }
println( count )
```

## Q182 
### Problem 
 >Find the first unrepeated character in a given string. Solve this in a single pass. 


This question originally appeared [here](https://careercup.com/question?id=5769433499828224).
Tagged with : Yahoo,Software Engineer / Developer,Algorithm 

### Solution 

```scala
// ZoomBA
def single_pass( string ){
   l = list()
   set( string.value ) ->{
     if ( $.item @ $.partial ){
       l -=  $.item 
     }else{
       l += $.item 
     }
     $.item // is the key 
   }
   empty(l) ? 'Nothing' : l[0] 
}
```

## Q183 
### Problem 
 >In product System , we receive lots of data and in some case data contains duplicate . In order to process the data correctly we need to identify the row which contains the duplicate .  In this problem , You are receiving data onto 2d Array (m*n ) formate  you and need to find the number of row containing duly data based on on criteria .  Opretaion 1 Return the number of row containing the duplicate data in rows ( minimum count of duplicates can vary ) .  Opreation 2 Retrun the number of rows wherever there are duplicates in the column across the row (minimum count of of duplicates to consider the row can varry )  if none of the above operating the program will return - 1 .  static int findDuplicates(int[][] data, int operation, int numOfDuplicates) { }  Example  1,3,5,9 1,2,1,2 1,4,7,9 20,25,20,35   Operation 1  answer  = 2  Operation 2 numOfDuplicate=2  then  answer = 1 


This question originally appeared [here](https://careercup.com/question?id=5127399114014720).
Tagged with : Adjetter Media Network Pvt Ltd.,Java Developer 

### Solution 

```scala
// ZoomBA : the data 
data = [ [1,3,5,9],
   [1,2,1,2],
   [1,4,7,9],
   [20,25,20,35] ]
// operation 1 :
repeat_in_rows = sum ( data ) -> { (size( set($.o) ) != size($.o) )? 1:0 }
println( repeat_in_rows )
// operation 2 :
num_dup = 2 
num_dup_in_rows = sum ( data ) -> { 
  // get mset, key, num_of_occurences 
  ms = mset($.o) 
  // when there are at least 2 keys with num_of_occurences >= num_dup :: dup 
  dups = select( ms.values ) :: { break( size($.p) >= num_dup ) ; $.o > 1 }
  // simple 
  size(dups) == num_dup ? 1 : 0  
}
println( num_dup_in_rows )
```

## Q184 
### Problem 
 >Print all permutations of a given string. 


This question originally appeared [here](https://careercup.com/question?id=5738113449066496).
Tagged with : Facebook,Software Engineer 

### Solution 

```scala
/* 
A nice trick to find permutations is use integers with base b.
Suppose there is a string of length b, with all unique characters.
Now, that means, the string can be represented as a base b integer.
Permutaions, of the string can be now found by incrementing the number, 
such that all digits are unique.
Now a demonstration:
abc : 3 chars.
3 digit number, and base 3.
The digits are: a:0, b:1, c:2
012 021 102 120 201 210 
--> map_back to -->
abc acb bac bca cab cba 
and we are done.
This can be easily implemented:
*/
def map_back(encoded, string){
  str( encoded.value ,'' ) -> { string[int($.o)] }
}

def permutations( string ) {
    // imagine all chars are unique, else there will be repeatation 
    b = size(string)
    // start with min.
    min_str = '0' + str( [1:b] , '' ) -> { str($.o,b) }
    max_str = min_str ** -1
    min = int(min_str,b,0)
    max = int(max_str,b,0)
    perms = list()
    perms += map_back(min_str,string) 
    for ( x : [ min + 1 : max  ] ){
      str_x = str(x,b)
      if ( size(str_x) < b ){ str_x = '0' + str_x }
      if ( size( set(str_x.value) ) == b ){
          // all different - map it 
          perms += map_back(str_x,string)
      }  
    }  
    perms += map_back(max_str,string) 
}
p = permutations( "abc" )
println(p)
```

## Q185 
### Problem 
 >Given a pattern and a string, return a boolean of whether the string matches the pattern.Each character in pattern represents one of more characters in string.Method: is_match(pattern, string)Sample Testcasesis_match('abba', 'dogfishfishdog') -> Trueis_match('aba', 'dogfishdog') -> Trueis_match('a', 'acdefghijk') -> Trueis_match('ab', 'acdefghijk') -> True is_match('aba', 'dogfishfish') -> Falseis_match('aba', 'dogfishhorse') -> FalsePreferable use Python, and say the Big O runtime and space. 


This question originally appeared [here](https://careercup.com/question?id=6290452837826560).
Tagged with : Algorithm 

### Solution 

```scala
// ZoomBA
def is_match( string, pattern ){
   string =~ pattern 
}
println( is_match( '101' ,'\d+' )
```

## Q186 
### Problem 
 >Check if any number is sum of any two numbers in array of length nExample: [40,90,50] -> check -> true (90 = 40 + 50)Example: [1,2,4] -> check -> false( A_x != A_y + A_z) 


This question originally appeared [here](https://careercup.com/question?id=5642495974178816).
Tagged with : Web Developer,Algorithm 

### Solution 

```scala
arr = [-2,-1,0,1, 3 ]
x = sum_in_array_sort(arr,-1)
print (x)
```

## Q187 
### Problem 
 >Given infinite supply of coins of denominations 25, 10, 5 and 1, find the distinct number of ways to use the coins to sum up to the given value 


This question originally appeared [here](https://careercup.com/question?id=6232486708248576).
Tagged with : Adobe,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
def ways_to_dist( target_value ){
   count = 0
   q = list() // create a queue
   q.enqueue ( [0,0,list(0)] ) // add to it
   // now the loop
   while ( !empty(q) ){
     #( prev_inc , cur_sum , path ) = q.dequeue()
     continue ( cur_sum > target_value ) // obvious, nothing else can do
     continue ( cur_sum == target_value  ) {
       println( path ) // for debug diagnostic
       count +=1 // increment possible scenarios
     }
     for ( inc : [1,5,10,25 ] ){
        if ( inc >= prev_inc ){ // ensure sorted path
           q.enqueue( [ inc , cur_sum + inc , path + inc  ] )
        }
     }
   }
   return count
}
println ( ways_to_dist( 42 ))
```
```scala
if ( inc >= prev_inc ){ // ensure sorted path
```

## Q188 
### Problem 
 >Given two sorted integer arrays, find the median element. Note that for an even sized collection, median element is to be defined as the average of the central two elements. 


This question originally appeared [here](https://careercup.com/question?id=5707031095803904).
Tagged with : Goldman Sachs,Software Architect,Algorithm 

### Solution 

```scala
def find_median_lazy( a, b ){
  s = sset() // sorted set 
  s += a 
  s += b 
  l = list(s) // done, to get indexer up 
  len = size(l)
  if ( len == 0 ) return null
  // and now the rest 
  mid = len/2 
  if ( len % 2 == 0 ){
    // even case, then 
    return (l[mid-1] + l[mid])/2.0
  }
  return l[mid] 
}
println( find_median_lazy( [1,2] , [3,4] ) )
println( find_median_lazy( [1,2,5] , [3,4] ) )

def _merge_(a,b){
  l = list()
  i_a = 0 
  l_a = size(a)
  i_b = 0 
  l_b = size(b)
  while ( i_a < l_a && i_b < l_b ){
    if ( a[i_a] < b[i_b] ){
      l += a[i_a]
      i_a += 1 
    } else {
      l += b[i_b]
      i_b += 1
    }
  }
  while ( i_a < l_a ){
    l += a[i_a]
    i_a += 1
  }
  while ( i_b < l_b ){
    l += b[i_b]
    i_b += 1
  }
  l // return 
}
def find_median_with_unnecessary_work(a,b){
  l = _merge_(a,b)
  len = size(l)
  if ( len == 0 ) return null
  // and now the rest 
  mid = len/2 
  if ( len % 2 == 0 ){
    // even case, then 
    return (l[mid-1] + l[mid])/2.0
  }
  return l[mid]
}

println( find_median_with_unnecessary_work( [1,2] , [3,4] ) )
println( find_median_with_unnecessary_work( [1,2,5] , [3,4] ) )
```

## Q189 
### Problem 
 >given a stream of natural numbers ,and a array J contains integers in increasing ordersoperations performed J = [2,3,4]1 2 3 4 5 6 7 8 9 10…………..27....100...1111first operationJ[0] = 2 => remove every 2nd integernow the stream is1 3 5 7 … 27J[1] = 3remove every 3rdstream is now1 3 7 …    3rdgiven a natural number n , find if it will survive given J, or at what index it will die. 


This question originally appeared [here](https://careercup.com/question?id=5107149794443264).
Tagged with : Amazon,Software Engineer in Test,Algorithm 

### Solution 

```scala
// returns -1 if does not go bye bye, 
// else returns the index 
will_bye_bye( n , J ){
  // one liner in ZoomBA
  r = [1:n+1]
  for ( i = 0 ; i < size(J) ; i+=1 ){
    // select those whose index is divisible by the item index 
    r = select ( r ) :: {  J[i] /? ($.index + 1) } 
    if( empty(r) || r[-1] != n ){ return i }
  }
  return -1
}
```

## Q190 
### Problem 
 >I have a file which has a number of 10 digit  numerals and 10 digit alphanumeric characters. Write a UNIX basic command to print distinct 10 digit alphanumeric chartersSample Input12345678901234567890123456789X0974385495Expected O/P123456789X 


This question originally appeared [here](https://careercup.com/question?id=6258651296694272).
Tagged with : Amazon,SDE1,Unix 

### Solution 

```scala
uniq <file_name> | grep "[a-zA-Z]"
```

## Q191 
### Problem 
 >Given an array arr and a number n, you have to find whether there exist a subset in arr whose sum is n. You have to print length of the subset.1. There exists only one subset like that2. All number in arr are positive 


This question originally appeared [here](https://careercup.com/question?id=5760923399618560).
Tagged with : Amazon,SDE1,Dynamic Programming 

### Solution 

```scala
/* Sounds like cheating - it is not 
   The problem of iterating over power set 
   is done by sequences() function */
a = [1,2,3,20,4,9,89,54,21]
k = 16 
v = find( sequences( [0:size(a)] ) ) :: {
  sum( $.o ) -> { a[$.o] } == k 
}
printf( 'Indices are: %s, size: %d\n', v.value, size(v.value) )
```

## Q192 
### Problem 
 >Find the number of ways you can have breakfast in 'n' days, given Bread-butter can be eaten every day, Pizza can be eaten every alternate day and Burger can be eaten every two days. 


This question originally appeared [here](https://careercup.com/question?id=5666933727821824).
Tagged with : Microsoft,Applications Developer,Dynamic Programming 

### Solution 

```scala
/*
Assuming I can have any items, for breakfast.
That is, I can have bread-butter, as well as Pizza together
I know, I am a foodie.
*/
_options_ = [ 'bread-butter' , 'pizza' , 'burger' ]
_combinations_ = list ( sequences(_options_) ) // yep, neat!
println(_combinations_)
// is the combination valid 
def is_valid_combination( items, history ){
   if ( empty(history) ) return true 
   if ( 'pizza' @ items && 'pizza' @ history[-1] ) return false 
   if ( 'burger' @ items ){
       if ( 'burger' @ history[-1] ) return false 
       if ( size(history) > 1 && 'burger' @ history[-2] ) return false  
   }  
   return true 
}

// the recursive definition 
def _recurse_(days, history){
   if ( size(history) == days ){
       printf('%s\n', history )
       return
   }
   for ( c : _combinations_ ){
       if ( is_valid_combination( c, history ) ){
           h = list(history)
           h.add( c )
           _recurse_(days,h) 
       }
   }
}
// call it 
def gen_combo( days ){
   _recurse_( days,[])
} 
gen_combo(3)
```
```scala
/*
Now, for the health concious, who eats only one item.
Surprize, same code, almost!
*/
_options_ = [ 'bread-butter' , 'pizza' , 'burger' ]
// is the combination valid 
def is_valid_combination( item , history ){
   if ( empty(history) ) return true 
   if ( 'pizza' == item && 'pizza' == history[-1] ) return false 
   if ( 'burger' == item ){
       if ( 'burger' == history[-1] ) return false 
       if ( size(history) > 1 && 'burger' == history[-2] ) return false  
   }  
   return true 
}

// the recursive definition 
def _recurse_(days, history){
   if ( size(history) == days ){
       printf('%s\n', history )
       return
   }
   for ( c : _options_ ){
       if ( is_valid_combination( c, history ) ){
           h = list(history)
           h.add( c )
           _recurse_(days,h) 
       }
   }
}
// call it 
def gen_combo( days ){
   _recurse_( days,[])
} 
gen_combo(3)
```

## Q193 
### Problem 
 >Get an 0. From user and display it’s prime factorExample 24 is 2,2,2,3 and 55 is 5,11 


This question originally appeared [here](https://careercup.com/question?id=5742470735331328).
Tagged with : HTC Global Services,Software Developer,C++ 

### Solution 

```scala
// ZoomBA
def prime_factors( n ){
#( primes, factors ) = lfold ( [2:n+1] , [ list() , dict() ] ) -> { 
    cur = $.o ; partial_primes = $.p.0 ; partial_factors = $.p.1
    is_prime_cur = !exists ( partial_primes ) :: {  $.o /? cur }
    continue ( !is_prime_cur )
    partial_primes += cur 
    multiplicty = 0 
    for ( x = n ; cur /? x ; x/= cur ){ multiplicty += 1 } 
    if ( multiplicty > 0 ){ partial_factors[cur] = multiplicty }
    $.p 
  } 
  factors 
}
println( prime_factors ( 36 ) )
```
```scala
// ZoomBA
def prime_factors( n ){
 select ( lfold ( [2:n+1] , dict() ) -> { 
    continue ( exists ( $.p.keySet ) :: {  $.o /? $.$.o } )
    $.p[$.o] = 0  
    for ( x = n ; $.o /? x ; x/= $.o ){ $.p[$.o] += 1 } 
    $.p 
  } ) :: { $.o.value > 0 } 
}
println( prime_factors ( 36 ) )
```

## Q194 
### Problem 
 >given 2 strings A and B. generate all possible solutions when B is merged in A.Ex: A =  "hey"B: "sam"then solutions are :heysam,hseaym,hesaym,sahemy etc.notice that order should be the same for both of strings while merging. 


This question originally appeared [here](https://careercup.com/question?id=5663263489523712).
Tagged with : Google,Software Developer,Algorithm 

### Solution 

```scala
/* ZoomBA. 
Observe the problem can be solved fully declaratively.
Observe we can index the chars from the 
1. first string as : 0,1,2,3,4...
2. second string as : -1,-2,-3,...
Now, join with condition:
all -ve should be sorted descending 
all +ve should be sorted ascending .
then, map back to generate the merged string
*/

def merge_strings( s1, s2 ){
   r1 = [ 0 : #|s1| ]
   r2 = [ -1 : -#|s2| - 1: -1]
   l = r1.list + r2.list
   join_args = list( [0:#|l|] ) -> { l }
   join ( @ARGS = join_args ) :: {
     // must be unique 
     continue( #|set($.o)| != #|$.o| )
     #(pos,neg) = partition( $.o ) :: { $.o >= 0 }
     // sorted ascending? if previous > current then it is not 
     last = reduce( pos ) -> { break( $.p > $.o ){ null } ; $.o }
     continue ( last == null )
     // sorted descending? if previous < current then it is not
     last = reduce( neg ) -> { break( $.p < $.o ){ null } ; $.o }
     continue ( last == null )
     // now map back the string as word 
     true } -> { fold( $.o , '' ) -> { 
      $.p += ( $.o >= 0 ? s1[ $.o ] : s2[ -$.o - 1] ) } 
    }
}

println( merge_strings( 'hey', 'sam' ) )
```

## Q195 
### Problem 
 >Given a string "2-4a0r7-4k", there are two dashes which we can split into 3 groups of length 1, 5, 2. If we want each group to be length 4, then we get "24A0-R74k"Given a String A and an int K, return a correctly formatted string.IF A is "2-4A0r7-4k" and  B is 4, string is "24A0-R74K"IF K is 3, string is "24-A0R-74K" as the first grp could be shorter. 


This question originally appeared [here](https://careercup.com/question?id=5758287824814080).
Tagged with : Google,Algorithm 

### Solution 

```scala
// ZoomBA
def group( s, k ){
  s = s.replace('-','')
  len = #|s|
  rreduce ( s.value ) -> {
    (( $.index != 0 && k /? ( len - $.index) )?'-':'') +
    $.item + $.prev }
}
s = "2-4a0r7-4k"
println ( group ( s, 2 ) )
println ( group ( s, 3 ) )
println ( group ( s, 4 ) )
println ( group ( s, 5 ) )
```

## Q196 
### Problem 
 >Write a code to convert from long type time to date using java?Eg: 324561092314 to 02/20/2015 


This question originally appeared [here](https://careercup.com/question?id=5700803177218048).
Tagged with : Intuit,Senior Software Development Engineer,Algorithm 

### Solution 

```scala
MS_IN_NORMAL_YEAR = 365 * 24* 60 * 60 * 1000 
MS_IN_LEAP_YEAR = 366 * 24 * 60 * 60 * 1000 
DAYS_IN_MONTH = [ 0, 31, 28 , 31, 30, 31, 30, 31, 31, 30, 31 , 30, 31 ]

def is_leap_year( year ){
  return ( ( 400 /? year ) || ( !( 100 /? year ) && ( 4 /? year ) ) )
}
def get_ms_in_month( month, is_leap ){
  if ( is_leap && month == 2 ) return 29 * 24 * 60 * 60 * 1000 
  return DAYS_IN_MONTH[month] * 24 * 60 * 60 * 1000 
}

def date( millisec ){
   year = 1970 // *nix inception year 
   is_leap = is_leap_year( year )
   ms_in_year = ( is_leap ? MS_IN_LEAP_YEAR : MS_IN_NORMAL_YEAR )
   while ( millisec > ms_in_year ){
      millisec -= ms_in_year 
      year +=1
      is_leap = is_leap_year( year ) 
      ms_in_year = ( is_leap ? MS_IN_LEAP_YEAR : MS_IN_NORMAL_YEAR )
   }
   println ( year )
   // at this point, we have found the years, now add months 
   month = 1
   ms_in_month = get_ms_in_month ( month, is_leap )
   while ( millisec > ms_in_month ){
     millisec -= ms_in_month
     month +=1
     ms_in_month = get_ms_in_month ( month, is_leap )
   }
   days = (millisec / ( 24 * 60 * 60 * 1000 ))  + 1 
   [ days, month, year ]
}

res = date ( 1476615214741  )
println( res )
```

## Q197 
### Problem 
 >A list of students and their marks in three subjects are given in the respective order.Student1	20	40	65Student2	35	40	50Student3	10	55	65Given n = 2. Find the name of the students who has got top marks in atleast n subjects.Output for the above examplestudent1student3since they got top marks in atleast 2 subjects 


This question originally appeared [here](https://careercup.com/question?id=5733020146335744).
Tagged with : ThoughtWorks,Applications Developer,Algorithm 

### Solution 

```scala
data = [ [ 'Student1', 20, 40, 65], 
         [ 'Student2', 35, 40, 50], 
         [ 'Student3', 10, 55, 65] ]
// for 3 subjects... can be generalized ... 
max = list([0:3]) -> { -1 } // hoping scores do not become -ve
max = fold ( data , max ) ->{
  row = $.o ; max = $.p 
  for ( i = 1 ; i < 4 ; i += 1 ){
    if ( row[i] > max[i-1] ){
      max[i-1] = row[i]
    }
  }
  max // return 
}
// next, pick and group toppers student -> topped_in_sub_count 
// select those who are topper in more than n subjects ?
n = 2 
toppers = fold( data ) -> {
  row = $.o 
  count = sum ( [1:4] ) -> { row[$.o] == max[$.i] ? 1 : 0 }
  continue ( count < n ) // ignore when count is < n 
  [ row[0] , count ] // return 
}
println( toppers )
```

## Q198 
### Problem 
 >Given arrays for N (>=  2) users, each representing the IDs of hotels visited, find the common IDs of the hotels visited amongst the users.Input:userA = { 2, 3, 1 }userB = { 2, 5, 3 }userC = { 7, 3, 1 }Output:{3}Assumptions:Arrays are unsorted.Cases:1) Each array consists of distinct hotel IDs2) Each array may contain duplicate hotel IDs 


This question originally appeared [here](https://careercup.com/question?id=5664081785651200).
Tagged with : Booking.com,Software Engineer / Developer 

### Solution 

```scala
(zoomba)userA = [ 2, 3, 1 ] 
@[ 2,3,1 ] // ZArray
(zoomba)userB = [ 2, 5, 3 ] 
@[ 2,5,3 ] // ZArray
(zoomba)userC = [ 7, 3, 1 ] 
@[ 7,3,1 ] // ZArray
(zoomba)s = set()
{  } // ZSet
(zoomba)s += userA 
{ 1,2,3 } // ZSet
(zoomba)s &= userB
{ 2,3 } // ZSet
(zoomba)s &= userC  // &= mutable intersection 
{ 3 } // ZSet
(zoomba)
```

## Q199 
### Problem 
 >Assume you have a function isAccountHacked(String username) This function is called by a system whenever a there is a failed login by a particular username.The function returns true if there have been "n" consecutive unsuccessful login attempts in the last 1hr/36,000  seconds.How will you write this method.I was asked this question in an interview and I came up with few solutions of logging last n timestamps in 1 hr.He wanted a solution with space complexity of timestamps less than O(n).Let me know if you need any more details 


This question originally appeared [here](https://careercup.com/question?id=5720992639877120).
Tagged with : SDE-3,Algorithm 

### Solution 

```scala
login_info = { 'tries' : 0 , 'last_successful' : time() }

def is_account_hacked( user_name , max_tries ){
    login_info = LOGIN_INFO[ user_name ]
    #atomic {
    success = login( user_name )
    current = time() 
    if ( success ){
       login_info.last_successful = current
       login_info.tries = 0 
       return false
    }
    login_info.tries += 1 
    return (  current - login_info.last_successful <= 36000000 &&
         login_info.tries >= n )
  }
}
```

## Q200 
### Problem 
 >Given a dictionary, 7 digit phone number and a phone pad where each number could have a list of alphabets attached to it, for eg. 2- {a,b,c}, 3-{d, e,f} and so on, find the list of possible meaningful strings that could be formed with the phone number. 


This question originally appeared [here](https://careercup.com/question?id=5712661934243840).
Tagged with : Software Engineer / Developer 

### Solution 

```scala
// Shows the full declarative power of ZoomBA
tel_dict = { 2 : [ 'a','b','c' ] , 3 : ['d' ,'e' ,'f' ],
             4 : [ 'g' , 'h' ,'i' ], 5 : [ 'j', 'k', 'l' ],
             6 : ['m','n','o'  ], 7 : [ 'p', 'q','r' ,'s' ],
             8 : [ 't' ,'u','v' ], 9 : [  'w' , 'y' ,'z' ] }
// meaningful words?
words = select( file('/usr/share/dict/words') ) :: { #|$.o| == 7  } -> { $.o.toLowerCase }
ph_no = 2742538 // your input : cricket
args = list( str(ph_no).value ) -> { tel_dict[ int($.o) ] }
meaningful = join( @ARGS = args ) :: {
    word = str( $.o ,'' )
    word @ words } -> { str( $.o ,'' ) }
println ( meaningful )
```

## Q201 
### Problem 
 >Write a program to create a sentence at runtime and count number of vowels in it ? 


This question originally appeared [here](https://careercup.com/question?id=5665917238247424).
Tagged with : HTC Global Services,Software Developer,C++ 

### Solution 

```scala
//ZoomBA
s  = random("([a-zA-Z ])+", 40 ) // generates a list of random words total 40 char 
vowels = "AEIOU" 
total_vowels = sum(s.toCharArray ) as {   $.o @ vowels ? 1 : 0  }
```

## Q202 
### Problem 
 >Given an array, move the smaller no to the left and the larger nos to the right. The relative positioning between the small no's and the relative positions between the large nos should not change.The original ( ill formulated ) question can be found here :question?id=5756583549075456.Example :a = [ 6 4 5 0 2 1 11 -1 ]  
   after_a = [  0 , 2, 1, -1,  6, 4, 5, 11 ]Note, for lack of good explanation, please do not laugh at the poster in the solutions. After all, they are trying to help or get help. 


This question originally appeared [here](https://careercup.com/question?id=5112255352930304).
Tagged with : Arrays 

### Solution 

```scala
// ZoomBA
def left_right( a ){
  len = #|a|
  odd = !(2 /? len)
  left_heap = fold( a , heap( len/2 + 1 ) ) -> { $.partial  += $.item  }

  #(l,r) = fold( a, [list(),list()] ) -> {
    if ( $.item < left_heap.max ){ $.partial.0 += $.item }
    if ( $.item > left_heap.max ){ $.partial.1 += $.item }
    if ( !odd && $.item == left_heap.max ) { $.partial.1 += $.item }
    $.partial
  }
  println ( l +  (odd ? left_heap.max : []) + r )
}

a = [ 6, 4, 5, 0, 2, 1, 11 , -1 ]
left_right(a)
a = [ 6, 4, 5, 0, 2, 1, 11 , -1, 9  ]
left_right(a)
```

## Q203 
### Problem 
 >Given a string and dictionary of words, form a word by removing minimum number of characters. Characters can be removed in-order only. 


This question originally appeared [here](https://careercup.com/question?id=5123159217930240).
Tagged with : Google,Software Developer,Algorithm 

### Solution 

```scala
/* 
One way, as Chris said,
for n chars, 2^n for sure,
but then sort these no.s such that 
the comparison is how many 1's the binary string has.
Now, iterate and find the binary string with 1 means deletion of char 
where the new word after deletion is into dictionary 
*/

def find_word( word , dictionary ){
  n = #|word|
  l = list([1: 2 ** n ]) -> { s = str($.o, 2) ; '0' ** ( n - #|s|) + s }
  sorta( l ) :: { 
    ls = sum( $.left.value ) -> { $.o == _'1' ? 1:0 }
    rs = sum( $.right.value ) -> { $.o == _'1' ? 1:0 }
    ls - rs 
    }
   fold( l , null ) :: {  
         s = select ( $.o.value ) :: { $.o == _'0' } -> { word[$.i] }
         w = str(s,'')
         break ( w @ dictionary ){ w }  
   } 
}
dictionary = set ( 'fellow' , 'hello' , 'one' , 'hell' , 'fhel' )
word = 'fhellowne'

println ( find_word( word, dictionary ) )
```

## Q204 
### Problem 
 >Given a 4 X 4 game slot that has random alphabets in all the slotsWrite a function that takes the keyboard and the word as input and returns true if the word can be formedFalse otherwise.A word can be formed on the board by connecting alphabets adjacent to each other (horizontal, vertical and diagonally) Same alphabet should not be reused. 


This question originally appeared [here](https://careercup.com/question?id=5137694256529408).
Tagged with : Facebook,Software Engineer / Developer,Algorithm 

### Solution 

```scala
// ZoomBA : Much better optimised code.
board = [ [ 'h', 'i', 'c', 'f'],
          [ 'g', 's', 'b', 'k'],
          [ 't', 'z', 't', 'v'],
          [ 'n', 'e', 'd', 'u'] ]

map = fold ( board , dict() ) -> {
  row = $.item ; row_index = $.index 
  $.partial |= dict( row ) -> { 
    [ board[row_index][$.index] , [ row_index , $.index ] ] }
}
def is_found( positions ){
    fold ( [1: #|positions| ] , [ true, true, true ] ) :: {
        #(p_r, p_c) = positions[ $.item - 1]
        #(r, c) = positions[ $.item ]
        #(h,v,d) = $.partial
        // horizontal 
        h &= ( (p_r == r ) && ( c - p_c == 1 ) ) 
        // vertical 
        v &= ( ( r - p_r == 1 ) && ( c == p_c ) ) 
        // diagonal  
        d &= ( ( r - p_r == 1 ) && ( c  - p_c == 1 ) )
        [ h, v, d] 
    } 
}

def word_exists( word ){
  if ( empty(word) ) return false
  #(positions ? e )  = list ( word.value ) -> { map[str($.item)] }
  if ( empty(positions) ) return false 
  #(h,v,d) = is_found( positions )
  ( h || v || d ) 
}
println( word_exists ( 'hi') )
println( word_exists ( 'is') )
```

## Q205 
### Problem 
 >Given a string, find the longest substring with k distinct characters.e.g - “aaaabbbb”, k = 2, “aaaabbbb” 	“asdfrttt” k = 3, “asd”, “frttt”[Telephonic Question] 


This question originally appeared [here](https://careercup.com/question?id=5158272756613120).
Tagged with : Google,Software Developer,Java 

### Solution 

```scala
// fully declarative --> un-optimal 
def k_distinct_d( string , k ){
  // create a range 
  r = [ 0 : size(string) ]
  // create combination pair 
  pairs = list ( comb( r , 2 ) )
  // sort them by their size : end_index - start_index
  sortd ( pairs ) :: { ( $.left.1 - $.left.0 ) <  ( $.right.1 - $.right.0 )  }
  // find a pair such that the distinct chars are exactly equal to k 
  v = find ( pairs ) :: { s = string[$.left:$.right] ; size(set(s.value)) == k }
  // yep, we are good
  v.nil?'nothing found':string[ v.value.0 : v.value.1 ]  
}
s = k_distinct_d("asdfrttt", 3)
println(s)
// semi imperative : reasonably optimal 
def k_distinct_i( string , k ){
  // we need to generate pairs with larger size first..so 
  len = size(string)
  r = [0:len]
  MAX = [ '' ] // global is frowned upon, use side effect 
  res = join ( r, r.reverse ) :: { 
    continue( $.0 >= $.1 )
    s = string[ $.0 : $.1 ]
    continue( size( set(s.value) ) != k || size(s) <= size(MAX.0) )   
    MAX.0 = s // set max 
    false // do not collect 
  } 
  empty(MAX.0) ? 'nothing found' : MAX.0  
}
s = k_distinct_i("asdfrttt", 3)
println(s)
```

## Q206 
### Problem 
 >Given an input string E4N4S4W calculate a robot position 


This question originally appeared [here](https://careercup.com/question?id=5723929873219584).
Tagged with :  

### Solution 

```scala
// ZoomBA
def get_pos( string ){
  pos = { 'x' : 0 , 'y' : 0 }
  tokens( string , '([NEWS](\\d*))' ) -> { 
    directive = $.o // the directive 
    direction = directive[0]
    amount = int( directive[1:-1] , 1 )
    printf( '%s %s\n', direction, amount )
    switch( direction ){
      case _'N' : pos.y += amount
      case _'S' : pos.y -= amount 
      case _'E' : pos.x += amount
      case _'W' : pos.x -= amount
    }
  }
  pos // return it 
}
// use it 
s = 'E4N4S4W'
println ( get_pos(s) )
```

## Q207 
### Problem 
 >Find the shortest path between a start node and end node in a undirected +ve weighted graph.You are allowed to add at max one edge between any two nodes which are not directly connected to each other.ex:From | To | Weight1 2 21 4 42 3 13 4 34 5 1 start node = 1, end node = 5.extra edge weight = 2.1----(2)----2
	 |	         |
	 |			 |
	(4)  	    (1)
	 |			 |
	 |			 |
5-(1)-4----(3)----3

In this case answer would be 3 (from 1 - > 5 - > 4) 
Solution:

     1----(2)----2
	/|	         |
   / |			 |
(2)/ (4)  	    (1)
 /   |			 |
/    |			 |
5-(1)-4----(3)----3 


This question originally appeared [here](https://careercup.com/question?id=5721708662095872).
Tagged with : Google,Software Developer,Algorithm 

### Solution 

```scala
find_min (  start, end,  [ original , modified_1, modified_2, ...modified_k ] )  find_min (  start, end,  [ original_graph ] )
```

## Q208 
### Problem 
 >Balance a string with parentheses. "a(b)" -> "a(b)"; "(((((" -> ""; "(()())" -> "(()())"; ")ab(()" -> "ab()"; etc... 


This question originally appeared [here](https://careercup.com/question?id=5712549635948544).
Tagged with : Google,SDE1 

### Solution 

```scala
/* Balance Parentheris */
def is_balanced(string){
    !exists([0: size(string)/2 ] ) where {
        // when s[index] != s[-1-index] 
        string[$.o] != string[-1-$.o] 
    } 
}
def balance_paren(string){
    if ( is_balanced(string) ) { return string }
    // now, here... generates a multi set 
    m = mset(string.value)
    left_count = m[_'('] 
    right_count = m[_')'] 
    #(min,max) = minmax(left_count,right_count)
    // extract the bracket less string 
    s = fold(m.keys,'') as {  
         continue( $.o == _'(' || $.o == _')' )
         $.p += ( str($.o) ** m[$.o] ) 
        }
    // duplicate bracket-ness and... creare a balanced string
    ( '(' ** max ) + s + ( ')' ** max )     
}
println( balance_paren(@ARGS[0]) )
```

## Q209 
### Problem 
 >Find the length of a maximum palindrome subset in an array. For example: in 1, 2, 4, 1 the maximum palindrome subset is 1, 2, 1 and the answer is 3 


This question originally appeared [here](https://careercup.com/question?id=5705610449387520).
Tagged with : Linkedin,Software Engineer,Algorithm 

### Solution 

```scala
// From ZoomBA , with Love 
def len_of_max_subset_palindrome( a ){
  m = mset( a )
  has_odd = [ false ]
  sum( m ) -> { 
    count = $.o.value
    even = ( 2 /? count ) // 2 divides value ?
    if( !even ){ 
      has_odd.0 = true
      count -=1  
    }
    count   
    } + (has_odd.0 ? 1:0 ) // adds 1 for the max odd guy    
}
```

## Q210 
### Problem 
 >Given Map<char,<List<char>> and an input string. Return all possible combinations by replacing each char in input string by one char in mapped set.e.g. 1 -> a,x; 2 -> b,y12 -> ab,ay,xb,xy 


This question originally appeared [here](https://careercup.com/question?id=5087589246697472).
Tagged with : SDE-2 

### Solution 

```scala
(zoomba)m = { 1 : [ 'a' , 'x' ] , 2 : ['b','y'] }
{1=@[ a,x ], 2=@[ b,y ]} // ZMap
(zoomba)join( @ARGS = list( '12'.value ) -> { m[int($.o)]} ) ->{str($.o,'') }
[ ab,ay,xb,xy ] // ZList  /* standard recursion */
map = { 1 : [ 'a' , 'x' ] , 2 : ['b','y'] }

def _recurse(string, options, tmp='') {
   if ( empty(string) ){  options+= tmp ; return }
   head = string[0]
   tail = string[1:-1]
   for ( o :  map[ int(head) ] ){
     _recurse( tail, options, tmp + o )
   }
}

def mapper( string ){
  options = list()
  _recurse(string,options)
  options // return 
}
println( mapper('12') )
```

## Q211 
### Problem 
 >Write a program to get the user name and ageCondition:The name should be more than six characters  and it shouldn’t contain any other characters than alphabet(like !,@,#,$,^,& these are not to be present)The age of the employee must between 18-35 


This question originally appeared [here](https://careercup.com/question?id=6310895161442304).
Tagged with : HTC Global Services,Software Developer,C++ 

### Solution 

```scala
// ZoomBA
def is_valid_login( name, age  ){
   name =~ '[a-zA-Z]+' && size(name) > 6 &&
        18 <= age  && age <= 35 // potential lawsuit glaring at ya. 
}
```

## Q212 
### Problem 
 >PhoneBook search. Given input phone book - John, JohnDavis, Ted, JackMaySearching J should return John, JohnDavis and JackMaySeaching JD should return JohnDavis 


This question originally appeared [here](https://careercup.com/question?id=6273845624307712).
Tagged with : SDE-2 

### Solution 

```scala
/* search words */
_words_ = [ "John", "JohnDavis", "Ted", "JackMay"  ]
// the mumbo jumbo 
def search( word ){
  // we have a bug here, what about $.o is a regex?
  // we need to escape that, but...well... 
  regex = "^" + str(word.value, '') as { str($.o) + ".*" } + "$" 
  println(regex)
  select ( _words_ ) where { $.o =~ regex }
}
// now take for a spin 
println( search( 'J' ) )
println( search( 'JD' ) )  ➜  zoomba99 git:(master) ✗ zmb tmp.zm
^J.*$
[ John,JohnDavis,JackMay ]
^J.*D.*$
[ JohnDavis ]
➜  zoomba99 git:(master) ✗
```

## Q213 
### Problem 
 >You are given an array with duplicates. You have to sort the array with decreasing frequency of elements. If two elements have the same frequency, sort them by their actual value in increasing order.Ex: [2 3 5 3 7 9 5 3 7]Output: [3 3 3 5 5 7 7 2 9] 


This question originally appeared [here](https://careercup.com/question?id=5636146378833920).
Tagged with : Expedia,Software Engineer / Developer,Sorting 

### Solution 

```scala
(zoomba)input=[2,3,5,3,7,9,5,3,7]
@[ 2,3,5,3,7,9,5,3,7 ] // ZArray
(zoomba)ms = mset(input)
{2=1, 3=3, 5=2, 7=2, 9=1} // HashMap
(zoomba)l = list(ms.entries)
[ 2=1,3=3,5=2,7=2,9=1 ] // ZList
(zoomba)sortd(l) :: { $.l.value < $.r.value }
true // Boolean
(zoomba)l
[ 3=3,5=2,7=2,2=1,9=1 ] // ZList
(zoomba)x = fold(l,list()) -> { for(i:[0:$.o.value] ){ $.p += $.o.key } } 
[ 3,3,3,5,5,7,7,2,9 ] // ZList
```

## Q214 
### Problem 
 >Given an array of integers and a sum 'S'. Find 2 integers in the array that add up to S. 


This question originally appeared [here](https://careercup.com/question?id=5675315331334144).
Tagged with : Yahoo,Software Engineer / Developer,Algorithm 

### Solution 

```scala
/* 
With extra space, again exact in 2n
Given array not sorted 
*/
def single_pass( arr , S ){
   s = set( arr ) // create a set out of it 
   // find item x in arr such that S -x is in the set!
   v = find ( arr ) :: { (S - $.item) @  s }
   v.nil ? 'Nothing' : v.value 
}
```

## Q215 
### Problem 
 >java program to interchage continous vowels in a stringex:vowelsbecomes vewolscontinuous becomes cintonouuus 


This question originally appeared [here](https://careercup.com/question?id=5735558386024448).
Tagged with : Akamai,Software Developer 

### Solution 

```scala
def reverse_vowels_declarative(word){
  vowels = set( 'aeiou'.value ) // get into a set 
  vowels_in_word = select( word.value ) where {  $.o @ vowels }
  // put back 
  fold ( word.value , -1 ) as { 
    continue( !($.o @ vowels) )
    word.value[$.i] = vowels_in_word[$.p]
    $.p -= 1 
  }
word 
}

def reverse_vowels_imperative(word){
  vowels = set( 'aeiou'.value ) // get into a set 
  lp = 0 ; rp = size(word) - 1 
  while ( true ){
      while ( lp < rp && !(word[lp] @ vowels) ){ lp += 1 }
      while ( lp < rp && !(word[rp] @ vowels) ){ rp -= 1 }
      break(lp >= rp )
      t = word[rp]
      word.value[rp] = word[lp] 
      word.value[lp] = t   
      lp +=1 ; rp -= 1
  }  
  word // done 
}

println( reverse_vowels_imperative('vowels') )
```

## Q216 
### Problem 
 >You are given a sorted list of distinct integers from 0 to 99, for instance [0, 1, 2, 50, 52, 75]. Your task is to produce a string that describes numbers missing from the list; in this case "3-49,51,53-74,76-99".Examples:[] “0-99”[0] “1-99”[3, 5] “0-2,4,6-99” 


This question originally appeared [here](https://careercup.com/question?id=5753739085348864).
Tagged with : Google,Software Engineer Intern,C++ 

### Solution 

```scala
// ZoomBA
def sanitize( range ){
  #(l,r) = range.split('-')
  return ( l == r ? l : range ) 
}
def split_range( range , n ){
  #(l,r) = range.split('-')
  if ( n == l ){
    return  sanitize(  str( int(l) + 1 ) + '-' + r )
  }   
  if ( n == r ){
    return  sanitize ( l + '-' + str( int(r) - 1 ) )
  }
  [ sanitize(l + '-' + ( n - 1) ) , sanitize( str(n+1) + '-' + r ) ]
}
def do_fancy_stuff( l ){
 fold ( l , list( '0-99' ) ) ->{
   res = split_range ( $.prev[-1] , $.item )
   $.prev.remove( #|$.prev| - 1 )
   $.prev += res // cool ? 
 }
}
println( do_fancy_stuff( [ 0,1,2,50,52,75 ] )  )
```

## Q217 
### Problem 
 >Given a positive integer n, find the no of integers less than equal to n, whose binary representation doesn't contain consecutive 1s. eg:I/P : 4O/P: 4 (0,1,2,4 Valid)public int count(int n){} 


This question originally appeared [here](https://careercup.com/question?id=5708268191088640).
Tagged with : Facebook,SDE1 

### Solution 

```scala
n = 8
t = sum ( [0:n+1] ) -> {
  x = $.o
  m = 3 // 11 
  found = false
  // 11, 110, 1100, ... 
  while ( m < x ){
    break( (m & x) != 0 ){ found = true }
    m *= 2
  }
  (found ? 0 : 1)
}
println(t)
```

## Q218 
### Problem 
 >How to calculate sum of all numbers in a string. Example 11aa22bb33dd44 =110Note: Should not use Regex and replace 


This question originally appeared [here](https://careercup.com/question?id=5748922686373888).
Tagged with : Amazon,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
s = '11aa22bb33dd44' 
r = lfold( s.toCharArray , ['', 0 ] ) ->{
  if ( $.o @ '0123456789' ){
    $.p.0 += $.o 
  }else{
    $.p.1 += int( $.p.0 , 0 )
    $.p.0 = ''
  } 
  $.p  
}
r.1 += int( r.0 , 0 )
println(r.1)
```

## Q219 
### Problem 
 >Find if a given number can be expressed in the form of p^q, where p and q are integers 


This question originally appeared [here](https://careercup.com/question?id=5454925876166656).
Tagged with : Amazon,SDE1,Algorithm,Coding 

### Solution 

```scala
// ZoomBA
def gcd( a,b ){ // copied from wikipedia 
  while ( b != 0 ){
    t = b ; b = a % b ; a = t  
  } 
  return a 
}

def is_expressible( n ){
  multiplicities =  select ( lfold ( [2:n+1] , dict() ) -> { 
    continue ( exists ( $.p.keySet ) :: {  $.o /? $.$.o } )
    $.p[$.o] = 0  
    for ( x = n ; $.o /? x ; x/= $.o ){ $.p[$.o] += 1 } 
    $.p 
  } ) :: { $.o.value > 0 } -> { $.o.value }
  // calculate gcd of all the multiplicities
  final_gcd = reduce ( multiplicities ) -> { gcd( $.p , $.o ) }
  final_gcd > 1 // for a non trivial solution 
}
```

## Q220 
### Problem 
 >Create an iterator class that stores a list of the built-in Iterators. Implement the next() and hasNext() methods in a Round Robin pattern (pops next element in a circle).Example:Given a list [iterator1,iterator2, iterator3...]when calling RoundIterator.next()pops iterator1.next if iterator1.hasNext() is true when calling RoundIterator.next()pops iterator2.next()  if iterator2.hasNext() is truewhen calling RoundIterator.next()pops iterator3.next  if iterator3.hasNext() is true...when calling RoundIterator.next()pops iterator1.next  if iterator1.hasNext() is truewhen calling RoundIterator.next()pops iterator2.next  if iterator2.hasNext() is truewhen calling RoundIterator.next()pops iterator3.next  if iterator3.hasNext() is true...until there is no more element in any of the iterators 


This question originally appeared [here](https://careercup.com/question?id=5644846673952768).
Tagged with : Google,Algorithm 

### Solution 

```scala
def Sample : { 
   $$ : def(){
      $.its = list([0:random(4) + 1 ]) ->{
        l = list([0:random(5)]) ->{ random(100) } 
        println(l)
        l.iterator()
      }
      $.index = -1 
   },
   hasNext : def(){ 
     while ( !empty($.its) ) {
       $.index =  ($.index + 1) % size( $.its )
       it = $.its[$.index]
       if ( it.hasNext ) { return true }
       $.its.remove( $.index )
       $.index -= 1 
     }
     false 
   },
   next : def(){ return $.its[ $.index].next() }
}

s = new ( Sample )
while ( s.hasNext() ){
  printf(' %s ', s.next())
}
println()
```

## Q221 
### Problem 
 >Second Least common element from an Integer array. Example: [5,5,4,5,4,6,6,6,1,3,3,4,4,5,4] Answer: 3 Reason: {1=1, 3=2, 4=5, 5=4, 6=3} 


This question originally appeared [here](https://careercup.com/question?id=5645782997794816).
Tagged with : Bank of America,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
a = [5,5,4,5,4,6,6,6,1,3,3,4,4,5,4]
m = mset(a)
h = heap ( 2 ) :: {  $.o.0.value < $.o.1.value  }
h += m.entrySet
println ( h[0].key )
```

## Q222 
### Problem 
 >Given a binary tree & the following TreeNode definition, return all root-to-leaf paths.Definition of TreeNode:public class TreeNode {
      public int val;
      public TreeNode left, right;
      public TreeNode(int val) {
          this.val = val;
          this.left = this.right = null;
      }
  }EXAMPLEGiven the following binary tree:
   1
 /   \
2     3
 \
  5

All root-to-leaf paths are:
[ "1->2->5", "1->3" ]From Lint Code - http://www.lintcode.com/en/problem/binary-tree-paths/ 


This question originally appeared [here](https://careercup.com/question?id=5641503067078656).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
// Tree Node schema 
def t_node : { v : 0 , l : left_node , r : right_node }
paths = list()
def find_paths( node , path ){
   if ( empty(node) ){
     paths.add ( path.split('/') )
     return 
   }
   if ( !empty( node.left ) ){
     find_paths ( node.left , path + '/' + node.left.v )
   }
   if ( !empty( node.right ) ){
     find_paths ( node.right , path + '/' + node.right.v )
   }
}
// call this way 
find_paths ( root, '/' + root.v )
```

## Q223 
### Problem 
 >Identifying if all the elements of a set (in enterity) is present in a list of sets.For example checking for set1 = {1,2} in {1,2,3}, {5,6} should return true as {1,2} is present in {1,2,3}. Similiary it will be true for {1,2,8,9}, {1,2,4}But checking for {1,2} in {1,5,6}, {2,3,1} should return false as {1,5,6} does not contain all elements of {1,2} 2 is missing 


This question originally appeared [here](https://careercup.com/question?id=5702357208793088).
Tagged with : Amazon,SDE-2,Algorithm 

### Solution 

```scala
// if it was [1] <= is subset 
exists(list_of_sets) where { my_set <= $.o }
// if it was [2] subset is not invertible, so...
!exists(list_of_sets) where { !(my_set <= $.o) }
// if it was [3] | is union, OR 
my_set <= reduce( list_of_sets ) as { $.p | $.o }
```

## Q224 
### Problem 
 >generate and print first N binary palindromes .ex first 4 are as follow111101111 


This question originally appeared [here](https://careercup.com/question?id=5692699114995712).
Tagged with : AMD,Associate,Algorithm 

### Solution 

```scala
// ZoomBA
def bin_palindrom( n ){
   tot = 0 
   for ( [1: 2 ** (n - 1) : 2] ){
     bin_s = str($,2)
     continue ( bin_s ** -1 != bin_s )
     println( bin_s )
     tot += 1
     break  ( tot == n ) 
   }
}
bin_palindrom ( 10 )
```

## Q225 
### Problem 
 >With times stored in the format "HH:MM:SS", find the number of "interesting" times that can be displayed between two given times S and T (inclusive).An "interesting" time is one that can be displayed with at most 2 characters.For example S "15:15:00" and T "15:15:12" would return the count of 1 as the only "interesting" time between S and T (inclusive) is "15:15:11".Another example, S "22:22:21" and T "22:22:23" would return the count of 3 because of the following times: "22:22:21", "22:22:22", "22:22:23"Constraints:  S <= T  00 <= HH <= 23  00 <= MM <= 59  00 <= SS <= 59 


This question originally appeared [here](https://careercup.com/question?id=6199206977994752).
Tagged with : Senior Software Development Engineer 

### Solution 

```scala
/*
Essentially - generate a date-time range,
with spacing of 1 sec, each,
and then check if the string rep has 2 or less unique chars.
That is how one does a fully declarative coding.
Observe : en.wikipedia.org/wiki/ISO_8601
*/
def compute_interesting_times( s1, s2 ){
  input_time_format = 'HH:mm:ss'
  time_range = [ time(s1,input_time_format) : time(s2,input_time_format) + 'PT1S' : 'PT1S' ]
  output_time_format = 'HHmmss'
  x = select( time_range ) where {
     s = str($.o, output_time_format)
     size(set(s.value)) <= 2
  }
  size(x)
}
println( compute_interesting_times("15:15:00", "15:15:12") )
println( compute_interesting_times("22:22:21", "22:22:23") )
```

## Q226 
### Problem 
 >If we have telephone directory with 1,00,000 entries,which sorting algorithm is best? 


This question originally appeared [here](https://careercup.com/question?id=5670600447098880).
Tagged with : Algorithm 

### Solution 

```scala
d * n  ,  n * log ( n )
```

## Q227 
### Problem 
 >Given m 0 and n 1,  count the total number of permutations  where two 1 cannot be adjacentpublic int count(int m, int n){} 


This question originally appeared [here](https://careercup.com/question?id=4836228449959936).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/*
Imagine the problem as finding all integers with
n 1s and m 0 s such that in the binary representation
There is no '*11*'. And we are good. So....
*/

def do_garbled(m,n){
  range = [0 : 2 ** ( m + n )]
  count = 0
  for ( x : range ){
    s = str(x,2)
    s = '0' ** (size(s) - m - n) + s
    continue ( n != sum(s.value) -> { _'1' == $.o ? 1 : 0 } )
    continue ( s =~ '.*11.*')
    count += 1
  }
}

// now in full ZoomBA mode, we can write this:
m = 2
n = 2
res = from ( [0 : 2 ** ( m + n )] , set() ) as {
   s = str($.o,2)
   s = '0' ** (size(s) - m - n) + s
} where {
  n == sum($.o.value ) as { _'1' == $.o ? 1 : 0 }  } where {
  $.o !~ '.*11.*'
}
println ( res )
println ( size(res) )
```

## Q228 
### Problem 
 >I have Start time and End Time in a log file in lines next to each other. Can anyone suggest me Unix command to get output of Start time and Endtime in side by side columns 


This question originally appeared [here](https://careercup.com/question?id=5726088203337728).
Tagged with : unix system programmin 

### Solution 

```scala
head -n 42  | tail -n 2  |   tr -s ' '  '\n'
```

## Q229 
### Problem 
 >Given a string, determine if a permutation of a string can form a palindrome. 


This question originally appeared [here](https://careercup.com/question?id=5641948569272320).
Tagged with : Uber,Software Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
#|select ( mset(s.toCharArray) ) :: {  $.o.value % 2 != 0 }| <= 1
```

## Q230 
### Problem 
 >Write func repeat(e, n).Args:	e: any object	n: a number of timesReturns:	an iterator producing the element e n times 


This question originally appeared [here](https://careercup.com/question?id=5726732524978176).
Tagged with : Google,Software Engineer Intern,Data Structures 

### Solution 

```scala
/*
Showcasing ZoomBA iterator proto-object types
Try beating this one on elegance and aesthetics  
*/
def Iter : { obj : null , times : 0 , 
  $next : def(){
    break ( $.times <= 0 )
    $.times -= 1 
    $.obj
  }  
}
my_iter = new ( Iter , obj = 'ZoomBA' , times = 5 )
for ( my_iter ){
  println( $ )
}
```

## Q231 
### Problem 
 >Print elements of a matrix in spiral form. 


This question originally appeared [here](https://careercup.com/question?id=6227199318294528).
Tagged with : Microsoft,SDE1,Matrix 

### Solution 

```scala
def spiral( m ){
  bound = size(m) ; times = bound
  row = 0 ; col = 0
  while ( bound > 0 ){
    while ( times != 0 ){ printf(' %d ', m[row][col]) ; col += 1 ;  times -= 1 }
    bound -= 1 ; times = bound  ; col -= 1 ; row += 1  
    while ( times != 0 ){ printf(' %d ', m[row][col]) ; row += 1 ; times -= 1 }
    times = bound ; col -= 1 ; row -= 1 
    while ( times != 0 ) { printf(' %d ', m[row][col]) ; col-= 1 ;  times -= 1 }
    bound -= 1 ;  times = bound ; row = times ; col += 1  
    while ( times != 0 ) { printf(' %d ', m[row][col]) ; row-= 1 ;  times -= 1 }
    col += 1 ; row += 1 ; times = bound 
  } 
  println()
}
```

## Q232 
### Problem 
 >You are given a string S consisting of N brackets, opening "("and/or closing ")". The goal is to split S into two parts (left andright), such that the number of opening brackets in the left part isequal to the number of closing brackets in the right part.For example, given S = "(())", the function should return 2,because:the first two characters of S, "((", contain twoopening brackets, andthe remaining two characters of S, "))", containtwo closing brackets.In other example, given S = "(())))(", the function should return4, because:the first four characters of S, "(())", contain twoopening brackets, andthe remaining three characters of S, "))(", containtwo closing brackets.In other example, given S = "))", the function should return 2,because:the first two characters of S, "))", contain zeroopening brackets, andthere are no remaining characters, so they containalso zero closing brackets. 


This question originally appeared [here](https://careercup.com/question?id=5633243081605120).
Tagged with : Arrays 

### Solution 

```scala
// ZoomBA
/* 
Should be solvable by 2 iterations and exactly n space.
You can optimize it later.
Pass 1: At every index position left_bracket_count
-1 : 0 .... 
Pass 2: At every index position right_bracket_count
from right, so size : 0 check where left and right count matches.
You are cool. 
*/
def find_match_count( string ){
  #(mem,count) = fold ( string.value, [list(), 0] ) -> {  
    $.p.1 += ($.o == _'(' ? 1 : 0) 
    $.p.0 += $.p.1 ; $.p 
  }
  // this is DUMB.. but well..
  if ( mem[-1] == 0 ) return #'Index #{size(string)} Count 0'  
  right_count = 0 
  i = rindex ( string.value ) :: {
    right_count += ($.o == _')' ? 1 : 0)
    mem[$.i] == right_count
  }
  i<0 ? 'Nothing Found' : #'Index #{i} Count : #{mem[i]}'
}
println( find_match_count( "(())" ) )
println( find_match_count( "(())))(" ) )
println( find_match_count( "))" ) )
```

## Q233 
### Problem 
 >Generate a random number with UNIFORM DISTRIBUTION between [0,n) where n is given and excluded list is given. The randomly generated number should belong to the range [0, n) but should be excluded from the given excluded list. For example, n = 10 and excluded list ={2,3,0} then the random number should be from {1,4,5,6,7,8,9} such that any number from the list {1,4,5,6,7,8,9} has UNIFORM probablility of occuring 


This question originally appeared [here](https://careercup.com/question?id=5754655823888384).
Tagged with : Google,Software Developer 

### Solution 

```scala
if __name__ == '__main__':
    func = gen_random(4,[2,1])
    counter = dict()
    for i in range(0,1000):
        x = func.next()
        if x not in counter:
            counter[x] = 0
        counter[x] += 1
    print (counter)
```

## Q234 
### Problem 
 >Face to faceQ3) stream of numbers coming, get 'n' min elements at any point of time 


This question originally appeared [here](https://careercup.com/question?id=5673066304634880).
Tagged with : Amazon,SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA 
def infinite_stream(){
   random(1000)
}
n = 5 
h = heap(n)
// infinite stream iterator 
h = lfold ( infinite_stream , h ) -> { 
  println ($.o) ;  break( $.o > 980 ) ; $.p += $.o }
println(h)
```

## Q235 
### Problem 
 >Given an n-ary tree, find the longest sequence in it. The sequence doesn't end to start at the root. It can go from leaf to leaf. 


This question originally appeared [here](https://careercup.com/question?id=5140810577215488).
Tagged with : Google,Software Engineer Intern,Trees and Graphs 

### Solution 

```scala
// to store all the paths x/y/z form
ALL_PATHS = list()
// how to enumerate all root to leaf
def enumerate_paths( node , path ){
    if ( empty(node.children) ) {
       // add the path, because node is leaf
       ALL_PATHS += path
       return // nothing more is to be done
    }
    for ( c : node.children ){
       // recurse into it
       enumerate_paths ( c , path +'/' + c.id )
    }
}
// enumerate all the paths
enumerate_paths ( root, '' )
/* all these paths are from root to leaf,
so find a pair such that :
1. The overlap is only at root
2. And is max length */
len = #|ALL_PATHS|
max = [ '' , '' , 0 ]
for ( i : [0:len] ){
   for ( j : [i + 1 : len] ){
       p1 = ALL_PATHS[i] ; p2 = ALL_PATHS[j]
       a = p1.split('/') ; b = p2.split('/')
       total_len = size(a) + size(b)
       // a.0 == b.0 means overlap, e.g. /x/y , /x/z
       if ( a[0] != b[0] && total_len > max.2 ){
          max.0 = p1 ; max.1 = p2 ; max.2 = total_len
       }
   }
}
// here we have everything
println ( max )
// and note that we are still less than 42 lines
```

## Q236 
### Problem 
 >Given two strings, return true if they are one edit away from each other, else return false. An edit is insert/replace/delete a character. Ex. {"abc","ab"}->true, {"abc","adc"}->true, {"abc","cab"}->false 


This question originally appeared [here](https://careercup.com/question?id=5926214520799232).
Tagged with : Amazon,SDE1 

### Solution 

```scala
/*  ZoomBA
1. When both strings have same length 
  > Check if they differ in only one char
2. When both strings differ in length 
  > Check if they differ only by 1 size 
  > larger can be created by a split of the smaller:
  > larger = smaller_1 new_char smaller_2     
*/
def same_length(string1, string2){
  counter = 0
  !exists ( [0: #|string1| ] ) :: {
    counter += ( ( string1[$.o] != string2[$.o] ) ? 1 : 0 )
    counter > 1 // updates local, seeded by global 
  }
}
def diff_length( smaller , larger ){
  first_diff = index ( [0:#|smaller|] ) :: { 
    smaller[$.o] != larger[$.o] }
  // from first diff, everything else should match 
  !exists ( [first_diff : #|smaller| ] ) :: {
    smaller[$.o] != larger[$.o + 1 ] }
}  
def one_distant_apart( s1, s2 ){
  l1 = #|s1| ; l2 = #|s2|
  if ( #|l1 -l2 | > 1 ) return false 
  if ( l1 == l2 ) { return same_length( s1, s2 ) } 
  if ( l1 < l2 ){ return diff_length ( s1, s2 ) }  
  return diff_length ( s2, s1 ) 
}
```

## Q237 
### Problem 
 >How will you serialize the binary tree ? 


This question originally appeared [here](https://careercup.com/question?id=5192167330938880).
Tagged with : Amazon,SDE-3,Algorithm 

### Solution 

```scala
/* 
Binary Tree :
      a
     / \ 
    /  right_tree
  /         
left_tree  
*/

tree_pre = "(a (left_tree)(right_tree))"
tree_in =  "((left_tree) a (right_tree))"
tree_post =  "((left_tree) (right_tree) a)"
/* use () to define non existing node */
```

## Q238 
### Problem 
 >Find the largest repeating sub-string in a string.ex: banana ans is: ana 


This question originally appeared [here](https://careercup.com/question?id=5681177959596032).
Tagged with : Amazon,SDE1 

### Solution 

```scala
def longest_repeated_substring( string ){
  n = size( string )
  if ( n <=1 ) return ''
  counter = dict()
  join ( [0:n], [0:n] ) where {
    continue(  $.1 - $.0 <= 1 ) // when ( j - i > 1 ) for pairs 
    sub_string = string[$.0:$.1]
    if ( sub_string @ counter ){
      counter[sub_string] += 1
    } else {
      counter[sub_string] = 1
    }
    false // we do not want to add, at all 
  }
  // find min, max 
  #(min,max) = minmax ( counter ) where { $.l.value < $.r.value }
  max.key 
}
println( longest_repeated_substring('banana') )
```

## Q239 
### Problem 
 >You are a string conscious guy. You categorise strings into 3 types: good, bad, mixed. If a string has 3 consecutive vowels or 5 consecutive consonants or both, it is bad. Else it is good. If a string has ‘?’, that can be replaced with any character. Thus, string ‘?aa’ can be bad if ‘?’ is vowel or good if consonant. Thus, it is mixed. Implement function which takes string s as input and returns good, bad or mixed 


This question originally appeared [here](https://careercup.com/question?id=5165588511981568).
Tagged with : Adobe,SDE1,Algorithm 

### Solution 

```scala
// ZoomBA
def classify( string ){
   // please do some menial work and list all consonants, to ensure that 
   // we do not false miss stuff not in [a-z]
   // =~ is the matches operator 
   if ( string =~ '.*[^aeiou]{5,}.*|.*[aeiou]{3,}.*' ) return 'bad'
   if ( string =~ '.*[\?aeiou]{3,}.*|.*(\?|[^aeiou]){5,}.*' ) return 'mixed'
   return 'good'
}
```

## Q240 
### Problem 
 >Reverse the words in string eg. 'The Sky is Blue'. then print 'Blue is Sky The'. 


This question originally appeared [here](https://careercup.com/question?id=5645125836341248).
Tagged with : Expedia,Software Developer,String Manipulation 

### Solution 

```scala
/* 
There are three variations of the problem, 
when I used to ask this.
1. A string is actually a sequence of chars.
   Those makes whitespaces special.
   1.1 'I am   cool' --> 'cool   am I'
   1.2 'cool am   I' 
2. 2.1 'cool am I'
People generally tend to solve the 2.1.
While, 1.1, and 1.2 are where the fun really is.
Now, I present 1 liners, fully declarative form.
*/
// 2.1. The easiest one
str( (tokens( string , '\\S+') ** -1 ), ' ')
/* 
Blue is Sky The // String
*/
// 1.1 :: 'I am   cool'
spaces = tokens( string , '\\s+') 
words = tokens( string , '\\S+')
l = fold ( [size(spaces)-1:-1 ] , list(words[-1]) ) as { $.p += (spaces[$.o] + words[$.o]) } 
str(l,'')
// 1.2 trivial. left as exercise .
```

## Q241 
### Problem 
 >Given a string with only parenthesis. Check if the string is balanced.ex - 1) "<({()})[]> is balanced2) "<({([)})[]> is not balanced 


This question originally appeared [here](https://careercup.com/question?id=5659999612174336).
Tagged with : Amazon,SDE-2,Algorithm 

### Solution 

```scala
// ZoomBA
paren_arr = [ { _'<' : 1 ,  _'>' : -1 , 'c' : 0 },
              { _'(' : 1 ,  _')' : -1 , 'c' : 0 },
              { _'{' : 1 ,  _'}' : -1 , 'c' : 0 },
              { _'[' : 1 ,  _']' : -1 , 'c' : 0 } ]
// now the indexing 
parens = { _'<' : 0 , _'>' : 0 , 
           _'(' : 1 , _')' : 1 ,
           _'{' : 2 , _'}' : 2 ,
           _'[' : 3 , _']' : 3 }

def match_paren(string){
   fold ( string.value , true ) -> {
     matcher = paren_arr[ parens[ $.item ] ]
     matcher.c += matcher[ $.item ]
     break ( matcher.c < 0 ){ false }
     true
   } && ! ( exists ( paren_arr ) :: {  $.item.c != 0 } )
}
println ( match_paren (  "<({()})[]>"  ) ) 
println ( match_paren ( "<({([)})[]>" ) )
```

## Q242 
### Problem 
 >Make 100 HTTP GET requests to http://en.wikipedia.org/wiki/Main_Page and print the following in Javastatistics for the response time to stdout:• 10th, 50th, 90th, 95th, 99th Percentile• Mean• Standard DeviationYour solution must be parallel. You must make at least N (say 10, but should be configurable)requests at a time.Explain design choices, known limitations and edge cases.What challenges did you face? How would you improve the code if you had more time? 


This question originally appeared [here](https://careercup.com/question?id=5710657300201472).
Tagged with : Google,Senior Software Development Engineer 

### Solution 

```scala
package com.zeroturnaround.callspy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 */
public class CareerCup {

    static class IThread implements Runnable{

        static double timing(String url) {
            try {
                URL myUrl = new URL(url);
                long l = System.currentTimeMillis();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(myUrl.openStream()));
                while ((in.readLine()) != null);
                in.close();
                double spentMS = System.currentTimeMillis()  - l;
                return spentMS/1000;
            }catch (Throwable t){
                return Double.MAX_VALUE ;
            }
        }

        static List<IThread> makePool(int times, String url){
            List<IThread> pool = new ArrayList<>(times);
            for ( int i = 0 ; i < times; i++ ){
                IThread it = new IThread(url);
                Thread t = new Thread( it);
                pool.add(it);
                t.start();
            }
            return pool;
        }

        double timing;

        final String url;

        Thread myThread;

        IThread(String url){
            this.url = url;
            timing = Double.MAX_VALUE ;

        }

        @Override
        public void run() {
            this.myThread = Thread.currentThread();
            timing = timing(url);
        }
    }

    public static void analyze(int times, String url){
        List<IThread> pool = IThread.makePool(times,url);
        // wait for all to complete execution :: poll()
        while ( true ){
            boolean oneAlive = false ;
            for ( IThread t : pool ){
                oneAlive |= t.myThread.isAlive();
            }
            if ( !oneAlive ){ break; }
        }
        List<Double> data = new ArrayList<>();
        for ( IThread t : pool ){
            data.add(t.timing);
        }
        // now we have the data, do mean, sd, what not...
        Collections.sort(data);
        int _90 = (int)Math.floor(times * 0.9);
        System.out.printf("90p is : %f", data.get(_90 ) );
    }

    public static void main(String[] args){
        analyze(20, "give your url here" );
    }
}
```
```scala
/* Make times calls to print stats on a static url 
Making it heavily documented so that ChrisK
can read ZoomBA. It is trivial, once you master 
what -> , :: , $ are.
-> is "as" that is a mapper function 
:: is "where" that is a predicate, a condition 
$ is the iteration variable, holding 
$.o -> object of iteration , the item 
$.i -> the index of the current iteration 
$.c -> the context, the iterable on which iteration is happening 
$.p -> partial, the result of the iteration, as of now 
*/
def analyze ( url, times=10) {
  percentiles = [ 0.99, 0.95, 0.9 , 0.5, 0.1 ] 
  // an inner function 
  def timing( url ){
    // gets the pair, timing in sec, output of the call 
    // using clock statetement 
    // which has the read function to readf from the url 
    #(t,o) = #clock { read ( url ) }
    t // return value are implicit like scala, no point saying return 
    // side effect, there is really no truly void function in ZoomBA 
  }
  def parallelize( url , times ){
    // create a list of *times* threads 
    // each thread has a body of function call timing()
    // zoomba threads have a field :value, which stores the thread functions return value 
    // this is massive improvement from Java, see similar code and the pain below 
    threads = list([0:times] ) -> { thread() -> { timing( url ) } }
    // polls num tries, poll-interval, until condition is true
    // returns true if condition was true, else returns false  
    // :: (such that) is a short form of where clase in zoomba
    // the code waits till no alive thread 
    // shorthand of java function isXxx() -> xxx in  zoomba
    // making it way more succint 
    // could easily be done using a .join() but, why care?
    poll(300,50) :: { !exists( threads ) :: { $.o.alive } }
    // extracting the return value of the threads into another list 
    // -> is the 'as' symbol 
    // it reads create a list from ( threads ) as ( mapping ) thread.value as the item 
    // $ is the iteration construct, more like *this* for a loop.
    // $.o stores the iteration object for the loop, this this case, a thread object 
    list( threads ) -> { $.o.value }
  }
  def stats( data ){
    // sum 
    mean = sum(data) / size(data)
    // sum over item - mean whole squared, right?
    variance = sum( data ) -> { ($.o - mean) ** 2 }
    sd = variance ** 0.5 
    printf( 'mean: %s\n', mean )
    printf( 'sd: %s\n', sd )
    // now percentile calculations
    // sorta --> sort ascending the data values 
    sorta(data) 
    // another iteration - for() would be same 
    fold ( percentiles ) ->{
      printf( '%.2fp: %s\n', $.o , data[floor( size(data) *  $.o )] )
    }
  }
  println('url -> ' + url )
  println('num of times -> ' + times )
  println('All in secconds.')  
  data = parallelize( url, times )
  stats(data)
}
```

## Q243 
### Problem 
 >Function to find if the characters of the sample string is in the same order in the text string. They need not beconsecutive.Eg.. TextString: Redmond, WashingtonSample string :Rdd Waitn 


This question originally appeared [here](https://careercup.com/question?id=5708280396513280).
Tagged with : Student,freshers,Programming Skills 

### Solution 

```scala
( anything not s[0] ) * -> s[0] -> ( anything not s[1] ) * -> s[1] .... -> s[-1] -> end  def recognize( pattern_string , text_string ){
  cur_pattern_index = 0 
  for ( char : text_string ){
     continue ( char != pattern_string[ cur_pattern_index ] )
     cur_pattern_index += 1 // matched, so move to next pattern letter 
  }
  return cur_pattern_index == size( pattern_string ) 
}
println ( recognize('Rdd Waitn' , 'Redmond, Washington' ) )
println ( recognize('Rdd Waixtn' , 'Redmond, Washington' ) )
```

## Q244 
### Problem 
 >/From the input array, output a subset array with numbers part of a Fibonacci series.//  input: [4,2,8,5,20,1,40,13,23]//  output: [2,5,1,8,13]public static List<Integer> getFibNumbers(int[] nums){} 


This question originally appeared [here](https://careercup.com/question?id=5154130839470080).
Tagged with : Facebook,SDE1 

### Solution 

```scala
/* 
Showing Idiomatic ZoomBA 
*/
input = [4,2,8,5,20,1,40,13,23] 
#(m,M) = minmax(input)
fib = seq ( 0, 1 ) -> { $.item[-1] + $.item[-2 ] } 
while ( fib.next <= M );
result = input & fib.history
println(result)  ➜  wiki git:(master) ✗ zmb tmp.zm
[ 1,2,5,8,13 ]
➜  wiki git:(master) ✗
```

## Q245 
### Problem 
 >A producer continuously produces a stream of characters. There are multiple consumer threads which read the chars and match to one of known strings and increment the counter for that pattern. Write the consumer function. Show how you will synchronize and maintain parallelism.Ex: Producer: abcdegabccaabbaacv ......Known strings[] = {"ab", "aa", "fff" ... }patternMatchCount[] = {3, 2, 0 ... } 


This question originally appeared [here](https://careercup.com/question?id=5166346791813120).
Tagged with : NVIDIA,Software Engineer,Algorithm 

### Solution 

```scala
strings = { "ab" : 0 , "aa" : 1 , "fff" : 2 , ... }
counters = [ 3, 2, 0, ...]

def increment_counter( s ){
   idx = strings[ s ]
   #atomic{
      // make atomic operation : increment 
      counters[idx] += 1
   }
}
```

## Q246 
### Problem 
 >Given an NxN grid with an array of lamp coordinates. Each lamp provides illumination to every square on their x axis, every square on their y axis, and every square that lies in their diagonal (think of a Queen in chess). Given an array of query coordinates, determine whether that point is illuminated or not. The catch is when checking a query all lamps adjacent to, or on,… 


This question originally appeared [here](https://careercup.com/question?id=5759602621677568).
Tagged with : Google,SDE1 

### Solution 

```scala
// ZoomBA
// preprocessing :: generate the n X n grid 
n = 8 
// create grid 
grid = list ( [0:n] ) -> { list( [0:n] ) -> { 0 } }

// basic function to put the lamp 
def put_lamp( x, y ){
   fold ( [0:n] ) -> { grid[ x ][ $.item ] = 1 }
   fold ( [0:n] ) -> { grid[ $.item ][ y ] = 1 }
   // left top -> bottom right diagonal 
   i = x ; j = y 
   while ( i >= 0 && j >= 0 ) { grid[i][j] = 1 ; i -=1 ; j-=1 ; }
   i = x ; j = y 
   while ( i < n && j < n ) { grid[i][j] = 1 ; i +=1 ; j+=1 ; }
   // top right -> bottom left 
   i = x ; j = y 
   while ( i >= 0 && j < n ) { grid[i][j] = 1 ; i -=1 ; j+=1 ; }
   i = x ; j = y 
   while ( i < n && j >= 0 ) { grid[i][j] = 1 ; i +=1 ; j-=1 ; } 
}

put_lamp ( 5, 1 )
// print the grid 
fold ( grid ) -> { printf('%s\n' , str( $.item , ' ' ) ) }
```

## Q247 
### Problem 
 >Write a code to find index of integer item in an integer array using java? Note : Complexity should be less than O(n) or with out using any for loop. 


This question originally appeared [here](https://careercup.com/question?id=5696357181423616).
Tagged with : Intuit,Senior Software Development Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
// preprocessing 
m = dict ( arr ) -> { [ $.item, $.index ] } 
// subsequently 
idx = item @ m ? m[item] : -1
```

## Q248 
### Problem 
 >Write a program to print the list of the maximum repeating number from an array.Example: Input:: a[]={1, 2, 1, 0, 5, 2, 4, 2, 3, 0, 1, 3, 2, 4}output: 2, 1, 0, 4, 3, 5 


This question originally appeared [here](https://careercup.com/question?id=4808737102495744).
Tagged with : Skill Subsist Impulse Ltd,Data Scientist,C 

### Solution 

```scala
a = [1, 2, 1, 0, 5, 2, 4, 2, 3, 0, 1, 3, 2, 4]
ms = mset(a)
l = list( ms.entries )
sortd( l ) :: { $.left.value < $.right.value } 
println( str( l , ',') -> { $.key } )
```

## Q249 
### Problem 
 >Program to print string value if each vowel of associated with value 1 and each consonant associated with value 2 print the sum of string valueEx if input:aPrint O/p:1I/p:abO/p:1+2=3I/p:abcdO/l:1+2+2+2=7I/p:abcdeO/p1+2+2+2+1=8 


This question originally appeared [here](https://careercup.com/question?id=5182622754930688).
Tagged with : Amazon,Software Developer,C++ 

### Solution 

```scala
// ZoomBA
string = 'abcde'
vowels = set( 'aeiou'.value ) // set for faster access 
x = sum ( string.value ) -> { $.item @ vowels ? 1 : 2 } // pretty iconic 
println(x)
```

## Q250 
### Problem 
 >Q: Find the absolute paths to all directories with image files, given a file system that looks like this. The subdirectory is one indent over./usr
   /local
       profile.jpg
       /bin
           config.txt
           dest.png
       /rbin
    image.gif
/sys
   /re
   /tmp
       pic.jpg
       .....
…… 


This question originally appeared [here](https://careercup.com/question?id=5655351266377728).
Tagged with : Google,Software Engineer,Algorithm 

### Solution 

```scala
def find_all_pic_dir(dir='.', 
          pic_extensions=set('.jpg', '.png' , '.gif') ){
  // nest it 
  def recurse(cur_dir){
    d = file ( cur_dir )
    if ( !d.file.directory ) return 
    pic_dir = exists ( d ) :: { file($.o).extension.toLowerCase @ pic_extensions }
    if ( pic_dir ){ dir_paths += cur_dir }
    for ( child : d ){
      recurse( child.canonicalPath )
    }
  }
  // set up and call 
  dir_paths = set()
  recurse( file(dir).file.canonicalPath )
  // return 
  return dir_paths
}

dirs = find_all_pic_dir ( "/" )
println( dirs )
```

## Q251 
### Problem 
 >Print series 010203040506. Using multi-threading 1st thread will print only 0 2nd thread will print only even numbers and 3rd thread print only odd numbers. 


This question originally appeared [here](https://careercup.com/question?id=5082791237648384).
Tagged with : Goldman Sachs,SDE-2,Threads 

### Solution 

```scala
// Globals are terribly bad...
// use closure instead
$state = 0 
$even = true 

def exit(){ $state >= 12  }

def func_0(){  
    while( !exit() ){ 
      #atomic{
        if ( 2 /? $state ){
          printf(0)
          $state += 1
          $even = !$even
        }
     } 
  } 
}

def func_even(){ 
   n = 0 
    while( !exit() ){ 
      #atomic{
        if ( $state % 2 == 1 && $even ){
          n += 2 
          printf(n)
          $state += 1
        }
     } 
  } 
}

def func_odd(){ 
   n = -1 
    while( !exit() ){ 
      #atomic{
        if ( $state % 2 == 1 && !$even ){
          n += 2 
          printf(n)
          $state += 1
        }
     } 
  } 
}

threads = [ thread() as { func_0() } , thread() as { func_even() } , thread() as { func_odd() } ]
while ( exists( threads ) where { $.o.alive } );
println()
```

## Q252 
### Problem 
 >Return the pivot index of the given array of numbers. The pivot index is the index where the sum of the numbers on the left is equal to the sum of the numbers on the right. Input Array {1,2,3,4,0,6} 


This question originally appeared [here](https://careercup.com/question?id=5695352763056128).
Tagged with : Google,Software Engineer,Arrays 

### Solution 

```scala
/*
Everyone solved it right.
Thus, I am flexing some muscle power of ZoomBA,
to showcase the full declarative nature of the language : 
*/
def find_pivot_index( arr ){
  if ( empty(arr) ) return -1
  if ( size(arr) == 1 ) return 0
  sums = [ 0 , sum( arr ) - arr[0] ]
  x = index ( [ 1 : #|arr| ] ) :: {  
     sums.0 += arr[ $.item - 1]
     sums.1 -= arr[ $.item ]
     sums.0 == sums.1 
  }
  x < 0 ? x : x + 1 
}
println ( find_pivot_index( [ 0,6,4,1,2,3 ] ) )
```

## Q253 
### Problem 
 >CAREERCUP is a boad game hat contains m x n on a board. The objective of the CAREERCUP game is to reach the bottom of he board (bottom right corner) from the top of the board (top left corner) while moving one grid at a ime in either the down, right or diagonally downwrd directions.Write a method called CareerSolution that takes in two integers representing m and n, and returns the total number of ways a player can complete the game.PS: Was later asked to optimize the solution.int CareerSolution(int m, int n) {} 


This question originally appeared [here](https://careercup.com/question?id=5654319417262080).
Tagged with : Amazon,Intern,Arrays 

### Solution 

```scala
// ZoomBA
def careercup(path, x, y, M, N){
  if ( x > M || y > N ) return 0
  if ( x == M  && y == N ) {
    println( path )
    return 1 
  }
  result = ( careercup( path + str('(%s,%s)', x+1,y) , x + 1, y , M, N) + 
             careercup( path + str('(%s,%s)', x,y+1) , x, y + 1 , M, N) +
             careercup( path + str('(%s,%s)', x+1,y+1) , x + 1, y + 1, M, N) )
}
println ( careercup('(0,0)', 0, 0, 2, 2) )
```

## Q254 
### Problem 
 >Find the uncommon elements from 2 lists.I/P : List 1 : 1, 2, 3, 4, 5List 2 : 3, 4, 5, 6, 7O/P : 1, 2, 6, 7I/P : List 1 : 1, 2, 3, 1, 5List 2 : 3, 4, 5, 6, 7O/P : 1, 1, 2, 6, 7Most probably using space O(n) 


This question originally appeared [here](https://careercup.com/question?id=5736492145049600).
Tagged with : Android Engineer,Algorithm 

### Solution 

```scala
// ZoomBA
symmetric_diff = list1 ^ list2
```

