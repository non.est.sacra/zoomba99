# ZoomBA 99
Welcome.
The Goal is to duplicate the 
[99 problems](http://www.ic.unicamp.br/~meidanis/courses/mc336/2009s2/prolog/problemas/)
for [ZoomBA](https://gitlab.com/non.est.sacra/zoomba). 
Such adaption is not new, and very prevailing in the
[GitHub](https://github.com/shekhargulati/99-problems).

While creating this repo, we thought - why not add some more scripts to it.
Thus, all our day to day scripts are stored here.

For those who wants to get a quick job, there is a folder
for careercup. 

The samples should allow you to think in ZoomBA.
Enjoy your stay here.
